#include "TFile.h"
#include "TH2.h"
#include "TF1.h"
#include "TLorentzVector.h"
#include "TRandom3.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TMath.h"
#include "TColor.h"
#include "TStyle.h"
#include "TVector.h"
#include "TError.h"
#include "TSystemDirectory.h"
#include "TSystemFile.h"
#include "TVector3.h"
#include "TArrayI.h"

#include <iostream>
#include <utility>
#include <vector>

#include "TROOT.h"
#include "TSystem.h"
#include "TApplication.h"

#include "TString.h"
#include "TRandom3.h"
#include "TClonesArray.h"

#include "TLorentzVector.h"

#include "classes/DelphesClasses.h"

#include "ExRootAnalysis/ExRootTreeReader.h"
#include "ExRootAnalysis/ExRootTreeWriter.h"
#include "ExRootAnalysis/ExRootTreeBranch.h"
#include "ExRootAnalysis/ExRootResult.h"
#include "ExRootAnalysis/ExRootUtilities.h"

// Plots

TClonesArray * bEvent;
TClonesArray * bElectron;
TClonesArray * bMuon;
TClonesArray * bTruthWZ;
TClonesArray * bJet;
TClonesArray * bMissingET;
TClonesArray * bTruthLeptons;
TClonesArray * bParticle;

// Output
TFile * OutputFile;

TH1D * h_EventCount;
TH1D * h_WeightCount;

TH1D * h_W_Pt;
TH1D * h_Electron_Pt;
TH1D * h_Muon_Pt;
TH1D * h_Jet_Pt;
TH1D * h_WW_Mass;
TH1D * h_MissingET_Pt;
TH1D * h_ZPrimeReco_Mass;
TH1D * h_ZPrimeRecoHadronic_Mass;
TH1D * h_ZPrimeRecoLeptonic_Mass;
TH1D * h_ZPrimeRecoSemiLeptonic0_Mass;
TH1D * h_ZPrimeRecoSemiLeptonic1_Mass;
TH1D * h_ZPrimeRecoSemiLeptonic2_Mass;
TH1D * h_ZPrimeRecoOther_Mass;
TH1D * h_JetNum;
TH1D * h_ElectronNum;
TH1D * h_TruthElectronNum;
TH1D * h_MuonNum;
TH1D * h_TruthMuonNum;
TH1D * h_LeptonNum;
TH1D * h_TruthLeptonNum;
TH1D * h_TruthTauNum;
TH1D * h_JetDeltaR;
TH1D * h_VisibleLeptonSources;
TH1D * h_WElectrons;
TH1D * h_LeptonElectrons;
TH1D * h_MesonElectrons;
TH1D * h_BaryonElectrons;
TH1D * h_PhotonElectrons;
TH1D * h_WMuons;
TH1D * h_LeptonMuons;
TH1D * h_MesonMuons;
TH1D * h_BaryonMuons;
TH1D * h_PhotonMuons;

ExRootTreeReader * InitReader(const TString FilePath);

void Process(ExRootTreeReader * treeReader);

void ClearBranches();

int main(int argc, char* argv[]);
