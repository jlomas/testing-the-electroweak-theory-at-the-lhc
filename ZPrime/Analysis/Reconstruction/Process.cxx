
#include "Process.h"

bool Debug = false;

int main(int argc, char* argv[]) {

    // Input Delphes File

    const TString InputFile = argv[1];
    const TString OutputFileName = argv[2];

    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "Running Process"  << std::endl;
    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "InputFile = " << InputFile << std::endl;
    std::cout << "OutputFileName = " << OutputFileName << std::endl;
    std::cout << "-------------------------------------------------------------"  << std::endl;

    ExRootTreeReader * reader = NULL;
    reader = InitReader(InputFile);

    //------------------------------------
    // Declare the output
    //------------------------------------

    OutputFile = new TFile(OutputFileName,"recreate");

    OutputFile->cd();

    h_EventCount = new TH1D("h_EventCount","",1,0,1);
    h_WeightCount = new TH1D("h_WeightCount","",1,0,1);

    h_Electron_Pt = new TH1D("h_Electron_Pt","; Electron p_{T} [GeV]; Events / 5 GeV",200,0.0,1000.0);
    h_Muon_Pt = new TH1D("h_Muon_Pt","; Muon p_{T} [GeV]; Events / 5 GeV",200,0.0,1000.0);
    h_W_Pt = new TH1D("h_W_Pt","; W p_{T} [GeV]; Events / 5 GeV",200,0.0,1000.0);
    h_Jet_Pt = new TH1D("h_Jet_Pt","; Jet p_{T} [GeV]; Events / 5 GeV",200,0.0,1000.0);
    h_WW_Mass = new TH1D("h_WW_Mass","; WW Mass [GeV]; Events / 10 GeV",200,0.0,2000.0);
    h_MissingET_Pt = new TH1D("h_MissingET","; Missing E_{T} [GeV]; Events / 10 GeV",200,0.0,2000.0);
    h_ZPrimeReco_Mass = new TH1D("h_ZPrimeReco_Mass","; Reconstructed Z' Mass [GeV]; Events / 10 GeV",200,0.0,4000.0);
    h_ZPrimeRecoHadronic_Mass = new TH1D("h_ZPrimeRecoHadronic_Mass","; Reconstructed Z' Mass [GeV]; Events / 10 GeV",200,0.0,4000.0);
    h_ZPrimeRecoLeptonic_Mass = new TH1D("h_ZPrimeRecoLeptonic_Mass","; Reconstructed Z' Mass [GeV]; Events / 10 GeV",200,0.0,4000.0);
    h_ZPrimeRecoSemiLeptonic0_Mass = new TH1D("h_ZPrimeRecoSemiLeptonic0_Mass","; Reconstructed Z' Mass [GeV]; Events / 10 GeV",200,0.0,4000.0);
    h_ZPrimeRecoSemiLeptonic1_Mass = new TH1D("h_ZPrimeRecoSemiLeptonic1_Mass","; Reconstructed Z' Mass [GeV]; Events / 10 GeV",200,0.0,4000.0);
    h_ZPrimeRecoSemiLeptonic2_Mass = new TH1D("h_ZPrimeRecoSemiLeptonic2_Mass","; Reconstructed Z' Mass [GeV]; Events / 10 GeV",200,0.0,4000.0);
    h_ZPrimeRecoOther_Mass = new TH1D("h_ZPrimeRecoOther_Mass","; Reconstructed Z' Mass [GeV]; Events / 10 GeV",200,0.0,4000.0);

    //------------------------------------

    // Run the selection
    Process(reader);

    std::cout << "Events in EventCount: " << h_EventCount->GetEntries() << std::endl;

    std::cout << "Write to file..." << std::endl;

    OutputFile->cd();

    h_EventCount->Write();
    h_WeightCount->Write();

    h_W_Pt->Write();
    h_Electron_Pt->Write();
    h_Muon_Pt->Write();
    h_Jet_Pt->Write();
    h_WW_Mass->Write();
    h_MissingET_Pt->Write();
    h_ZPrimeReco_Mass->Write();
    h_ZPrimeRecoHadronic_Mass->Write();
    h_ZPrimeRecoLeptonic_Mass->Write();
    h_ZPrimeRecoSemiLeptonic0_Mass->Write();
    h_ZPrimeRecoSemiLeptonic1_Mass->Write();
    h_ZPrimeRecoSemiLeptonic2_Mass->Write();
    h_ZPrimeRecoOther_Mass->Write();

    OutputFile->Close();

    std::cout << "Tidy..." << std::endl;

    delete reader;

    std::cout << "Done!" << std::endl;

    return 0;

}

ExRootTreeReader * InitReader(const TString FilePath) {

    std::cout << "InitReader" << std::endl;

    TFile * f = TFile::Open(FilePath);

    TChain * Chain = new TChain("Delphes","");

    Chain->Add(FilePath);

    // Create object of class ExRootTreeReader
    ExRootTreeReader * r = new ExRootTreeReader(Chain);

    return r;
}

void Process(ExRootTreeReader * treeReader) {

    // Get pointers to branches used in this analysis
    bEvent = treeReader->UseBranch("Event");
    bJet = treeReader->UseBranch("Jet");
    bElectron = treeReader->UseBranch("Electron");
    bMuon = treeReader->UseBranch("Muon");
    bTruthWZ = treeReader->UseBranch("TruthWZParticles");
    bMissingET = treeReader->UseBranch("MissingET");
    bTruthLeptons = treeReader->UseBranch("TruthLeptonParticles");
    bParticle = treeReader->UseBranch("Particle");

    Long64_t numberOfEntries = treeReader->GetEntries();

    int nSelected = 0;

    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "Input: " << numberOfEntries << " events to process" << std::endl;

    // Loop over all events
    for(Int_t entry = 0; entry < numberOfEntries; ++entry) {

        // Load selected branches with data from specified event
        treeReader->ReadEntry(entry);

        HepMCEvent * event = (HepMCEvent*) bEvent->At(0);
    	const float Event_Weight = event->Weight;

        h_EventCount->Fill(0.5);
        h_WeightCount->Fill(0.5,Event_Weight);

        if( (entry > 0 && entry%1000 == 0) || Debug) {
            std::cout << "-------------------------------------------------------------"  << std::endl;
            std::cout << "Processing Event Number =  " << entry  << std::endl;
            std::cout << "-------------------------------------------------------------"  << std::endl;
        }

	std::vector<TLorentzVector> list_ZPrimeReco;

        //------------------------------------------------------------------
        // Jet Loop
        //------------------------------------------------------------------

	for(int i = 0; i < bJet->GetEntriesFast(); ++i) {

            Jet* jet = (Jet*) bJet->At(i);

            TLorentzVector Vec_Jet;
            Vec_Jet.SetPtEtaPhiM(jet->PT,jet->Eta,jet->Phi,jet->Mass);
	    h_Jet_Pt->Fill( Vec_Jet.Pt(), Event_Weight );
	    list_ZPrimeReco.push_back(Vec_Jet);

        } // Jet Loop

        //------------------------------------------------------------------
        // Electron Loop
        //------------------------------------------------------------------

	for(int i = 0; i < bElectron->GetEntriesFast(); ++i) {

            Electron* lep = (Electron*) bElectron->At(i);

            TLorentzVector Vec_Electron;
            Vec_Electron.SetPtEtaPhiM(lep->PT,lep->Eta,lep->Phi,511e-6);
            h_Electron_Pt->Fill( Vec_Electron.Pt(), Event_Weight );
	    list_ZPrimeReco.push_back(Vec_Electron);

        } // Electron Loop

	//------------------------------------------------------------------
        // Muon Loop
        //------------------------------------------------------------------
        
	for(int i = 0; i < bMuon->GetEntriesFast(); ++i) {

            Muon* lep = (Muon*) bMuon->At(i);

            TLorentzVector Vec_Muon;
            Vec_Muon.SetPtEtaPhiM(lep->PT,lep->Eta,lep->Phi,0.1056583745);
            h_Muon_Pt->Fill( Vec_Muon.Pt(), Event_Weight );
	    list_ZPrimeReco.push_back(Vec_Muon);

        } // Muon Loop

	//------------------------------------------------------------------
        // Missing ET Loop
        //------------------------------------------------------------------
        for(int i = 0; i < bMissingET->GetEntriesFast(); ++i) {

            MissingET* met = (MissingET*) bMissingET->At(i);
            
            TLorentzVector Vec_MissingET;
            Vec_MissingET.SetPtEtaPhiM(met->MET,met->Eta,met->Phi,0);
            h_MissingET_Pt->Fill( Vec_MissingET.Pt(), Event_Weight );
	    list_ZPrimeReco.push_back(Vec_MissingET);

        } // Missing ET Loop

	std::vector<TLorentzVector> list_Wboson;

        //------------------------------------------------------------------
        // W/Z Boson Loop
        //------------------------------------------------------------------
        for(int i = 0; i < bTruthWZ->GetEntriesFast(); ++i) {
            
            GenParticle* v = (GenParticle*) bTruthWZ->At(i);
            
            TLorentzVector Vec_V;
            Vec_V.SetPtEtaPhiM(v->PT,v->Eta,v->Phi,v->Mass);
        
    	    // Look for W bosons (ID = +/-24)
            if( abs(v->PID) == 24 ) {
    		h_W_Pt->Fill( Vec_V.Pt(), Event_Weight );
    		list_Wboson.push_back(Vec_V);
             }
            
        } // W/Z Loop

	if( list_Wboson.size() > 1 ) {

		TLorentzVector Zprime = list_Wboson.at(0) + list_Wboson.at(1);

              h_WW_Mass->Fill( Zprime.M() , Event_Weight );
	}

	//------------------------------------------------------------------
        // Z' Reconstruction
        //------------------------------------------------------------------
	TLorentzVector ZPrimeReco;
	for(int i = 0; i < list_ZPrimeReco.size(); ++i) {
	    ZPrimeReco += list_ZPrimeReco.at(i);
	}
	
	h_ZPrimeReco_Mass->Fill(ZPrimeReco.M(), Event_Weight );

	if( bJet->GetEntriesFast() == 4 ) {
	    if ( bElectron->GetEntriesFast() == 0 && bMuon->GetEntriesFast() == 0 ) {
		h_ZPrimeRecoHadronic_Mass->Fill(ZPrimeReco.M(), Event_Weight );
	    }
	}
	else if( bJet->GetEntriesFast() == 2 ) {
	    if ( bElectron->GetEntriesFast() == 0 && bMuon->GetEntriesFast() == 0 ) {
		h_ZPrimeRecoSemiLeptonic0_Mass->Fill(ZPrimeReco.M(), Event_Weight );
	    }
	    else if ( bElectron->GetEntriesFast() == 1 || bMuon->GetEntriesFast() == 1 ) {
		h_ZPrimeRecoSemiLeptonic1_Mass->Fill(ZPrimeReco.M(), Event_Weight );
	    }
	    else if ( bElectron->GetEntriesFast() == 2 || bMuon->GetEntriesFast() == 2 ) {
		h_ZPrimeRecoSemiLeptonic2_Mass->Fill(ZPrimeReco.M(), Event_Weight );
	    }
	}
	else if( bJet->GetEntriesFast() == 0 ) {
	    h_ZPrimeRecoLeptonic_Mass->Fill(ZPrimeReco.M(), Event_Weight );
	}
	else {
	    h_ZPrimeRecoOther_Mass->Fill(ZPrimeReco.M(), Event_Weight );
	}

    } // Loop over all events
}
