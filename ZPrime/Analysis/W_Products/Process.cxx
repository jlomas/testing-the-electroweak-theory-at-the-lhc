
#include "Process.h"

bool Debug = false;

int main(int argc, char* argv[]) {

	// Input Delphes File

	const TString InputFile = argv[1];
	const TString OutputFileName = argv[2];

	std::cout << "-------------------------------------------------------------" << std::endl;
	std::cout << "Running Process" << std::endl;
	std::cout << "-------------------------------------------------------------" << std::endl;
	std::cout << "InputFile = " << InputFile << std::endl;
	std::cout << "OutputFileName = " << OutputFileName << std::endl;
	std::cout << "-------------------------------------------------------------" << std::endl;

	ExRootTreeReader* reader = NULL;
	reader = InitReader(InputFile);

	//------------------------------------
	// Declare the output
	//------------------------------------

	OutputFile = new TFile(OutputFileName, "recreate");

	OutputFile->cd();

	h_EventCount = new TH1D("h_EventCount", "", 1, 0, 1);
	h_WeightCount = new TH1D("h_WeightCount", "", 1, 0, 1);

	//------------------------------------

	// Run the selection
	Process(reader);

	std::cout << "Events in EventCount: " << h_EventCount->GetEntries() << std::endl;

	std::cout << "Write to file..." << std::endl;

	OutputFile->cd();

	h_EventCount->Write();
	h_WeightCount->Write();

	OutputFile->Close();

	std::cout << "Tidy..." << std::endl;

	delete reader;

	std::cout << "Done!" << std::endl;

	return 0;

}

ExRootTreeReader* InitReader(const TString FilePath) {

	std::cout << "InitReader" << std::endl;

	TFile* f = TFile::Open(FilePath);

	TChain* Chain = new TChain("Delphes", "");

	Chain->Add(FilePath);

	// Create object of class ExRootTreeReader
	ExRootTreeReader* r = new ExRootTreeReader(Chain);

	return r;
}

void Process(ExRootTreeReader* treeReader) {

	// Get pointers to branches used in this analysis
	bEvent = treeReader->UseBranch("Event");
	bJet = treeReader->UseBranch("Jet");
	bGenJet = treeReader->UseBranch("GenJet");
	bElectron = treeReader->UseBranch("Electron");
	bMuon = treeReader->UseBranch("Muon");
	bTruthLeptons = treeReader->UseBranch("TruthLeptonParticles");
	bMissingET = treeReader->UseBranch("MissingET");
	bGenMissingET = treeReader->UseBranch("GenMissingET");
	bParticle = treeReader->UseBranch("Particle");

	Long64_t numberOfEntries = treeReader->GetEntries();
	Int_t EntriestoPrint = 50;
	std::vector<Long64_t> WMothers;

	int nSelected = 0;

	std::cout << "-------------------------------------------------------------" << std::endl;
	std::cout << "Input: " << numberOfEntries << " events to process" << std::endl;

	// Loop over all events
	for (Int_t entry = 0; entry < numberOfEntries; ++entry) {

		// Load selected branches with data from specified event
		treeReader->ReadEntry(entry);

		HepMCEvent* event = (HepMCEvent*)bEvent->At(0);
		const float Event_Weight = event->Weight;

		h_EventCount->Fill(0.5);
		h_WeightCount->Fill(0.5, Event_Weight);

		if ((entry > 0 && entry % 1000 == 0) || Debug) {
			std::cout << "-------------------------------------------------------------" << std::endl;
			std::cout << "Processing Event Number =  " << entry << std::endl;
			std::cout << "-------------------------------------------------------------" << std::endl;
		}

		Long64_t PrintEntry = 0;
		Long64_t Wnum = 0;

		if (entry < EntriestoPrint) {
			PrintEntry += 1;
		}

		for (int i = 0; i < bParticle->GetEntriesFast(); ++i) {
			GenParticle* particle = (GenParticle*)bParticle->At(i);
			if (abs(particle->PID) == 24) {
				GenParticle* M1 = (GenParticle*)bParticle->At(particle->M1);
				GenParticle* D1 = (GenParticle*)bParticle->At(particle->D1);
				Long64_t D1_index = D1->D1;
				while (abs(D1->PID) == 24) {
					D1_index = D1->D1;
					D1 = (GenParticle*)bParticle->At(D1_index);
				}
				GenParticle* D2 = (GenParticle*)bParticle->At(particle->D2);

				TString W_output = "None";
				if (abs(M1->PID) == 32) {
					Wnum++;
					if (particle->Charge < 0) {
						W_output = "\033[1;32mW-\033[0m";
					}
					else {
						W_output = "\033[1;32mW+\033[0m";
					}
					if (PrintEntry > 0) {
						std::cout << "Event  " << entry + 1 << ": " << W_output << " -> D1 = " << D1->PID << ", D2 = " << D2->PID << std::endl;
					}
				}
			}
		}
	}
}
