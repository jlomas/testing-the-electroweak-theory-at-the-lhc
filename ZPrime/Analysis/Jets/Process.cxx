
#include "Process.h"

bool Debug = false;

int main(int argc, char* argv[]) {

	// Input Delphes File

	const TString InputFile = argv[1];
	const TString OutputFileName = argv[2];

	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "Running Process"  << std::endl;
	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "InputFile = " << InputFile << std::endl;
	std::cout << "OutputFileName = " << OutputFileName << std::endl;
	std::cout << "-------------------------------------------------------------"  << std::endl;

	ExRootTreeReader * reader = NULL;
	reader = InitReader(InputFile);

	//------------------------------------
	// Declare the output
	//------------------------------------

	OutputFile = new TFile(OutputFileName,"recreate");

	OutputFile->cd();

	h_EventCount = new TH1D("h_EventCount","",1,0,1);
	h_WeightCount = new TH1D("h_WeightCount","",1,0,1);

	h_JetPairMass = new TH1D("h_JetPairMass", "; Mass of Jet Pair / GeV; Events", 200, 0, 2000);

	//------------------------------------

	// Run the selection
	Process(reader);

	std::cout << "Events in EventCount: " << h_EventCount->GetEntries() << std::endl;

	std::cout << "Write to file..." << std::endl;

	OutputFile->cd();

	h_EventCount->Write();
	h_WeightCount->Write();

	h_JetPairMass->Write();

	OutputFile->Close();

	std::cout << "Tidy..." << std::endl;

	delete reader;

	std::cout << "Done!" << std::endl;

	return 0;

}

ExRootTreeReader * InitReader(const TString FilePath) {

	std::cout << "InitReader" << std::endl;

	TFile * f = TFile::Open(FilePath);

	TChain * Chain = new TChain("Delphes","");

	Chain->Add(FilePath);

	// Create object of class ExRootTreeReader
	ExRootTreeReader * r = new ExRootTreeReader(Chain);

	return r;
}

void Process(ExRootTreeReader * treeReader) {

	// Get pointers to branches used in this analysis
	bEvent = treeReader->UseBranch("Event");
	bJet = treeReader->UseBranch("Jet");
	bGenJet = treeReader->UseBranch("GenJet");
	bElectron = treeReader->UseBranch("Electron");
	bMuon = treeReader->UseBranch("Muon");
	bTruthLeptons = treeReader->UseBranch("TruthLeptonParticles");
	bMissingET = treeReader->UseBranch("MissingET");
	bGenMissingET = treeReader->UseBranch("GenMissingET");
	bParticle = treeReader->UseBranch("Particle");

	Long64_t numberOfEntries = treeReader->GetEntries();

	int nSelected = 0;

	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "Input: " << numberOfEntries << " events to process" << std::endl;

	// Loop over all events
	for(Int_t entry = 0; entry < numberOfEntries; ++entry) {

		// Load selected branches with data from specified event
		treeReader->ReadEntry(entry);

		HepMCEvent * event = (HepMCEvent*) bEvent->At(0);
		const float Event_Weight = event->Weight;

		h_EventCount->Fill(0.5);
		h_WeightCount->Fill(0.5,Event_Weight);

		if( (entry > 0 && entry%1000 == 0) || Debug) {
			std::cout << "-------------------------------------------------------------"  << std::endl;
			std::cout << "Processing Event Number =  " << entry  << std::endl;
			std::cout << "-------------------------------------------------------------"  << std::endl;
		}

		//------------------------------------------------------------------
		// Jet Loop
		//------------------------------------------------------------------

		for (int i = 0; i < bGenJet->GetEntriesFast(); ++i) {
			Jet* genjet1 = (Jet*)bGenJet->At(i);
			std::vector<Jet*> MatchedGenJets;
			MatchedGenJets.push_back(genjet1);
			for (int j = i+1; j < bGenJet->GetEntriesFast(); ++j) {
				Jet* genjet2 = (Jet*)bGenJet->At(j);

				Double_t Delta_phi = abs(genjet1->Phi - genjet2->Phi)
				if (Delta_phi > TMath::Pi()) {
					Delta_phi = (2 * TMath::Pi()) - Delta_phi;
				}
				Double_t Delta_R = TMath::Sqrt(TMath::Sq(genjet1->Eta - genjet2->Eta) + TMath::Sq(Delta_phi));
				if (Delta_R < 0.5) {
					MatchedGenJets.push_back(genjet2);
				}
			}
			if (MatchedGenJets.size() == 2) {
				Int_t JetMatches = 0;
				for (int j = 0; j < bJet->GetEntriesFast(); ++j) {
					Jet* jet1 = (Jet*)bJet->At(j);

					Double_t Delta_phi = abs(genjet1->Phi - jet1->Phi)
						if (Delta_phi > TMath::Pi()) {
							Delta_phi = (2 * TMath::Pi()) - Delta_phi;
						}
					Double_t Delta_R = TMath::Sqrt(TMath::Sq(genjet1->Eta - jet1->Eta) + TMath::Sq(Delta_phi));
					if (Delta_R < 0.5) {
						JetMatches++;
					}
				}
				if (JetMatches = 1) {
					for (int i = 0; i < bGenJet->GetEntriesFast(); ++i) {
						Jet* genjet1 = (Jet*)bGenJet->At(i);
						std::cout << "GenJet " << i + 1 << ": pT = " << genjet->PT << ", eta = " << genjet->Eta << ", phi = " << genjet->Phi << ", energy = " << Vec_GenJet.E() << ", mass = " << genjet->Mass << ", flavour = " << genjet->Flavor << std::endl;
				}
			}
		} // Jet Loop

	} // Loop over all events

}
