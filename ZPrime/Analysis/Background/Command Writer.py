import numpy as np

def write_phasespace_command(Zmassmax, BGtype, mHatLow, mHatHigh, SixMass, nohup=False):
    if nohup == True:
        out = 'nohup ./Process'
    else:
        out = './Process'
    out += ' /disk/homedisk/home/user190/Zprime/Analysis/SemiLeptonic_withBG_' + BGtype + '_PhaseSpaceCuts_to' + str(Zmassmax) + 'TeV/SemiLeptonic_withBG_' + BGtype + '_PhaseSpaceCuts_to' + str(Zmassmax) + 'TeV_histo.root'
    mHatRange = np.arange(mHatLow, mHatHigh+100, 100)
    for mHatMin in mHatRange:
        if mHatMin < SixMass:
            out += ' /disk/homedisk/home/user190/Zprime/Analysis/Background/' + BGtype + '/Output\ Files/mHatMin_' + str(mHatMin) + '/output_' + BGtype + '_mHatMin_' + str(mHatMin) + '_1e5.root'
        else:
            out += ' /disk/homedisk/home/user190/Zprime/Analysis/Background/' + BGtype + '/Output\ Files/mHatMin_' + str(mHatMin) + '/output_' + BGtype + '_mHatMin_' + str(mHatMin) + '_1e6.root'
    if nohup == True:
        out += ' > nohup_' + BGtype + '.out &'
    print(out)
    return

def write_reconwithbg_command(Zmass, mHatMin, sizes, nohup=False):
    BG_types = ['WParton', 'Diboson', 'ttbar', 'SingleTop']
    if nohup == True:
        out = 'nohup ./Process'
    else:
        out = './Process'
    out += ' /disk/homedisk/home/user190/Zprime/Output\ Files/Zprime_' + str(Zmass) + 'GeV/output_Zprime_' + str(Zmass) + 'GeV_1e5.root'
    out += ' /disk/homedisk/home/user190/Zprime/Analysis/SemiLeptonic_withBG/Zprime_' + str(Zmass) + 'GeV_histo.root'
    for i, b in enumerate(BG_types):
        out += ' /disk/homedisk/home/user190/Zprime/Analysis/Background/' + b + '/Output\ Files/mHatMin_' + str(mHatMin) + '/output_' + b + '_mHatMin_' + str(mHatMin) + '_1e' + str(sizes[i]) + '.root'
    if nohup == True:
        out += ' > nohup' + str(Zmass) + '.out &'
    print(out)
    return

def write_cutflow_command(Zmass, mHatMin, sizes, nohup=False):
    BG_types = ['WParton', 'Diboson', 'ttbar', 'SingleTop']
    if nohup == True:
        out = 'nohup ./Process'
    else:
        out = './Process'
    out += ' /disk/homedisk/home/user190/Zprime/Output\ Files/Zprime_' + str(Zmass) + 'GeV/output_Zprime_' + str(Zmass) + 'GeV_1e5.root'
    out += ' /disk/homedisk/home/user190/Zprime/Analysis/SemiLeptonic_withBG_CutFlow/CutFlow_' + str(Zmass) + 'GeV_histo.root'
    for i, b in enumerate(BG_types):
        out += ' /disk/homedisk/home/user190/Zprime/Analysis/Background/' + b + '/Output\ Files/mHatMin_' + str(mHatMin) + '/output_' + b + '_mHatMin_' + str(mHatMin) + '_1e' + str(sizes[i]) + '.root'
    if nohup == True:
        out += ' > nohup' + str(Zmass) + '.out &'
    print(out)
    return

def write_n_minus_1_command(Zmass, mHatMin, sizes, nohup=False):
    BG_types = ['WParton', 'Diboson', 'ttbar', 'SingleTop']
    if nohup == True:
        out = 'nohup ./Process'
    else:
        out = './Process'
    out += ' /disk/homedisk/home/user190/Zprime/Output\ Files/Zprime_' + str(Zmass) + 'GeV/output_Zprime_' + str(Zmass) + 'GeV_1e5.root'
    out += ' /disk/homedisk/home/user190/Zprime/Analysis/SemiLeptonic_withBG_n-1Plots/n-1Plots_' + str(Zmass) + 'GeV_histo.root'
    for i, b in enumerate(BG_types):
        out += ' /disk/homedisk/home/user190/Zprime/Analysis/Background/' + b + '/Output\ Files/mHatMin_' + str(mHatMin) + '/output_' + b + '_mHatMin_' + str(mHatMin) + '_1e' + str(sizes[i]) + '.root'
    if nohup == True:
        out += ' > nohup' + str(Zmass) + '.out &'
    print(out)
    return

def write_parameters(Excel_output):
    types = ['Zprime', 'WParton', 'Diboson', 'ttbar', 'SingleTop']
    out = '\nDouble_t Zprime_Mass = ' + Excel_output[0:4] + ';\n'
    if Excel_output[8] == '\t':
        for n, t in enumerate(types):
            n += 1
            n *= 10
            out += 'Double_t ' + t + '_CrossSection = ' + Excel_output[n-1:n+4] + 'e' + Excel_output[n+5:n+8] + ';\n'
    else:
        for n, t in enumerate(types):
            n += 1
            n *= 10
            out += 'Double_t ' + t + '_CrossSection = ' + Excel_output[n:n+5] + 'e' + Excel_output[n+6:n+9] + ';\n'
    print(out[:-1])
    return