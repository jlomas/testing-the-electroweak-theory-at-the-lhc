
#include "Process.h"

bool Debug = false;

// Constants
Int_t ToPrint = 0;
Double_t e_mass = 0.00051099895;
Double_t muon_mass = 0.1056583755;
Double_t W_mass = 80.38;
Double_t Int_Luminosity = 139.0e+15;

//Parameters
Double_t Zprime_Mass = 1000;
Double_t Zprime_CrossSection = 2.534e-13;
Double_t WParton_CrossSection = 1.219e-09;
Double_t Diboson_CrossSection = 7.210e-12;
Double_t ttbar_CrossSection = 2.055e-10;
Double_t SingleTop_CrossSection = 4.992e-11;

// Cuts
/// Signal
Double_t JetPairMass_lowcut_signal = 60;
Double_t JetPairMass_highcut_signal = 110;
//Double_t JetPairMass_lowcut_signal = 0;
//Double_t JetPairMass_highcut_signal = 200;
Double_t Lepton_pTcut_signal = 10;
Double_t MET_cut = 40;
Double_t MET_DeltaM_cut = 30;
/// Background
Double_t HadronicW_pTcut = 390;
Double_t JetPairMass_cutwidth = 22;
Double_t LeptonicW_pTcut = 350;
Double_t Lepton_pTcut_background = 10;
Int_t Jet_NumLimit = 5;
Double_t Rfunction_cut = 0;
Double_t Window_size = 260;

int main(int argc, char* argv[]) {

	// Input Delphes File

	const TString SignalFile = argv[1];
	const TString OutputFileName = argv[2];
	const TString Background_WParton_File = argv[3];
	const TString Background_Diboson_File = argv[4];
	const TString Background_ttbar_File = argv[5];
	const TString Background_SingleTop_File = argv[6];

	std::vector<TString> InputFiles;
	InputFiles.push_back(SignalFile);
	for (int i = 3; i < 7; i++) {
		InputFiles.push_back(argv[i]);
	}

	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "Running Process"  << std::endl;
	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "SignalFile = " << SignalFile << std::endl;
	std::cout << "OutputFileName = " << OutputFileName << std::endl;
	std::cout << "-------------------------------------------------------------"  << std::endl;

	//------------------------------------
	// Declare the output
	//------------------------------------

	OutputFile = new TFile(OutputFileName,"recreate");

	OutputFile->cd();

	h_EventCount = new TH1D("h_EventCount","",1,0,1);
	h_WeightCount = new TH1D("h_WeightCount","",1,0,1);

	THStack * h_ZprimeMass = new THStack("h_ZprimeMass", "; WW Mass [GeV]; Events / 50 GeV");
	THStack * h_ZprimeMass_Window = new THStack("h_ZprimeMass_Window", "; WW Mass [GeV]; Events / 50 GeV");

	THStack* h_ZprimeMass_precuts = new THStack("h_ZprimeMass_precuts", "; WW Mass [GeV]; Events / 50 GeV");
	THStack* h_ZprimeMass_Window_precuts = new THStack("h_ZprimeMass_Window_precuts", "; WW Mass [GeV]; Events / 50 GeV");

	Double_t JetMassCutWidth_list[80]{}, JetMassCutWidthSignalCount[80]{}, JetMassCutWidthBackgroundCount[80]{};
	for (Double_t i = 1.0; i < W_mass; i += 1.0) {
		JetMassCutWidth_list[TMath::FloorNint(i) - 1] = i;
		JetMassCutWidthSignalCount[TMath::FloorNint(i) - 1] = 0.0;
		JetMassCutWidthBackgroundCount[TMath::FloorNint(i) - 1] = 0.0;
		JetMassCutWidthSignalCount[TMath::FloorNint(i) - 1] = 0.0;
		JetMassCutWidthBackgroundCount[TMath::FloorNint(i) - 1] = 0.0;
	}
	Double_t JetNumLimit_list[8]{}, JetNumSignalCount[8]{}, JetNumBackgroundCount[8]{};
	for (Double_t i = 0.0; i < 8.0; i += 1.0) {
		JetNumLimit_list[TMath::FloorNint(i)] = i + 3.0;
		JetNumSignalCount[TMath::FloorNint(i)] = 0.0;
		JetNumBackgroundCount[TMath::FloorNint(i)] = 0.0;
		JetNumSignalCount[TMath::FloorNint(i)] = 0.0;
		JetNumBackgroundCount[TMath::FloorNint(i)] = 0.0;
	}
	Double_t LeptonpTCut_list[TMath::FloorNint(Zprime_Mass / 2.0)]{}, LeptonpTSignalCount[TMath::FloorNint(Zprime_Mass / 2.0)]{}, LeptonpTBackgroundCount[TMath::FloorNint(Zprime_Mass / 2.0)]{};
	for (Double_t i = 0.0; i < Zprime_Mass/2.0; i += 1.0) {
		LeptonpTCut_list[TMath::FloorNint(i)] = i + 1.0;
		LeptonpTSignalCount[TMath::FloorNint(i)] = 0.0;
		LeptonpTBackgroundCount[TMath::FloorNint(i)] = 0.0;
		LeptonpTSignalCount[TMath::FloorNint(i)] = 0.0;
		LeptonpTBackgroundCount[TMath::FloorNint(i)] = 0.0;
	}
	Double_t LeptonicWpTCut_list[TMath::FloorNint(Zprime_Mass/2.0)]{}, LeptonicWpTSignalCount[TMath::FloorNint(Zprime_Mass/2.0)]{}, LeptonicWpTBackgroundCount[TMath::FloorNint(Zprime_Mass/2.0)]{};
	for (Double_t i = 0.0; i < Zprime_Mass/2.0; i += 1.0) {
		LeptonicWpTCut_list[TMath::FloorNint(i)] = i + 1.0;
		LeptonicWpTSignalCount[TMath::FloorNint(i)] = 0.0;
		LeptonicWpTBackgroundCount[TMath::FloorNint(i)] = 0.0;
		LeptonicWpTSignalCount[TMath::FloorNint(i)] = 0.0;
		LeptonicWpTBackgroundCount[TMath::FloorNint(i)] = 0.0;
	}
	Double_t HadronicWpTCut_list[TMath::FloorNint(Zprime_Mass/2.0)]{}, HadronicWpTSignalCount[TMath::FloorNint(Zprime_Mass/2.0)]{}, HadronicWpTBackgroundCount[TMath::FloorNint(Zprime_Mass/2.0)]{};
	for (Double_t i = 0.0; i < Zprime_Mass/2.0; i += 1.0) {
		HadronicWpTCut_list[TMath::FloorNint(i)] = i + 1.0;
		HadronicWpTSignalCount[TMath::FloorNint(i)] = 0.0;
		HadronicWpTBackgroundCount[TMath::FloorNint(i)] = 0.0;
		HadronicWpTSignalCount[TMath::FloorNint(i)] = 0.0;
		HadronicWpTBackgroundCount[TMath::FloorNint(i)] = 0.0;
	}
	Double_t WindowSize_list[TMath::FloorNint(Zprime_Mass / 2.0) - 1]{}, WindowSize_SignalCount[TMath::FloorNint(Zprime_Mass / 2.0) - 1]{}, WindowSize_BackgroundCount[TMath::FloorNint(Zprime_Mass / 2.0) - 1]{};
	for (Double_t i = 1.0; i < Zprime_Mass / 2.0; i += 1.0) {
		WindowSize_list[TMath::FloorNint(i) - 1] = i;
		WindowSize_SignalCount[TMath::FloorNint(i) - 1] = 0.0;
		WindowSize_BackgroundCount[TMath::FloorNint(i) - 1] = 0.0;
	}
	Double_t RfunctionCut_list[50]{}, RfunctionSignalCount[50]{}, RfunctionBackgroundCount[50]{};
	for (Double_t i = 1.0; i < 51.0; i += 1.0) {
		RfunctionCut_list[TMath::FloorNint(i) - 1] = i / 100;
		RfunctionSignalCount[TMath::FloorNint(i) - 1] = 0.0;
		RfunctionBackgroundCount[TMath::FloorNint(i) - 1] = 0.0;
	}

	// define sub-histograms - pre-cuts
	
	TH1D* h_SignalReconMass_precuts = new TH1D("Z' precuts", "; WW Mass [GeV]; Events / 50 GeV", TMath::FloorNint((Zprime_Mass + 1000.0)/50.0), 0, Zprime_Mass+1000.0);
	TH1D* h_Background_WParton_ReconMass_precuts = new TH1D("V+jet precuts", "; WW Mass [GeV]; Events / 50 GeV", TMath::FloorNint((Zprime_Mass + 1000.0) / 50.0), 0, Zprime_Mass + 1000.0);
	TH1D * h_Background_Diboson_ReconMass_precuts = new TH1D("SM Diboson precuts", "; WW Mass [GeV]; Events / 50 GeV", TMath::FloorNint((Zprime_Mass + 1000.0) / 50.0), 0, Zprime_Mass + 1000.0);
	TH1D * h_Background_ttbar_ReconMass_precuts = new TH1D("t#bar{t} precuts", "; WW Mass [GeV]; Events / 50 GeV", TMath::FloorNint((Zprime_Mass + 1000.0) / 50.0), 0, Zprime_Mass + 1000.0);
	TH1D * h_Background_SingleTop_ReconMass_precuts = new TH1D("Single Top precuts", "; WW Mass [GeV]; Events / 50 GeV", TMath::FloorNint((Zprime_Mass + 1000.0) / 50.0), 0, Zprime_Mass + 1000.0);

	TH1D* h_SignalReconMass_Window_precuts = new TH1D("Z' precuts", "; WW Mass [GeV]; Events / 50 GeV", 8, Zprime_Mass - Window_size, Zprime_Mass + Window_size);
	TH1D* h_Background_WParton_ReconMass_Window_precuts = new TH1D("V+jet precuts", "; WW Mass [GeV]; Events / 50 GeV", 8, Zprime_Mass - Window_size, Zprime_Mass + Window_size);
	TH1D* h_Background_Diboson_ReconMass_Window_precuts = new TH1D("SM Diboson precuts", "; WW Mass [GeV]; Events / 50 GeV", 8, Zprime_Mass - Window_size, Zprime_Mass + Window_size);
	TH1D* h_Background_ttbar_ReconMass_Window_precuts = new TH1D("t#bar{t} precuts", "; WW Mass [GeV]; Events / 50 GeV", 8, Zprime_Mass - Window_size, Zprime_Mass + Window_size);
	TH1D* h_Background_SingleTop_ReconMass_Window_precuts = new TH1D("Single Top precuts", "; WW Mass [GeV]; Events / 50 GeV", 8, Zprime_Mass - Window_size, Zprime_Mass + Window_size);

	TH1D * h_JetPairMass_precuts = new TH1D("h_JetPairMass_precuts", "; m_{jj} [GeV]; Events / 5 GeV", 100, 0, 500);

	// define sub-histograms - post-cuts
	
	TH1D* h_SignalReconMass = new TH1D("Z'", "; WW Mass [GeV]; Events / 50 GeV", TMath::FloorNint((Zprime_Mass + 1000.0)/50.0), 0, Zprime_Mass+1000.0);
	TH1D* h_Background_WParton_ReconMass = new TH1D("V+jet", "; WW Mass [GeV]; Events / 50 GeV", TMath::FloorNint((Zprime_Mass + 1000.0)/50.0), 0, Zprime_Mass+1000.0);
	TH1D* h_Background_Diboson_ReconMass = new TH1D("SM Diboson", "; WW Mass [GeV]; Events / 50 GeV", TMath::FloorNint((Zprime_Mass + 1000.0)/50.0), 0, Zprime_Mass+1000.0);
	TH1D* h_Background_ttbar_ReconMass = new TH1D("t#bar{t}", "; WW Mass [GeV]; Events / 50 GeV", TMath::FloorNint((Zprime_Mass + 1000.0)/50.0), 0, Zprime_Mass+1000.0);
	TH1D* h_Background_SingleTop_ReconMass = new TH1D("Single Top", "; WW Mass [GeV]; Events / 50 GeV", TMath::FloorNint((Zprime_Mass + 1000.0)/50.0), 0, Zprime_Mass+1000.0);

	TH1D* h_SignalReconMass_Window = new TH1D("Z'", "; WW Mass [GeV]; Events / 50 GeV", 8, Zprime_Mass - Window_size, Zprime_Mass + Window_size);
	TH1D* h_Background_WParton_ReconMass_Window = new TH1D("V+jet", "; WW Mass [GeV]; Events / 50 GeV", 8, Zprime_Mass - Window_size, Zprime_Mass + Window_size);
	TH1D* h_Background_Diboson_ReconMass_Window = new TH1D("SM Diboson", "; WW Mass [GeV]; Events / 50 GeV", 8, Zprime_Mass - Window_size, Zprime_Mass + Window_size);
	TH1D* h_Background_ttbar_ReconMass_Window = new TH1D("t#bar{t}", "; WW Mass [GeV]; Events / 50 GeV", 8, Zprime_Mass - Window_size, Zprime_Mass + Window_size);
	TH1D* h_Background_SingleTop_ReconMass_Window = new TH1D("Single Top", "; WW Mass [GeV]; Events / 50 GeV", 8, Zprime_Mass - Window_size, Zprime_Mass + Window_size);

	TH1D* h_JetPairMass = new TH1D("h_JetPairMass", "; m_{jj} [GeV]; Events / 5 GeV", 100, 0, 500);

	//------------------------------------

	// Run the selection

	// Reading Input Files

	std::vector <Double_t> CrossSections;
	CrossSections.push_back(Zprime_CrossSection);
	CrossSections.push_back(WParton_CrossSection);
	CrossSections.push_back(Diboson_CrossSection);
	CrossSections.push_back(ttbar_CrossSection);
	CrossSections.push_back(SingleTop_CrossSection);

	Double_t SignalNumber, BGNumber, WPartonNumber, DibosonNumber, ttbarNumber, SingleTopNumber;
	Double_t JetPairMassSignalNumber = 0.0;
	Double_t JetBTaggerSignalNumber = 0.0;
	Double_t LeptonpTSignalNumber = 0.0;
	Double_t METSignalNumber = 0.0;

	for (int i = 0; i < InputFiles.size(); i++) {
		if (i == 0) {
			std::cout << "-------------------------------------------------------------" << std::endl;
			std::cout << "Processing Signal File..." << std::endl;
			std::cout << "-------------------------------------------------------------" << std::endl;
		}
		else {
			std::cout << "-------------------------------------------------------------" << std::endl;
			std::cout << "Processing Background File " << i << "..." << std::endl;
			std::cout << "-------------------------------------------------------------" << std::endl;
		}

		ExRootTreeReader* reader = NULL;
		reader = InitReader(InputFiles.at(i));
		
		// Get pointers to branches used in this analysis
		bEvent = reader->UseBranch("Event");
		bJet = reader->UseBranch("Jet");
		bGenJet = reader->UseBranch("GenJet");
		bElectron = reader->UseBranch("Electron");
		bMuon = reader->UseBranch("Muon");
		bTruthLeptons = reader->UseBranch("TruthLeptonParticles");
		bMissingET = reader->UseBranch("MissingET");
		bGenMissingET = reader->UseBranch("GenMissingET");
		bTruthWZ = reader->UseBranch("TruthWZParticles");

		Long64_t numberOfEntries = reader->GetEntries();

		int nSelected = 0;

		std::cout << "-------------------------------------------------------------" << std::endl;
		std::cout << "Input: " << numberOfEntries << " events to process" << std::endl;

		// Loop over all events
		for (Int_t entry = 0; entry < numberOfEntries; ++entry) {

			// Load selected branches with data from specified event
			reader->ReadEntry(entry);

			HepMCEvent* event = (HepMCEvent*)bEvent->At(0);
			const float Event_Weight = (CrossSections.at(i) * Int_Luminosity) / numberOfEntries;

			h_EventCount->Fill(0.5);
			h_WeightCount->Fill(0.5, Event_Weight);

			if ((entry > 0 && entry % 10000 == 0) || Debug) {
				if (i == 0) {
					std::cout << "-------------------------------------------------------------" << std::endl;
					std::cout << "Processing Signal Event Number =  " << entry << std::endl;
					std::cout << "-------------------------------------------------------------" << std::endl;
				}
				if (i == 1) {
					std::cout << "-------------------------------------------------------------" << std::endl;
					std::cout << "Processing WParton Event Number =  " << entry << std::endl;
					std::cout << "-------------------------------------------------------------" << std::endl;
				}
				if (i == 2) {
					std::cout << "-------------------------------------------------------------" << std::endl;
					std::cout << "Processing Diboson Event Number =  " << entry << std::endl;
					std::cout << "-------------------------------------------------------------" << std::endl;
				}
				if (i == 3) {
					std::cout << "-------------------------------------------------------------" << std::endl;
					std::cout << "Processing ttbar Event Number =  " << entry << std::endl;
					std::cout << "-------------------------------------------------------------" << std::endl;
				}
				if (i == 4) {
					std::cout << "-------------------------------------------------------------" << std::endl;
					std::cout << "Processing SingleTop Event Number =  " << entry << std::endl;
					std::cout << "-------------------------------------------------------------" << std::endl;
				}
			}

			//------------------------------------------------------------------
			// Jet Loop
			//------------------------------------------------------------------

			TLorentzVector JetPair;

			if (bJet->GetEntriesFast() >= 2) {
				Jet* jet1 = (Jet*)bJet->At(0);
				Jet* jet2 = (Jet*)bJet->At(1);

				TLorentzVector Vec_Jet1;
				TLorentzVector Vec_Jet2;
				Vec_Jet1.SetPtEtaPhiM(jet1->PT, jet1->Eta, jet1->Phi, jet1->Mass);
				Vec_Jet2.SetPtEtaPhiM(jet2->PT, jet2->Eta, jet2->Phi, jet2->Mass);

				JetPair = Vec_Jet1 + Vec_Jet2;

				//------------------------------------------------------------------
				// Lepton and MET Loop
				//------------------------------------------------------------------

				TLorentzVector Vec_Lepton;
				TLorentzVector Vec_MissingET;
				Int_t EtaCalc = 0;

				if (bElectron->GetEntriesFast() >= 1 || bMuon->GetEntriesFast() >= 1) {

					// Lepton
					if (bMuon->GetEntriesFast() == 0) {
						Electron* lepton = (Electron*)bElectron->At(0);
						Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, e_mass);
					}
					else if (bElectron->GetEntriesFast() == 0) {
						Muon* lepton = (Muon*)bMuon->At(0);
						Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, muon_mass);
					}
					else {
						Electron* electron = (Electron*)bElectron->At(0);
						Muon* muon = (Muon*)bMuon->At(0);
						if (electron->PT > muon->PT) {
							Electron* lepton = electron;
							Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, e_mass);
						}
						else {
							Muon* lepton = muon;
							Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, muon_mass);
						}
					}

					// MissingET 
					MissingET* missingET = (MissingET*)bMissingET->At(0);

					// Eta Calculation
					Double_t a = Vec_Lepton.E() - Vec_Lepton.Pz();
					Double_t b = TMath::Sq(W_mass) + TMath::Sq(Vec_Lepton.Px() + (missingET->MET * TMath::Cos(missingET->Phi))) + TMath::Sq(Vec_Lepton.Py() + (missingET->MET * TMath::Sin(missingET->Phi)));
					b += TMath::Sq(Vec_Lepton.Pz()) - TMath::Sq(Vec_Lepton.E()) - TMath::Sq(missingET->MET);
					b /= (-1 * missingET->MET);
					Double_t c = Vec_Lepton.E() + Vec_Lepton.Pz();
					Double_t det = TMath::Sq(b) - (4 * a * c);

					// Real solution for eta
					if (det >= 0) {
						EtaCalc = 1;

						Double_t eta_plus = TMath::Log((-b + TMath::Sqrt(det)) / (2 * a));
						Double_t eta_minus = TMath::Log((-b - TMath::Sqrt(det)) / (2 * a));

						if (abs(eta_plus) < abs(eta_minus)) {
							Vec_MissingET.SetPtEtaPhiM(missingET->MET, eta_plus, missingET->Phi, 0);
						}
						else {
							Vec_MissingET.SetPtEtaPhiM(missingET->MET, eta_minus, missingET->Phi, 0);
						}
					}

					// Imaginary solution for eta
					else {
						Double_t eta[1000]{}, deltaM[1000]{};
						Int_t i_min = 0;
						for (Int_t i = 0; i < 1000; i++) {
							eta[i] = -5.0 + (i * (10.0 / 1000));
							Double_t W_px = Vec_Lepton.Px() + Vec_MissingET.Px();
							Double_t W_py = Vec_Lepton.Py() + Vec_MissingET.Py();
							Double_t W_pz = Vec_Lepton.Pz() + Vec_MissingET.Pt() * TMath::SinH(eta[i]);
							Double_t W_E = Vec_Lepton.E() + Vec_MissingET.Pt() * TMath::CosH(eta[i]);
							Double_t W_m = TMath::Sqrt(TMath::Sq(W_E) - TMath::Sq(W_px) - TMath::Sq(W_py) - TMath::Sq(W_pz));
							deltaM[i] = abs(W_m - W_mass);
							if (deltaM[i] < deltaM[i_min]) {
								i_min = i;
							}
						}
						Vec_MissingET.SetPtEtaPhiM(missingET->MET, eta[i_min], missingET->Phi, 0);

						if (deltaM[i_min] < MET_DeltaM_cut) {
							EtaCalc = 1;
						}
					}

					if (EtaCalc == 1) {

						//------------------------------------------------------------------
						// Z' Reconstruction
						//------------------------------------------------------------------

						TLorentzVector LeptonandMET = Vec_Lepton + Vec_MissingET;
						TLorentzVector Zprime = JetPair + LeptonandMET;
						Double_t Rfunction;
						if (JetPair.Pt() < LeptonandMET.Pt()) {
							Rfunction = JetPair.Pt() / Zprime.M();
						}
						else {
							Rfunction = LeptonandMET.Pt() / Zprime.M();
						}
						// Setup histogram - pre cuts
						if (i == 0) {
							h_SignalReconMass_precuts->Fill(Zprime.M(), Event_Weight);
							h_SignalReconMass_Window_precuts->Fill(Zprime.M(), Event_Weight);
							h_JetPairMass_precuts->Fill(JetPair.M(), Event_Weight);
							if (JetPair.M() > JetPairMass_lowcut_signal && JetPair.M() < JetPairMass_highcut_signal) {
								JetPairMassSignalNumber += 1.0;
							}
							if (jet1->BTag == 0 && jet2->BTag == 0) {
								JetBTaggerSignalNumber += 1.0;
							}
							if (Vec_Lepton.Pt() > Lepton_pTcut_signal) {
								LeptonpTSignalNumber += 1.0;
							}
							if (Vec_MissingET.Pt() > MET_cut) {
								METSignalNumber += 1.0;
							}
						}
						else if (i > 0) {
							if (i == 1) {
								h_Background_WParton_ReconMass_precuts->Fill(Zprime.M(), Event_Weight);
								h_Background_WParton_ReconMass_Window_precuts->Fill(Zprime.M(), Event_Weight);
							}
							else if (i == 2) {
								h_Background_Diboson_ReconMass_precuts->Fill(Zprime.M(), Event_Weight);
								h_Background_Diboson_ReconMass_Window_precuts->Fill(Zprime.M(), Event_Weight);
							}
							else if (i == 3) {
								h_Background_ttbar_ReconMass_precuts->Fill(Zprime.M(), Event_Weight);
								h_Background_ttbar_ReconMass_Window_precuts->Fill(Zprime.M(), Event_Weight);
							}
							else if (i == 4) {
								h_Background_SingleTop_ReconMass_precuts->Fill(Zprime.M(), Event_Weight);
								h_Background_SingleTop_ReconMass_Window_precuts->Fill(Zprime.M(), Event_Weight);
							}
						}

						if (abs(JetPair.M() - W_mass) <= JetPairMass_cutwidth && JetPair.Pt() > HadronicW_pTcut && JetPair.M() > JetPairMass_lowcut_signal && JetPair.M() < JetPairMass_highcut_signal && jet1->BTag == 0 && jet2->BTag == 0 && Vec_Lepton.Pt() > Lepton_pTcut_signal && Vec_MissingET.Pt() > MET_cut){
							// Setup histogram - post cuts
							// 
							// 
							// bJet->GetEntriesFast() < Jet_NumLimit && 
							// LeptonandMET.Pt() > LeptonicW_pTcut && 
							// Vec_Lepton.Pt() > Lepton_pTcut_background && 
							// 

							if (i == 0) {
								h_SignalReconMass->Fill(Zprime.M(), Event_Weight);
								h_JetPairMass->Fill(JetPair.M(), Event_Weight);
								if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
									h_SignalReconMass_Window->Fill(Zprime.M(), Event_Weight);
									SignalNumber += Event_Weight;
								}
								for (Int_t j = 0; j < 80; j++) {
									if (JetPair.M() > W_mass - JetMassCutWidth_list[j] && JetPair.M() < W_mass + JetMassCutWidth_list[j]) {
										if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
											JetMassCutWidthSignalCount[j] += Event_Weight;
										}
									}
								}
								for (Int_t j = 0; j < 8; j++) {
									if (bJet->GetEntriesFast() < JetNumLimit_list[j]) {
										if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
											JetNumSignalCount[j] += Event_Weight;
										}
									}
								}
								for (Int_t j = 0; j < TMath::FloorNint(Zprime_Mass / 2.0); j++) {
									if (Vec_Lepton.Pt() > LeptonpTCut_list[j]) {
										if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
											LeptonpTSignalCount[j] += Event_Weight;
										}
									}
								}
								for (Int_t j = 0; j < TMath::FloorNint(Zprime_Mass / 2.0); j++) {
									if (LeptonandMET.Pt() > LeptonicWpTCut_list[j]) {
										if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
											LeptonicWpTSignalCount[j] += Event_Weight;
										}
									}
								}
								for (Int_t j = 0; j < TMath::FloorNint(Zprime_Mass / 2.0); j++) {
									if (JetPair.Pt() > HadronicWpTCut_list[j]) {
										if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
											HadronicWpTSignalCount[j] += Event_Weight;
										}
									}
								}
								for (Int_t j = 0; j < TMath::FloorNint(Zprime_Mass / 2.0) - 1; j++) {
									if (Zprime.M() >= Zprime_Mass - WindowSize_list[j] && Zprime.M() <= Zprime_Mass + WindowSize_list[j]) {
										WindowSize_SignalCount[j] += Event_Weight;
									}
								}
								for (Int_t j = 0; j < 50; j++) {
									if (Rfunction > RfunctionCut_list[j]) {
										if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
											RfunctionSignalCount[j] += Event_Weight;
										}
									}
								}
							}
							else if (i > 0) {
								if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
									BGNumber += Event_Weight;
									if (i == 1) {
										WPartonNumber += Event_Weight;
									}
									else if (i == 2) {
										DibosonNumber += Event_Weight;
									}
									else if (i == 3) {
										ttbarNumber += Event_Weight;
									}
									else if (i == 4) {
										SingleTopNumber += Event_Weight;
									}
								}
								if (i == 1) {
									h_Background_WParton_ReconMass->Fill(Zprime.M(), Event_Weight);
									h_Background_WParton_ReconMass_Window->Fill(Zprime.M(), Event_Weight);
								}
								else if (i == 2) {
									h_Background_Diboson_ReconMass->Fill(Zprime.M(), Event_Weight);
									h_Background_Diboson_ReconMass_Window->Fill(Zprime.M(), Event_Weight);
								}
								else if (i == 3) {
									h_Background_ttbar_ReconMass->Fill(Zprime.M(), Event_Weight);
									h_Background_ttbar_ReconMass_Window->Fill(Zprime.M(), Event_Weight);
								}
								else if (i == 4) {
									h_Background_SingleTop_ReconMass->Fill(Zprime.M(), Event_Weight);
									h_Background_SingleTop_ReconMass_Window->Fill(Zprime.M(), Event_Weight);
								}
								for (Int_t j = 0; j < 80; j++) {
									if (JetPair.M() > W_mass - JetMassCutWidth_list[j] && JetPair.M() < W_mass + JetMassCutWidth_list[j]) {
										if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
											JetMassCutWidthBackgroundCount[j] += Event_Weight;
										}
									}
								}
								for (Int_t j = 0; j < 8; j++) {
									if (bJet->GetEntriesFast() < JetNumLimit_list[j]) {
										if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
											JetNumBackgroundCount[j] += Event_Weight;
										}
									}
								}
								for (Int_t j = 0; j < TMath::FloorNint(Zprime_Mass / 2.0); j++) {
									if (Vec_Lepton.Pt() > LeptonpTCut_list[j]) {
										if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
											LeptonpTBackgroundCount[j] += Event_Weight;
										}
									}
								}
								for (Int_t j = 0; j < TMath::FloorNint(Zprime_Mass / 2.0); j++) {
									if (LeptonandMET.Pt() > LeptonicWpTCut_list[j]) {
										if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
											LeptonicWpTBackgroundCount[j] += Event_Weight;
										}
									}
								}
								for (Int_t j = 0; j < TMath::FloorNint(Zprime_Mass / 2.0); j++) {
									if (JetPair.Pt() > HadronicWpTCut_list[j]) {
										if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
											HadronicWpTBackgroundCount[j] += Event_Weight;
										}
									}
								}
								for (Int_t j = 0; j < TMath::FloorNint(Zprime_Mass / 2.0) - 1; j++) {
									if (Zprime.M() >= Zprime_Mass - WindowSize_list[j] && Zprime.M() <= Zprime_Mass + WindowSize_list[j]) {
										WindowSize_BackgroundCount[j] += Event_Weight;
									}
								}
								for (Int_t j = 0; j < 50; j++) {
									if (Rfunction > RfunctionCut_list[j]) {
										if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
											RfunctionBackgroundCount[j] += Event_Weight;
										}
									}
								}
							}
					
						} // Applying cuts
					
					} // Neutrino eta calculation

				} //Lepton and MET Loop

			} // Jet Loop

		} // Loop over all events

		delete reader;
	}

	Double_t Significance = SignalNumber / TMath::Sqrt(BGNumber);
	std::cout << SignalNumber << std::endl;
	std::cout << WPartonNumber << std::endl;
	std::cout << DibosonNumber << std::endl;
	std::cout << ttbarNumber << std::endl;
	std::cout << SingleTopNumber << std::endl;
	//std::cout << BGNumber << std::endl;
	//std::cout << Significance << std::endl;
	
	std::cout << "" << std::endl;
	std::cout << JetPairMassSignalNumber << std::endl;
	std::cout << JetBTaggerSignalNumber << std::endl;
	std::cout << LeptonpTSignalNumber << std::endl;
	std::cout << METSignalNumber << std::endl;

	Double_t JetMassCutWidthSignificance[80]{};
	for (Int_t i = 0; i < 80; i++) {
		if (JetMassCutWidthSignalCount[i] == 0 || JetMassCutWidthBackgroundCount[i] == 0) {
			JetMassCutWidthSignificance[i] = 0.0;
		}
		else {
			JetMassCutWidthSignificance[i] = JetMassCutWidthSignalCount[i] / TMath::Sqrt(JetMassCutWidthBackgroundCount[i]);
		}
	}

	Double_t JetNumSignificance[8]{};
	for (Int_t i = 0; i < 8; i++) {
		if (JetNumSignalCount[i] == 0 || JetNumBackgroundCount[i] == 0) {
			JetNumSignificance[i] = 0.0;
		}
		else {
			JetNumSignificance[i] = JetNumSignalCount[i] / TMath::Sqrt(JetNumBackgroundCount[i]);
		}
	}

	Double_t LeptonpTSignificance[TMath::FloorNint(Zprime_Mass/2.0)]{};
	for (Int_t i = 0; i < Zprime_Mass/2.0; i++) {
		if (LeptonpTSignalCount[i] == 0 || LeptonpTBackgroundCount[i] == 0) {
			LeptonpTSignificance[i] = 0.0;
		}
		else {
			LeptonpTSignificance[i] = LeptonpTSignalCount[i] / TMath::Sqrt(LeptonpTBackgroundCount[i]);
		}
	}

	Double_t LeptonicWpTSignificance[TMath::FloorNint(Zprime_Mass/2.0)]{};
	for (Int_t i = 0; i < Zprime_Mass/2.0; i++) {
		if (LeptonicWpTSignalCount[i] == 0 || LeptonicWpTBackgroundCount[i] == 0) {
			LeptonicWpTSignificance[i] = 0.0;
		}
		else {
			LeptonicWpTSignificance[i] = LeptonicWpTSignalCount[i] / TMath::Sqrt(LeptonicWpTBackgroundCount[i]);
		}
	}

	Double_t HadronicWpTSignificance[TMath::FloorNint(Zprime_Mass/2.0)]{};
	for (Int_t i = 0; i < Zprime_Mass/2.0; i++) {
		if (HadronicWpTSignalCount[i] == 0 || HadronicWpTBackgroundCount[i] == 0) {
			HadronicWpTSignificance[i] = 0.0;
		}
		else {
			HadronicWpTSignificance[i] = HadronicWpTSignalCount[i] / TMath::Sqrt(HadronicWpTBackgroundCount[i]);
		}
	}

	Double_t WindowSize_Significance[TMath::FloorNint(Zprime_Mass / 2.0) - 1]{};
	for (Int_t i = 0; i < TMath::FloorNint(Zprime_Mass / 2.0) - 1; i++) {
		if (WindowSize_SignalCount[i] == 0 || WindowSize_BackgroundCount[i] == 0) {
			WindowSize_Significance[i] = 0.0;
		}
		else {
			WindowSize_Significance[i] = WindowSize_SignalCount[i] / TMath::Sqrt(WindowSize_BackgroundCount[i]);
		}
	}
	
	Double_t RfunctionSignificance[50]{};
	for (Int_t i = 0; i < 50; i++) {
		if (RfunctionSignalCount[i] == 0 || RfunctionBackgroundCount[i] == 0) {
			RfunctionSignificance[i] = 0.0;
		}
		else {
			RfunctionSignificance[i] = RfunctionSignalCount[i] / TMath::Sqrt(RfunctionBackgroundCount[i]);
		}
	}


	TGraph* h_Significance_JetMassCutWidth = new TGraph(80, JetMassCutWidth_list, JetMassCutWidthSignificance);
	h_Significance_JetMassCutWidth->SetNameTitle("h_Significance_JetMassCutWidth", "; m_{jj} Cut Width [GeV]; Significance");
	h_Significance_JetMassCutWidth->Draw("AEP");

	TGraph * h_Significance_JetNum = new TGraph(8, JetNumLimit_list, JetNumSignificance);
	h_Significance_JetNum->SetNameTitle("h_Significance_JetNum", "; Jet Size Limit; Significance");
	h_Significance_JetNum->Draw("AEP");

	TGraph* h_Significance_LeptonpT = new TGraph(TMath::FloorNint(Zprime_Mass/2.0), LeptonpTCut_list, LeptonpTSignificance);
	h_Significance_LeptonpT->SetNameTitle("h_Significance_LeptonpT", "; Lepton p_{T} Cut [GeV]; Significance");
	h_Significance_LeptonpT->Draw("AEP");

	TGraph* h_Significance_LeptonicWpT = new TGraph(TMath::FloorNint(Zprime_Mass/2.0), LeptonicWpTCut_list, LeptonicWpTSignificance);
	h_Significance_LeptonicWpT->SetNameTitle("h_Significance_LeptonicWpT", "; Leptonic W p_{T} Cut [GeV]; Significance");
	h_Significance_LeptonicWpT->Draw("AEP");

	TGraph* h_Significance_HadronicWpT = new TGraph(TMath::FloorNint(Zprime_Mass/2.0), HadronicWpTCut_list, HadronicWpTSignificance);
	h_Significance_HadronicWpT->SetNameTitle("h_Significance_HadronicWpT", "; Hadronic W p_{T} Cut [GeV]; Significance");
	h_Significance_HadronicWpT->Draw("AEP");

	TGraph* h_Significance_WindowSize = new TGraph(TMath::FloorNint(Zprime_Mass / 2.0)-1, WindowSize_list, WindowSize_Significance);
	h_Significance_WindowSize->SetNameTitle("h_Significance_WindowSize", "; Mass Window Half-Width [GeV]; Significance");
	h_Significance_WindowSize->Draw("AEP");

	TGraph* h_Significance_Rfunction = new TGraph(50, RfunctionCut_list, RfunctionSignificance);
	h_Significance_Rfunction->SetNameTitle("h_Significance_Rfunction", "; R_{p_{T}/m}; Significance");
	h_Significance_Rfunction->Draw("AEP");

	h_Background_SingleTop_ReconMass_precuts->SetOption("HIST");
	h_Background_ttbar_ReconMass_precuts->SetOption("HIST");
	h_Background_Diboson_ReconMass_precuts->SetOption("HIST");
	h_Background_WParton_ReconMass_precuts->SetOption("HIST");
	h_SignalReconMass_precuts->SetOption("HIST");

	h_Background_SingleTop_ReconMass_Window_precuts->SetOption("HIST");
	h_Background_ttbar_ReconMass_Window_precuts->SetOption("HIST");
	h_Background_Diboson_ReconMass_Window_precuts->SetOption("HIST");
	h_Background_WParton_ReconMass_Window_precuts->SetOption("HIST");
	h_SignalReconMass_Window_precuts->SetOption("HIST");

	h_JetPairMass_precuts->SetOption("HIST");

	h_Background_SingleTop_ReconMass->SetOption("HIST");
	h_Background_ttbar_ReconMass->SetOption("HIST");
	h_Background_Diboson_ReconMass->SetOption("HIST");
	h_Background_WParton_ReconMass->SetOption("HIST");
	h_SignalReconMass->SetOption("HIST");

	h_Background_SingleTop_ReconMass_Window->SetOption("HIST");
	h_Background_ttbar_ReconMass_Window->SetOption("HIST");
	h_Background_Diboson_ReconMass_Window->SetOption("HIST");
	h_Background_WParton_ReconMass_Window->SetOption("HIST");
	h_SignalReconMass_Window->SetOption("HIST");

	h_JetPairMass->SetOption("HIST");

	h_ZprimeMass_precuts->Add(h_Background_SingleTop_ReconMass_precuts);
	h_ZprimeMass_precuts->Add(h_Background_ttbar_ReconMass_precuts);
	h_ZprimeMass_precuts->Add(h_Background_Diboson_ReconMass_precuts);
	h_ZprimeMass_precuts->Add(h_Background_WParton_ReconMass_precuts);
	h_ZprimeMass_precuts->Add(h_SignalReconMass_precuts);

	h_ZprimeMass_Window_precuts->Add(h_Background_SingleTop_ReconMass_Window_precuts);
	h_ZprimeMass_Window_precuts->Add(h_Background_ttbar_ReconMass_Window_precuts);
	h_ZprimeMass_Window_precuts->Add(h_Background_Diboson_ReconMass_Window_precuts);
	h_ZprimeMass_Window_precuts->Add(h_Background_WParton_ReconMass_Window_precuts);
	h_ZprimeMass_Window_precuts->Add(h_SignalReconMass_Window_precuts);
	
	h_ZprimeMass->Add(h_Background_SingleTop_ReconMass);
	h_ZprimeMass->Add(h_Background_ttbar_ReconMass);
	h_ZprimeMass->Add(h_Background_Diboson_ReconMass);
	h_ZprimeMass->Add(h_Background_WParton_ReconMass);
	h_ZprimeMass->Add(h_SignalReconMass);

	h_ZprimeMass_Window->Add(h_Background_SingleTop_ReconMass_Window);
	h_ZprimeMass_Window->Add(h_Background_ttbar_ReconMass_Window);
	h_ZprimeMass_Window->Add(h_Background_Diboson_ReconMass_Window);
	h_ZprimeMass_Window->Add(h_Background_WParton_ReconMass_Window);
	h_ZprimeMass_Window->Add(h_SignalReconMass_Window);

	TCanvas* c_WWprimeMass_precuts = new TCanvas("c_WWprimeMass_precuts", "", 1000, 1000);
	gStyle->SetPalette(1);
	c_WWprimeMass_precuts->cd();
	c_WWprimeMass_precuts->DrawFrame(0, 0, 1, 1);
	h_ZprimeMass_precuts->Draw("PFC");
	gPad->BuildLegend(0.75, 0.75, 0.95, 0.95, "");
	TLatex Text_precuts;
	Text_precuts.SetTextFont(42);
	Text_precuts.SetTextSize(0.04);
	Text_precuts.SetTextAlign(12);
	Text_precuts.DrawLatex(90, h_ZprimeMass_precuts->GetMaximum() * 0.97, "#sqrt{s} = 13 TeV");
	Text_precuts.DrawLatex(90, h_ZprimeMass_precuts->GetMaximum() * 0.85, "#int L dt = 139 fb^{-1}");
	Text_precuts.DrawLatex(90, h_ZprimeMass_precuts->GetMaximum() * 0.77, "DY Z'#rightarrow WW");

	TCanvas* c_WWprimeMass_Window_precuts = new TCanvas("c_WWprimeMass_Window_precuts", "", 1000, 1000);
	gStyle->SetPalette(1);
	c_WWprimeMass_Window_precuts->Divide(2, 1);
	c_WWprimeMass_Window_precuts->cd(1);
	h_ZprimeMass_precuts->Draw("PFC");
	gPad->BuildLegend(0.75, 0.75, 0.95, 0.95, "");
	c_WWprimeMass_Window_precuts->cd(2);
	h_ZprimeMass_Window_precuts->Draw("PFC");
	gPad->BuildLegend(0.75, 0.75, 0.95, 0.95, "");

	TCanvas* c_WWprimeMass = new TCanvas("c_WWprimeMass", "", 1000, 1000);
	gStyle->SetPalette(1);
	c_WWprimeMass->cd();
	c_WWprimeMass->DrawFrame(0, 0, 1, 1);
	h_ZprimeMass->Draw("PFC");
	gPad->BuildLegend(0.75, 0.75, 0.95, 0.95, "");
	TLatex Text;
	Text.SetTextFont(42);
	Text.SetTextSize(0.04);
	Text.SetTextAlign(12);
	Text.DrawLatex(90, h_ZprimeMass->GetMaximum() * 0.97, "#sqrt{s} = 13 TeV");
	Text.DrawLatex(90, h_ZprimeMass->GetMaximum() * 0.85, "#int L dt = 139 fb^{-1}");
	Text.DrawLatex(90, h_ZprimeMass->GetMaximum() * 0.77, "DY Z'#rightarrow WW");
	TString Sig = "#sigma = ";
	Sig.Insert(9, Significance);
	//Text.DrawLatex(90, h_ZprimeMass->GetMaximum() * 0.69, Sig);

	TCanvas* c_WWprimeMass_Window = new TCanvas("c_WWprimeMass_Window", "", 1000, 1000);
	gStyle->SetPalette(1);
	c_WWprimeMass_Window->Divide(2, 1);
	c_WWprimeMass_Window->cd(1);
	h_ZprimeMass->Draw("PFC");
	gPad->BuildLegend(0.75, 0.75, 0.95, 0.95, "");
	c_WWprimeMass_Window->cd(2);
	h_ZprimeMass_Window->Draw("PFC");
	gPad->BuildLegend(0.75, 0.75, 0.95, 0.95, "");


	// Writing to File

	std::cout << "Events in EventCount: " << h_EventCount->GetEntries() << std::endl;

	std::cout << "Write to file..." << std::endl;

	OutputFile->cd();

	h_EventCount->Write();
	h_WeightCount->Write();

	h_ZprimeMass_precuts->Write();
	h_ZprimeMass_Window_precuts->Write();
	c_WWprimeMass_precuts->Write();
	c_WWprimeMass_Window_precuts->Write();

	h_SignalReconMass_precuts->Write();
	h_Background_WParton_ReconMass_precuts->Write();
	h_Background_Diboson_ReconMass_precuts->Write();
	h_Background_ttbar_ReconMass_precuts->Write();
	h_Background_SingleTop_ReconMass_precuts->Write();

	h_JetPairMass_precuts->Write();

	h_Significance_HadronicWpT->Write();
	h_Significance_JetMassCutWidth->Write();
	h_Significance_LeptonicWpT->Write();
	h_Significance_Rfunction->Write();
	h_Significance_LeptonpT->Write();
	h_Significance_JetNum->Write();
	h_Significance_WindowSize->Write();
	
	h_ZprimeMass->Write();
	h_ZprimeMass_Window->Write();
	c_WWprimeMass->Write();
	c_WWprimeMass_Window->Write();

	h_SignalReconMass->Write();
	h_Background_WParton_ReconMass->Write();
	h_Background_Diboson_ReconMass->Write();
	h_Background_ttbar_ReconMass->Write();
	h_Background_SingleTop_ReconMass->Write();

	h_JetPairMass->Write();

	OutputFile->Close();

	std::cout << "Tidy..." << std::endl;

	std::cout << "Done!" << std::endl;

	return 0;

}

ExRootTreeReader * InitReader(const TString FilePath) {

	std::cout << "InitReader: " << FilePath << std::endl;

	TFile * f = TFile::Open(FilePath);

	TChain * Chain = new TChain("Delphes","");

	Chain->Add(FilePath);

	// Create object of class ExRootTreeReader
	ExRootTreeReader * r = new ExRootTreeReader(Chain);

	return r;
}
