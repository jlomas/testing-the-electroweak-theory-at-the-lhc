
#include "Process.h"

bool Debug = false;

int main(int argc, char* argv[]) {

    // Input Delphes File

    const TString InputFile = argv[1];
    const TString OutputFileName = argv[2];

    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "Running Process"  << std::endl;
    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "InputFile = " << InputFile << std::endl;
    std::cout << "OutputFileName = " << OutputFileName << std::endl;
    std::cout << "-------------------------------------------------------------"  << std::endl;

    ExRootTreeReader * reader = NULL;
    reader = InitReader(InputFile);

    //------------------------------------
    // Declare the output
    //------------------------------------

    OutputFile = new TFile(OutputFileName,"recreate");

    OutputFile->cd();

    h_EventCount = new TH1D("h_EventCount","",1,0,1);
    h_WeightCount = new TH1D("h_WeightCount","",1,0,1);

    h_JetNum = new TH1D("h_JetNum","; Number of Reconstructed Jets in Event; Events",10,0.0,10.0);
    h_ElectronNum = new TH1D("h_ElectronNum","; Number of Reconstructed Electrons in Event; Events",10,0.0,10.0);
    h_TruthElectronNum = new TH1D("h_TruthElectronNum","; Number of Truth Electrons in Event; Events",10,0.0,10.0);
    h_MuonNum = new TH1D("h_MuonNum","; Number of Reconstructed Muons in Event; Events",10,0.0,10.0);
    h_TruthMuonNum = new TH1D("h_TruthMuonNum","; Number of Truth Muons in Event; Events",10,0.0,10.0);
    h_LeptonNum = new TH1D("h_LeptonNum","; Number of Reconstructed Leptons in Event; Events",10,0.0,10.0);
    h_TruthLeptonNum = new TH1D("h_TruthLeptonNum","; Number of Truth Leptons in Event; Events",10,0.0,10.0);
    h_TruthTauNum = new TH1D("h_TruthTauNum","; Number of Truth Taus in Event; Events",10,0.0,10.0);
    h_JetDeltaR = new TH1D("h_JetDeltaR ","; #DeltaR of Jet; Events",200,0.0,1.0);
    h_VisibleLeptonSources = new TH1D("h_VisibleLeptonSources ","; Lepton Source; Events",6,0.5,6.5);
    h_ElectronSources = new TH1D("h_ElectronSources ","; Electron Source; Events",6,0.5,6.5);
    h_MuonSources = new TH1D("h_MuonSources ","; Muon Source; Events",6,0.5,6.5);
    h_WElectrons = new TH1D("h_WElectrons ","; Electron p_{T} [GeV]; Events / 5 GeV",200,0.0,1000.0);
    h_LeptonElectrons = new TH1D("h_LeptonElectrons ","; Electron p_{T} [GeV]; Events / 5 GeV",200,0.0,1000.0);
    h_MesonElectrons = new TH1D("h_MesonElectrons ","; Electron p_{T} [GeV]; Events / 5 GeV",200,0.0,1000.0);
    h_BaryonElectrons = new TH1D("h_BaryonElectrons ","; Electron p_{T} [GeV]; Events / 5 GeV",200,0.0,1000.0);
    h_PhotonElectrons = new TH1D("h_PhotonElectrons ","; Electron p_{T} [GeV]; Events / 5 GeV",200,0.0,1000.0);
    h_WMuons = new TH1D("h_WMuons ","; Muon p_{T} [GeV]; Events / 5 GeV",200,0.0,1000.0);
    h_LeptonMuons = new TH1D("h_LeptonMuons ","; Muon p_{T} [GeV]; Events / 5 GeV",200,0.0,1000.0);
    h_MesonMuons = new TH1D("h_MesonMuons ","; Muon p_{T} [GeV]; Events / 5 GeV",200,0.0,1000.0);
    h_BaryonMuons = new TH1D("h_BaryonMuons ","; Muon p_{T} [GeV]; Events / 5 GeV",200,0.0,1000.0);
    h_PhotonMuons = new TH1D("h_PhotonMuons ","; Muon p_{T} [GeV]; Events / 5 GeV",200,0.0,1000.0);

    //------------------------------------

    // Run the selection
    Process(reader);

    std::cout << "Events in EventCount: " << h_EventCount->GetEntries() << std::endl;

    std::cout << "Write to file..." << std::endl;

    OutputFile->cd();

    h_EventCount->Write();
    h_WeightCount->Write();

    h_JetNum->Write();
    h_ElectronNum->Write();
    h_TruthElectronNum->Write();
    h_MuonNum->Write();
    h_TruthMuonNum->Write();
    h_LeptonNum->Write();
    h_TruthLeptonNum->Write();
    h_TruthTauNum->Write();
    h_JetDeltaR->Write();
    h_VisibleLeptonSources->Write();
    h_ElectronSources->Write();
    h_MuonSources->Write();
    h_WElectrons->Write();
    h_LeptonElectrons->Write();
    h_MesonElectrons->Write();
    h_BaryonElectrons->Write();
    h_PhotonElectrons->Write();
    h_WMuons->Write();
    h_LeptonMuons->Write();
    h_MesonMuons->Write();
    h_BaryonMuons->Write();
    h_PhotonMuons->Write();

    OutputFile->Close();

    std::cout << "Tidy..." << std::endl;

    delete reader;

    std::cout << "Done!" << std::endl;

    return 0;

}

ExRootTreeReader * InitReader(const TString FilePath) {

    std::cout << "InitReader" << std::endl;

    TFile * f = TFile::Open(FilePath);

    TChain * Chain = new TChain("Delphes","");

    Chain->Add(FilePath);

    // Create object of class ExRootTreeReader
    ExRootTreeReader * r = new ExRootTreeReader(Chain);

    return r;
}

void Process(ExRootTreeReader * treeReader) {

    // Get pointers to branches used in this analysis
    bEvent = treeReader->UseBranch("Event");
    bJet = treeReader->UseBranch("Jet");
    bElectron = treeReader->UseBranch("Electron");
    bMuon = treeReader->UseBranch("Muon");
    bTruthLeptons = treeReader->UseBranch("TruthLeptonParticles");
    bParticle = treeReader->UseBranch("Particle");

    Long64_t numberOfEntries = treeReader->GetEntries();

    int nSelected = 0;

    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "Input: " << numberOfEntries << " events to process" << std::endl;

    Long64_t W_num = 0;
    Long64_t lepton_num = 0;
    Long64_t meson_num = 0;
    Long64_t baryon_num = 0;
    Long64_t gamma_num = 0;
    Long64_t other_num = 0;
    std::vector<Long64_t> other_PID;
    Long64_t has_M2 = 0;
    Long64_t total_num = 0;

    Long64_t ElectronCut = 65;
    Long64_t W_electron_num = 0;
    Long64_t lepton_electron_num = 0;
    Long64_t meson_electron_num = 0;
    Long64_t baryon_electron_num = 0;
    Long64_t gamma_electron_num = 0;
    Long64_t other_electron_num = 0;
    Long64_t total_electron_num = 0;
    Long64_t W_electron_passnum = 0;
    Long64_t lepton_electron_passnum = 0;
    Long64_t meson_electron_passnum = 0;
    Long64_t baryon_electron_passnum = 0;
    Long64_t gamma_electron_passnum = 0;

    Long64_t MuonCut = 75;
    Long64_t W_muon_num = 0;
    Long64_t lepton_muon_num = 0;
    Long64_t meson_muon_num = 0;
    Long64_t baryon_muon_num = 0;
    Long64_t gamma_muon_num = 0;
    Long64_t other_muon_num = 0;
    Long64_t total_muon_num = 0;
    Long64_t W_muon_passnum = 0;
    Long64_t lepton_muon_passnum = 0;
    Long64_t meson_muon_passnum = 0;
    Long64_t baryon_muon_passnum = 0;
    Long64_t gamma_muon_passnum = 0;

    std::vector<double_t> W_electrons;
    std::vector<double_t> other_electrons;
    std::vector<double_t> W_muons;
    std::vector<double_t> other_muons;

    // Loop over all events
    for(Int_t entry = 0; entry < numberOfEntries; ++entry) {

        // Load selected branches with data from specified event
        treeReader->ReadEntry(entry);

        HepMCEvent * event = (HepMCEvent*) bEvent->At(0);
    	const float Event_Weight = event->Weight;

        h_EventCount->Fill(0.5);
        h_WeightCount->Fill(0.5,Event_Weight);

        if( (entry > 0 && entry%1000 == 0) || Debug) {
            std::cout << "-------------------------------------------------------------"  << std::endl;
            std::cout << "Processing Event Number =  " << entry  << std::endl;
            std::cout << "-------------------------------------------------------------"  << std::endl;
        }

        //------------------------------------------------------------------
        // Jet Loop
        //------------------------------------------------------------------

	h_JetNum->Fill( bJet->GetEntriesFast(), Event_Weight );

	for(int i = 0; i < bJet->GetEntriesFast(); ++i) {

            Jet* jet = (Jet*) bJet->At(i);
            h_JetDeltaR->Fill( TMath::Sqrt(TMath::Sq(jet->DeltaEta)+TMath::Sq(jet->DeltaPhi)), Event_Weight );

        } // Jet Loop

        //------------------------------------------------------------------
        // Electron Loop
        //------------------------------------------------------------------
        
	h_ElectronNum->Fill( bElectron->GetEntriesFast(), Event_Weight );

	//------------------------------------------------------------------
        // Muon Loop
        //------------------------------------------------------------------
        
	h_MuonNum->Fill( bMuon->GetEntriesFast(), Event_Weight );
	h_LeptonNum->Fill( bElectron->GetEntriesFast() + bMuon->GetEntriesFast(), Event_Weight );

	//------------------------------------------------------------------
        // Truth Lepton Loop
        //------------------------------------------------------------------
	
	Long64_t TruthElectronNum = 0;
	Long64_t TruthMuonNum = 0;
	Long64_t TruthTauNum = 0;
	Long64_t TruthLeptonNum = 0;
	
	for(int i = 0; i < bTruthLeptons->GetEntriesFast(); ++i) {

	    std::vector<Long64_t> PID;

            GenParticle* lep = (GenParticle*) bTruthLeptons->At(i);

	    //Sort out electrons, muons and taus
	    if( abs(lep->PID) == 11) {
    		TruthElectronNum += 1;
    		TruthLeptonNum += 1;
	    }
	    if( abs(lep->PID) == 13) {
    		TruthMuonNum += 1;
    		TruthLeptonNum += 1;
	    }
	    if( abs(lep->PID) == 15) {
    		TruthTauNum += 1;
    		TruthLeptonNum += 1;
	    }

	    if (abs(lep->PID) == 11 || abs(lep->PID) == 13 || abs(lep->PID) == 15) {
	        GenParticle* M1 = (GenParticle*) bParticle->At(lep->M1);
	        total_num += 1;
	        PID.push_back(abs(M1->PID));
	        if (lep->M2 > 0) {
		    has_M2 += 1;
		    total_num += 1;
		    GenParticle* M2 = (GenParticle*) bParticle->At(lep->M2);
		    PID.push_back(abs(M2->PID));
	        }

	        std::vector<TString> M_Part;

    	        for(int j = 0; j < PID.size(); ++j) {
	            if (PID.at(j) == 24) {
		        M_Part.push_back("W");
			W_num += 1;
			h_VisibleLeptonSources->Fill(0.5, Event_Weight);
			if (abs(lep->PID) == 11 && PID.size() == 1) {
			    h_ElectronSources->Fill(0.5, Event_Weight);
			    h_WElectrons->Fill(lep->PT, Event_Weight);
			    W_electron_num += 1;
			    total_electron_num += 1;
			    W_electrons.push_back(lep->PT);
		            if (lep->PT > ElectronCut) {
				W_electron_passnum += 1;
			    }
			}
			if (abs(lep->PID) == 13 && PID.size() == 1) {
			    h_MuonSources->Fill(0.5, Event_Weight);
			    h_WMuons->Fill(lep->PT, Event_Weight);
			    W_muon_num += 1;
			    total_muon_num += 1;
			    W_muons.push_back(lep->PT);
		            if (lep->PT > MuonCut) {
				W_muon_passnum += 1;
			    }
			}
	            }
	            else if (PID.at(j) == 11 || PID.at(j) == 13 || PID.at(j) == 15) {
		        if (PID.at(j) == 11) {
			    M_Part.push_back("Electron");
			}
		        if (PID.at(j) == 13) {
			    M_Part.push_back("Muon");
			}
		        if (PID.at(j) == 15) {
			    M_Part.push_back("Tau");
			}
		        lepton_num += 1;
			h_VisibleLeptonSources->Fill(1.5, Event_Weight);
			if (abs(lep->PID) == 11 && PID.size() == 1) {
			    h_ElectronSources->Fill(1.5, Event_Weight);
			    h_LeptonElectrons->Fill(lep->PT, Event_Weight);
			    lepton_electron_num += 1;
			    total_electron_num += 1;
			    other_electrons.push_back(lep->PT);
		            if (lep->PT > ElectronCut) {
				lepton_electron_passnum += 1;
			    }
			}
			if (abs(lep->PID) == 13 && PID.size() == 1) {
			    h_MuonSources->Fill(1.5, Event_Weight);
			    h_LeptonMuons->Fill(lep->PT, Event_Weight);
			    lepton_muon_num += 1;
			    total_muon_num += 1;
			    other_muons.push_back(lep->PT);
		            if (lep->PT > MuonCut) {
				lepton_muon_passnum += 1;
			    }
			}
	            }
	            else if (PID.at(j) == 111 || PID.at(j) == 113 || PID.at(j) == 221 || PID.at(j) == 223 || PID.at(j) == 310 || PID.at(j) == 331 || PID.at(j) == 333 || PID.at(j) == 411 || PID.at(j) == 421 || PID.at(j) == 431 || PID.at(j) == 443 || PID.at(j) == 511 || PID.at(j) == 521 || PID.at(j) == 531) {
		        M_Part.push_back("Meson");
		        meson_num += 1;
			h_VisibleLeptonSources->Fill(2.5, Event_Weight);
			if (abs(lep->PID) == 11 && PID.size() == 1) {
			    h_ElectronSources->Fill(2.5, Event_Weight);
			    h_MesonElectrons->Fill(lep->PT, Event_Weight);
			    meson_electron_num += 1;
			    total_electron_num += 1;
			    other_electrons.push_back(lep->PT);
		            if (lep->PT > ElectronCut) {
				meson_electron_passnum += 1;
			    }
			}
			if (abs(lep->PID) == 13 && PID.size() == 1) {
			    h_MuonSources->Fill(2.5, Event_Weight);
			    h_MesonMuons->Fill(lep->PT, Event_Weight);
			    meson_muon_num += 1;
			    total_muon_num += 1;
			    other_muons.push_back(lep->PT);
		            if (lep->PT > MuonCut) {
				meson_muon_passnum += 1;
			    }
			}
	            }
	            else if (PID.at(j) == 3112 || PID.at(j) == 3122 || PID.at(j) == 3212 || PID.at(j) == 4122 || PID.at(j) == 4132 || PID.at(j) == 4232 || PID.at(j) == 5122 || PID.at(j) == 5132 || PID.at(j) == 5232) {
		        M_Part.push_back("Baryon");
		        baryon_num += 1;
			h_VisibleLeptonSources->Fill(3.5, Event_Weight);
			if (abs(lep->PID) == 11 && PID.size() == 1) {
			    h_ElectronSources->Fill(3.5, Event_Weight);
			    h_BaryonElectrons->Fill(lep->PT, Event_Weight);
			    baryon_electron_num += 1;
			    total_electron_num += 1;
			    other_electrons.push_back(lep->PT);
		            if (lep->PT > ElectronCut) {
				baryon_electron_passnum += 1;
			    }
			}
			if (abs(lep->PID) == 13 && PID.size() == 1) {
			    h_MuonSources->Fill(3.5, Event_Weight);
			    h_BaryonMuons->Fill(lep->PT, Event_Weight);
			    baryon_muon_num += 1;
			    total_muon_num += 1;
			    other_muons.push_back(lep->PT);
		            if (lep->PT > MuonCut) {
				baryon_muon_passnum += 1;
			    }
			}

	            }
	            else if (PID.at(j) == 22) {
		        M_Part.push_back("Photon");
		        gamma_num += 1;
			h_VisibleLeptonSources->Fill(4.5, Event_Weight);
			if (abs(lep->PID) == 11 && PID.size() == 1) {
			    h_ElectronSources->Fill(4.5, Event_Weight);
			    h_PhotonElectrons->Fill(lep->PT, Event_Weight);
			    gamma_electron_num += 1;
			    total_electron_num += 1;
			    other_electrons.push_back(lep->PT);
		            if (lep->PT > ElectronCut) {
				gamma_electron_passnum += 1;
			    }
			}
			if (abs(lep->PID) == 13 && PID.size() == 1) {
			    h_MuonSources->Fill(4.5, Event_Weight);
			    h_PhotonMuons->Fill(lep->PT, Event_Weight);
			    gamma_muon_num += 1;
			    total_muon_num += 1;
			    other_muons.push_back(lep->PT);
		            if (lep->PT > MuonCut) {
				gamma_muon_passnum += 1;
			    }
			}
	            }
	            else {
		        other_num += 1;
			h_VisibleLeptonSources->Fill(5.5, Event_Weight);
			if (abs(lep->PID) == 11 ) {
			    h_ElectronSources->Fill(5.5, Event_Weight);
			    other_electron_num += 1;
			}
			if (abs(lep->PID) == 11 ) {
			    h_MuonSources->Fill(5.5, Event_Weight);
			    other_muon_num += 1;
			}
		        M_Part.push_back("None");
		        Long64_t Curr_PID = PID.at(j);
		        Long64_t Match = 0;
		        for(int k = 0; k < other_PID.size(); ++k) {
		            if (other_PID.at(k) == Curr_PID) {
			        Match += 1;
		            }
		        }
		        if (Match == 0) {
		            other_PID.push_back(Curr_PID);
		        }
	            }
	        }

	        if (entry < 100 || PID.size() > 1) {
		    TString M1_Out = M_Part.at(0);
		    if (M1_Out == "None") {
		        Long64_t M1_Out = PID.at(0);
		    }
		    if (PID.size() > 1){
		        TString M2_Out = M_Part.at(1);
		        if (M2_Out == "None") {
			    Long64_t M2_Out = PID.at(1);
		        }
		        if (abs(lep->PID) == 11) {
			    std::cout << "Event " << entry+1 << ", Lepton " << i+1 << ", Electron " << TruthElectronNum << ": M1 = " << M1_Out << ", M2 = " << M2_Out << std::endl;
		        }
		        else if (abs(lep->PID) == 13) {
			    std::cout << "Event " << entry+1 << ", Lepton " << i+1 << ", Muon " << TruthMuonNum << ": M1 = " << M1_Out << ", M2 = " << M2_Out << std::endl;
		        }
		        else if (abs(lep->PID) == 15) {
			    std::cout << "Event " << entry+1 << ", Lepton " << i+1 << ", Tau " << TruthTauNum << ": M1 = " << M1_Out << ", M2 = " << M2_Out << std::endl;
		        }
		    }
		    else if (entry < 500) {
		        if (abs(lep->PID) == 11) {
			    std::cout << "Event " << entry+1 << ", Lepton " << i+1 << ", Electron " << TruthElectronNum << ": M1 = " << M1_Out << std::endl;
		        }
		        else if (abs(lep->PID) == 13) {
			    std::cout << "Event " << entry+1 << ", Lepton " << i+1 << ", Muon " << TruthMuonNum << ": M1 = " << M1_Out << std::endl;
		        }
		        else if (abs(lep->PID) == 15) {
			    std::cout << "Event " << entry+1 << ", Lepton " << i+1 << ", Tau " << TruthTauNum << ": M1 = " << M1_Out << std::endl;
		        }
		    }
                }
	    }
        } // Truth Lepton Loop

	h_TruthElectronNum->Fill( TruthElectronNum, Event_Weight );
	h_TruthMuonNum->Fill( TruthMuonNum, Event_Weight );
	h_TruthTauNum->Fill( TruthTauNum, Event_Weight );
	h_TruthLeptonNum->Fill( TruthLeptonNum, Event_Weight );

    } // Loop over all events

    double_t W_num_double = W_num;
    double_t lepton_num_double = lepton_num;
    double_t meson_num_double = meson_num;
    double_t baryon_num_double = baryon_num;
    double_t gamma_num_double = gamma_num;
    double_t other_num_double = other_num;
    double_t has_M2_double = has_M2;
    double_t total_num_double = total_num;

    double_t W_electron_num_double = W_electron_num;
    double_t lepton_electron_num_double = lepton_electron_num;
    double_t meson_electron_num_double = meson_electron_num;
    double_t baryon_electron_num_double = baryon_electron_num;
    double_t gamma_electron_num_double = gamma_electron_num;
    double_t other_electron_num_double = other_electron_num;
    double_t total_electron_num_double = total_electron_num;
    double_t W_electron_passnum_double = W_electron_passnum;
    double_t lepton_electron_passnum_double = lepton_electron_passnum;
    double_t meson_electron_passnum_double = meson_electron_passnum;
    double_t baryon_electron_passnum_double = baryon_electron_passnum;
    double_t gamma_electron_passnum_double = gamma_electron_passnum;

    double_t W_muon_num_double = W_muon_num;
    double_t lepton_muon_num_double = lepton_muon_num;
    double_t meson_muon_num_double = meson_muon_num;
    double_t baryon_muon_num_double = baryon_muon_num;
    double_t gamma_muon_num_double = gamma_muon_num;
    double_t other_muon_num_double = other_muon_num;
    double_t total_muon_num_double = total_muon_num;
    double_t W_muon_passnum_double = W_muon_passnum;
    double_t lepton_muon_passnum_double = lepton_muon_passnum;
    double_t meson_muon_passnum_double = meson_muon_passnum;
    double_t baryon_muon_passnum_double = baryon_muon_passnum;
    double_t gamma_muon_passnum_double = gamma_muon_passnum;

    std::cout << "*************************************************************"  << std::endl;
    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "Lepton Sources" << std::endl;
    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "W: " << W_num << " (" << (W_num_double/total_num_double)*100 << "%)" << std::endl;
    std::cout << "Lepton: " << lepton_num << " (" << (lepton_num_double/total_num_double)*100 << "%)" << std::endl;
    std::cout << "Meson: " << meson_num << " (" << (meson_num_double/total_num_double)*100 << "%)" << std::endl;
    std::cout << "Baryon: " << baryon_num << " (" << (baryon_num_double/total_num_double)*100 << "%)" << std::endl;
    std::cout << "Photon: " << gamma_num << " (" << (gamma_num_double/total_num_double)*100 << "%)" << std::endl;
    std::cout << "Other: " << other_num << " (" << (other_num_double/total_num_double)*100 << "%)" << std::endl;
    std::cout << "Other PID: " << std::endl;
    for(int i = 0; i < other_PID.size(); ++i) {
	std::cout << other_PID.at(i) << std::endl;
    }
    std::cout << "*************************************************************"  << std::endl;
    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "Electron Sources" << std::endl;
    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "W: " << W_electron_num << " (" << (W_electron_num_double/total_electron_num_double)*100 << "%)" << std::endl;
    std::cout << "Lepton: " << lepton_electron_num << " (" << (lepton_electron_num_double/total_electron_num_double)*100 << "%)" << std::endl;
    std::cout << "Meson: " << meson_electron_num << " (" << (meson_electron_num_double/total_electron_num_double)*100 << "%)" << std::endl;
    std::cout << "Baryon: " << baryon_electron_num << " (" << (baryon_electron_num_double/total_electron_num_double)*100 << "%)" << std::endl;
    std::cout << "Photon: " << gamma_electron_num << " (" << (gamma_electron_num_double/total_electron_num_double)*100 << "%)" << std::endl;
    std::cout << "Other: " << other_electron_num << " (" << (other_electron_num_double/total_electron_num_double)*100 << "%)" << std::endl;

    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "Muon Sources" << std::endl;
    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "W: " << W_muon_num << " (" << (W_muon_num_double/total_muon_num_double)*100 << "%)" << std::endl;
    std::cout << "Lepton: " << lepton_muon_num << " (" << (lepton_muon_num_double/total_muon_num_double)*100 << "%)" << std::endl;
    std::cout << "Meson: " << meson_muon_num << " (" << (meson_muon_num_double/total_muon_num_double)*100 << "%)" << std::endl;
    std::cout << "Baryon: " << baryon_muon_num << " (" << (baryon_muon_num_double/total_muon_num_double)*100 << "%)" << std::endl;
    std::cout << "Photon: " << gamma_muon_num << " (" << (gamma_muon_num_double/total_muon_num_double)*100 << "%)" << std::endl;
    std::cout << "Other: " << other_muon_num << " (" << (other_muon_num_double/total_muon_num_double)*100 << "%)" << std::endl;
    std::cout << "-------------------------------------------------------------"  << std::endl;

    std::cout << "*************************************************************"  << std::endl;
    std::cout << "Has M2: " << has_M2 << " (" << (has_M2_double/total_num_double)*100 << "%)" << std::endl;
    std::cout << "*************************************************************"  << std::endl;

    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "Electron p_T Cut = " << ElectronCut << " GeV" << std::endl;
    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "W: " << W_electron_passnum << " (" << (W_electron_passnum_double/W_electron_num_double)*100 << "%)" << std::endl;
    std::cout << "Lepton: " << lepton_electron_passnum << " (" << (lepton_electron_passnum_double/lepton_electron_num_double)*100 << "%)" << std::endl;
    std::cout << "Meson: " << meson_electron_passnum << " (" << (meson_electron_passnum_double/meson_electron_num_double)*100 << "%)" << std::endl;
    std::cout << "Baryon: " << baryon_electron_passnum << " (" << (baryon_electron_passnum_double/baryon_electron_num_double)*100 << "%)" << std::endl;
    std::cout << "Photon: " << gamma_electron_passnum << " (" << (gamma_electron_passnum_double/gamma_electron_num_double)*100 << "%)" << std::endl;
    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "Muon p_T Cut = " << MuonCut << " GeV" << std::endl;
    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "W: " << W_muon_passnum << " (" << (W_muon_passnum_double/W_muon_num_double)*100 << "%)" << std::endl;
    std::cout << "Lepton: " << lepton_muon_passnum << " (" << (lepton_muon_passnum_double/lepton_muon_num_double)*100 << "%)" << std::endl;
    std::cout << "Meson: " << meson_muon_passnum << " (" << (meson_muon_passnum_double/meson_muon_num_double)*100 << "%)" << std::endl;
    std::cout << "Baryon: " << baryon_muon_passnum << " (" << (baryon_muon_passnum_double/baryon_muon_num_double)*100 << "%)" << std::endl;
    std::cout << "Photon: " << gamma_muon_passnum << " (" << (gamma_muon_passnum_double/gamma_muon_num_double)*100 << "%)" << std::endl;
    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "*************************************************************"  << std::endl;

    // p_T cut tests
    double_t ElectronCutMin = 0;
    double_t ElectronCutMax = 500;
    double_t ElectronCutSteps = 100;
    double_t ElectronCutInterval = (ElectronCutMax - ElectronCutMin)/ElectronCutSteps;
    double_t MuonCutMin = 0;
    double_t MuonCutMax = 500;
    double_t MuonCutSteps = 100;
    double_t MuonCutInterval = (MuonCutMax - MuonCutMin)/MuonCutSteps;

    std::vector<double_t> ElectronCuts;
    std::vector<double_t> ElectronPassProportion;
    std::vector<double_t> MuonCuts;
    std::vector<double_t> MuonPassProportion;

    for (double_t Cut = ElectronCutMin; Cut <= ElectronCutMax; Cut += ElectronCutInterval) {
	ElectronCuts.push_back(Cut);
	double_t W_electrons_passnum = 0;
	double_t W_electrons_tot = W_electrons.size();
	for (int i = 0; i < W_electrons.size(); ++i){
	    if (W_electrons.at(i) > Cut) {
		W_electrons_passnum += 1;
	    }
	}
	double_t other_electrons_passnum = 0;
	double_t other_electrons_tot = other_electrons.size();
	for (int i = 0; i < other_electrons.size(); ++i){
	    if (other_electrons.at(i) > Cut) {
		other_electrons_passnum += 1;
	    }
	}
	ElectronPassProportion.push_back((W_electrons_passnum*W_electrons_passnum*other_electrons_tot)/(W_electrons_tot*W_electrons_tot*other_electrons_passnum));
    }
    
    double_t ElectronPassProportionMax = ElectronPassProportion.at(0);
    int ElectronMaxIndex = 0;
    for (int i = 0; i < ElectronPassProportion.size(); ++i) {
	if (ElectronPassProportion.at(i) > ElectronPassProportionMax) {
	    ElectronPassProportionMax = ElectronPassProportion.at(i);
	    ElectronMaxIndex = i;
	}
    }
	
    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "Electron Optimal p_T Cut = " << ElectronCuts.at(ElectronMaxIndex) << " GeV" << std::endl;
    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "W:Other Ratio =  " << ElectronPassProportion.at(ElectronMaxIndex) << std::endl;
    std::cout << "-------------------------------------------------------------"  << std::endl;

    for (double_t Cut = MuonCutMin; Cut <= MuonCutMax; Cut += MuonCutInterval) {
	MuonCuts.push_back(Cut);
	double_t W_muons_passnum = 0;
	double_t W_muons_tot = W_muons.size();
	for (int i = 0; i < W_muons.size(); ++i){
	    if (W_muons.at(i) > Cut) {
		W_muons_passnum += 1;
	    }
	}
	double_t other_muons_passnum = 0;
	double_t other_muons_tot = other_muons.size();
	for (int i = 0; i < other_muons.size(); ++i){
	    if (other_muons.at(i) > Cut) {
		other_muons_passnum += 1;
	    }
	}
	MuonPassProportion.push_back((W_muons_passnum*W_muons_passnum*other_muons_tot)/(W_muons_tot*W_muons_tot*other_muons_passnum));
    }
    
    double_t MuonPassProportionMax = MuonPassProportion.at(0);
    int MuonMaxIndex = 0;
    for (int i = 0; i < MuonPassProportion.size(); ++i) {
	if (MuonPassProportion.at(i) > MuonPassProportionMax) {
	    MuonPassProportionMax = MuonPassProportion.at(i);
	    MuonMaxIndex = i;
	}
    }
	
    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "Muon Optimal p_T Cut = " << MuonCuts.at(MuonMaxIndex) << " GeV" << std::endl;
    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "W:Other Ratio =  " << MuonPassProportion.at(MuonMaxIndex) << std::endl;
    std::cout << "-------------------------------------------------------------"  << std::endl;
    std::cout << "*************************************************************"  << std::endl;

}
