
#include "Process.h"

bool Debug = false;

// Constants
Int_t ToPrint = 0;
Double_t e_mass = 0.00051099895;
Double_t muon_mass = 0.1056583755;
Double_t W_mass = 80.38;
Double_t Int_Luminosity = 139.0e+15;

//Parameters
Double_t Zprime_Mass = 1000;
Double_t Zprime_CrossSection = 2.534e-13;
Double_t WParton_CrossSection = 1.219e-09;
Double_t Diboson_CrossSection = 7.210e-12;
Double_t ttbar_CrossSection = 2.055e-10;
Double_t SingleTop_CrossSection = 4.992e-11;

// Cuts
/// Signal
Double_t MET_cut = 40;
Double_t MET_DeltaM_cut = 30;
Double_t JetPairMass_lowcut = 69;
Double_t JetPairMass_highcut = 91;
/// Background
Int_t Jet_NumLimit = 5;
Double_t Lepton_pTcut = 40;
Double_t LeptonicW_pTcut = 250;
Double_t HadronicW_pTcut = 400;
Double_t Rfunction_cut = 0;
Double_t Window_size = 132;

int main(int argc, char* argv[]) {

	// Input Delphes File

	const TString SignalFile = argv[1];
	const TString OutputFileName = argv[2];
	const TString Background_WParton_File = argv[3];
	const TString Background_Diboson_File = argv[4];
	const TString Background_ttbar_File = argv[5];
	const TString Background_SingleTop_File = argv[6];

	std::vector<TString> InputFiles;
	InputFiles.push_back(SignalFile);
	for (int i = 3; i < 7; i++) {
		InputFiles.push_back(argv[i]);
	}

	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "Running Process"  << std::endl;
	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "SignalFile = " << SignalFile << std::endl;
	std::cout << "OutputFileName = " << OutputFileName << std::endl;
	std::cout << "-------------------------------------------------------------"  << std::endl;

	//------------------------------------
	// Declare the output
	//------------------------------------

	OutputFile = new TFile(OutputFileName,"recreate");

	OutputFile->cd();

	h_EventCount = new TH1D("h_EventCount","",1,0,1);
	h_WeightCount = new TH1D("h_WeightCount","",1,0,1);

	h_ZprimeMass = new THStack("h_ZprimeMass", "; WW Mass [GeV]; Events / 50 GeV");
	THStack * h_ZprimeMass_Window = new THStack("h_ZprimeMass_Window", "; WW Mass [GeV]; Events / 50 GeV");

	THStack* h_ZprimeMass_precuts = new THStack("h_ZprimeMass_precuts", "; WW Mass [GeV]; Events / 50 GeV");
	THStack* h_ZprimeMass_Window_precuts = new THStack("h_ZprimeMass_Window_precuts", "; WW Mass [GeV]; Events / 50 GeV");

	Double_t JetMassCutWidth_list[80]{}, JetMassCutWidthSignalCount[80]{}, JetMassCutWidthBackgroundCount[80]{}, JetMassCutWidthSignalCount_Window[80]{}, JetMassCutWidthBackgroundCount_Window[80]{};
	for (Double_t i = 1.0; i < W_mass; i += 1.0) {
		JetMassCutWidth_list[TMath::FloorNint(i) - 1] = i;
		JetMassCutWidthSignalCount[TMath::FloorNint(i) - 1] = 0.0;
		JetMassCutWidthBackgroundCount[TMath::FloorNint(i) - 1] = 0.0;
		JetMassCutWidthSignalCount_Window[TMath::FloorNint(i) - 1] = 0.0;
		JetMassCutWidthBackgroundCount_Window[TMath::FloorNint(i) - 1] = 0.0;
	}
	Double_t JetNumLimit_list[8]{}, JetNumSignalCount[8]{}, JetNumBackgroundCount[8]{}, JetNumSignalCount_Window[8]{}, JetNumBackgroundCount_Window[8]{};
	for (Double_t i = 0.0; i < 8.0; i += 1.0) {
		JetNumLimit_list[TMath::FloorNint(i)] = i + 3.0;
		JetNumSignalCount[TMath::FloorNint(i)] = 0.0;
		JetNumBackgroundCount[TMath::FloorNint(i)] = 0.0;
		JetNumSignalCount_Window[TMath::FloorNint(i)] = 0.0;
		JetNumBackgroundCount_Window[TMath::FloorNint(i)] = 0.0;
	}
	Double_t LeptonpTCut_list[TMath::FloorNint(Zprime_Mass/2.0)]{}, LeptonpTSignalCount[TMath::FloorNint(Zprime_Mass/2.0)]{}, LeptonpTBackgroundCount[TMath::FloorNint(Zprime_Mass/2.0)]{}, LeptonpTSignalCount_Window[TMath::FloorNint(Zprime_Mass/2.0)]{}, LeptonpTBackgroundCount_Window[TMath::FloorNint(Zprime_Mass/2.0)]{};
	for (Double_t i = 0.0; i < Zprime_Mass/2.0; i += 1.0) {
		LeptonpTCut_list[TMath::FloorNint(i)] = i + 1.0;
		LeptonpTSignalCount[TMath::FloorNint(i)] = 0.0;
		LeptonpTBackgroundCount[TMath::FloorNint(i)] = 0.0;
		LeptonpTSignalCount_Window[TMath::FloorNint(i)] = 0.0;
		LeptonpTBackgroundCount_Window[TMath::FloorNint(i)] = 0.0;
	}
	Double_t LeptonicWpTCut_list[TMath::FloorNint(Zprime_Mass/2.0)]{}, LeptonicWpTSignalCount[TMath::FloorNint(Zprime_Mass/2.0)]{}, LeptonicWpTBackgroundCount[TMath::FloorNint(Zprime_Mass/2.0)]{}, LeptonicWpTSignalCount_Window[TMath::FloorNint(Zprime_Mass/2.0)]{}, LeptonicWpTBackgroundCount_Window[TMath::FloorNint(Zprime_Mass/2.0)]{};
	for (Double_t i = 0.0; i < Zprime_Mass/2.0; i += 1.0) {
		LeptonicWpTCut_list[TMath::FloorNint(i)] = i + 1.0;
		LeptonicWpTSignalCount[TMath::FloorNint(i)] = 0.0;
		LeptonicWpTBackgroundCount[TMath::FloorNint(i)] = 0.0;
		LeptonicWpTSignalCount_Window[TMath::FloorNint(i)] = 0.0;
		LeptonicWpTBackgroundCount_Window[TMath::FloorNint(i)] = 0.0;
	}
	Double_t HadronicWpTCut_list[TMath::FloorNint(Zprime_Mass/2.0)]{}, HadronicWpTSignalCount[TMath::FloorNint(Zprime_Mass/2.0)]{}, HadronicWpTBackgroundCount[TMath::FloorNint(Zprime_Mass/2.0)]{}, HadronicWpTSignalCount_Window[TMath::FloorNint(Zprime_Mass/2.0)]{}, HadronicWpTBackgroundCount_Window[TMath::FloorNint(Zprime_Mass/2.0)]{};
	for (Double_t i = 0.0; i < Zprime_Mass/2.0; i += 1.0) {
		HadronicWpTCut_list[TMath::FloorNint(i)] = i + 1.0;
		HadronicWpTSignalCount[TMath::FloorNint(i)] = 0.0;
		HadronicWpTBackgroundCount[TMath::FloorNint(i)] = 0.0;
		HadronicWpTSignalCount_Window[TMath::FloorNint(i)] = 0.0;
		HadronicWpTBackgroundCount_Window[TMath::FloorNint(i)] = 0.0;
	}
	Double_t LeptonicWpT_noRCut_list[TMath::FloorNint(Zprime_Mass/2.0)]{}, LeptonicWpT_noRSignalCount[TMath::FloorNint(Zprime_Mass/2.0)]{}, LeptonicWpT_noRBackgroundCount[TMath::FloorNint(Zprime_Mass/2.0)]{}, LeptonicWpT_noRSignalCount_Window[TMath::FloorNint(Zprime_Mass/2.0)]{}, LeptonicWpT_noRBackgroundCount_Window[TMath::FloorNint(Zprime_Mass/2.0)]{};
	for (Double_t i = 0.0; i < Zprime_Mass/2.0; i += 1.0) {
		LeptonicWpT_noRCut_list[TMath::FloorNint(i)] = i + 1.0;
		LeptonicWpT_noRSignalCount[TMath::FloorNint(i)] = 0.0;
		LeptonicWpT_noRBackgroundCount[TMath::FloorNint(i)] = 0.0;
		LeptonicWpT_noRSignalCount_Window[TMath::FloorNint(i)] = 0.0;
		LeptonicWpT_noRBackgroundCount_Window[TMath::FloorNint(i)] = 0.0;
	}
	Double_t HadronicWpT_noRCut_list[TMath::FloorNint(Zprime_Mass/2.0)]{}, HadronicWpT_noRSignalCount[TMath::FloorNint(Zprime_Mass/2.0)]{}, HadronicWpT_noRBackgroundCount[TMath::FloorNint(Zprime_Mass/2.0)]{}, HadronicWpT_noRSignalCount_Window[TMath::FloorNint(Zprime_Mass/2.0)]{}, HadronicWpT_noRBackgroundCount_Window[TMath::FloorNint(Zprime_Mass/2.0)]{};
	for (Double_t i = 0.0; i < Zprime_Mass/2.0; i += 1.0) {
		HadronicWpT_noRCut_list[TMath::FloorNint(i)] = i + 1.0;
		HadronicWpT_noRSignalCount[TMath::FloorNint(i)] = 0.0;
		HadronicWpT_noRBackgroundCount[TMath::FloorNint(i)] = 0.0;
		HadronicWpT_noRSignalCount_Window[TMath::FloorNint(i)] = 0.0;
		HadronicWpT_noRBackgroundCount_Window[TMath::FloorNint(i)] = 0.0;
	}
	Double_t RfunctionCut_list[50]{}, RfunctionSignalCount[50]{}, RfunctionBackgroundCount[50]{}, RfunctionSignalCount_Window[50]{}, RfunctionBackgroundCount_Window[50]{};
	for (Double_t i = 1.0; i < 51.0; i += 1.0) {
		RfunctionCut_list[TMath::FloorNint(i) - 1] = i/100;
		RfunctionSignalCount[TMath::FloorNint(i) - 1] = 0.0;
		RfunctionBackgroundCount[TMath::FloorNint(i) - 1] = 0.0;
		RfunctionSignalCount_Window[TMath::FloorNint(i) - 1] = 0.0;
		RfunctionBackgroundCount_Window[TMath::FloorNint(i) - 1] = 0.0;
	}
	Double_t Rfunction_noWpTCut_list[50]{}, Rfunction_noWpTSignalCount[50]{}, Rfunction_noWpTBackgroundCount[50]{}, Rfunction_noWpTSignalCount_Window[50]{}, Rfunction_noWpTBackgroundCount_Window[50]{};
	for (Double_t i = 1.0; i < 51.0; i += 1.0) {
		Rfunction_noWpTCut_list[TMath::FloorNint(i) - 1] = i/100;
		Rfunction_noWpTSignalCount[TMath::FloorNint(i) - 1] = 0.0;
		Rfunction_noWpTBackgroundCount[TMath::FloorNint(i) - 1] = 0.0;
		Rfunction_noWpTSignalCount_Window[TMath::FloorNint(i) - 1] = 0.0;
		Rfunction_noWpTBackgroundCount_Window[TMath::FloorNint(i) - 1] = 0.0;
	}
	Double_t Window_list[TMath::FloorNint(Zprime_Mass / 2.0) - 1]{}, Window_SignalCount[TMath::FloorNint(Zprime_Mass / 2.0) - 1]{}, Window_BackgroundCount[TMath::FloorNint(Zprime_Mass / 2.0) - 1]{};
	for (Double_t i = 1.0; i < Zprime_Mass / 2.0; i += 1.0) {
		Window_list[TMath::FloorNint(i) - 1] = i;
		Window_SignalCount[TMath::FloorNint(i) - 1] = 0.0;
		Window_BackgroundCount[TMath::FloorNint(i) - 1] = 0.0;
	}

	// define sub-histograms - pre-cuts
	
	TH1D* h_SignalReconMass_precuts = new TH1D("Z' precuts", "; WW Mass [GeV]; Events / 50 GeV", TMath::FloorNint((Zprime_Mass + 1000.0)/50.0), 0, Zprime_Mass+1000.0);
	TH1D* h_Background_WParton_ReconMass_precuts = new TH1D("V+jet precuts", "; WW Mass [GeV]; Events / 50 GeV", TMath::FloorNint((Zprime_Mass + 1000.0) / 50.0), 0, Zprime_Mass + 1000.0);
	TH1D * h_Background_Diboson_ReconMass_precuts = new TH1D("SM Diboson precuts", "; WW Mass [GeV]; Events / 50 GeV", TMath::FloorNint((Zprime_Mass + 1000.0) / 50.0), 0, Zprime_Mass + 1000.0);
	TH1D * h_Background_ttbar_ReconMass_precuts = new TH1D("t#bar{t} precuts", "; WW Mass [GeV]; Events / 50 GeV", TMath::FloorNint((Zprime_Mass + 1000.0) / 50.0), 0, Zprime_Mass + 1000.0);
	TH1D * h_Background_SingleTop_ReconMass_precuts = new TH1D("Single Top precuts", "; WW Mass [GeV]; Events / 50 GeV", TMath::FloorNint((Zprime_Mass + 1000.0) / 50.0), 0, Zprime_Mass + 1000.0);

	TH1D* h_SignalReconMass_Window_precuts = new TH1D("Z' precuts", "; WW Mass [GeV]; Events / 50 GeV", 8, Zprime_Mass - Window_size, Zprime_Mass + Window_size);
	TH1D* h_Background_WParton_ReconMass_Window_precuts = new TH1D("V+jet precuts", "; WW Mass [GeV]; Events / 50 GeV", 8, Zprime_Mass - Window_size, Zprime_Mass + Window_size);
	TH1D* h_Background_Diboson_ReconMass_Window_precuts = new TH1D("SM Diboson precuts", "; WW Mass [GeV]; Events / 50 GeV", 8, Zprime_Mass - Window_size, Zprime_Mass + Window_size);
	TH1D* h_Background_ttbar_ReconMass_Window_precuts = new TH1D("t#bar{t} precuts", "; WW Mass [GeV]; Events / 50 GeV", 8, Zprime_Mass - Window_size, Zprime_Mass + Window_size);
	TH1D* h_Background_SingleTop_ReconMass_Window_precuts = new TH1D("Single Top precuts", "; WW Mass [GeV]; Events / 50 GeV", 8, Zprime_Mass - Window_size, Zprime_Mass + Window_size);

	TH1D * h_Signal_LeptonicW_pT_precuts = new TH1D("h_Signal_LeptonicW_pT_precuts", "; W p_{T} [GeV]; Events / 20 GeV", TMath::FloorNint(Zprime_Mass/20.0), 0, Zprime_Mass);
	TH1D * h_Background_LeptonicW_pT_precuts = new TH1D("h_Background_LeptonicW_pT_precuts", "; W p_{T} [GeV]; Events / 20 GeV", TMath::FloorNint(Zprime_Mass / 20.0), 0, Zprime_Mass);
	TH1D * h_Signal_HadronicW_pT_precuts = new TH1D("h_Signal_HadronicW_pT_precuts", "; W p_{T} [GeV]; Events / 20 GeV", TMath::FloorNint(Zprime_Mass / 20.0), 0, Zprime_Mass);
	TH1D * h_Background_HadronicW_pT_precuts = new TH1D("h_Background_HadronicW_pT_precuts", "; W p_{T} [GeV]; Events / 20 GeV", TMath::FloorNint(Zprime_Mass / 20.0), 0, Zprime_Mass);

	TH1D * h_Signal_Lepton_pT_precuts = new TH1D("h_Signal_Lepton_pT_precuts", "; Lepton p_{T} [GeV]; Events / 20 GeV", TMath::FloorNint(Zprime_Mass / 20.0), 0, Zprime_Mass);
	TH1D * h_Background_Lepton_pT_precuts = new TH1D("h_Background_Lepton_pT_precuts", "; Lepton p_{T} [GeV]; Events / 20 GeV", TMath::FloorNint(Zprime_Mass / 20.0), 0, Zprime_Mass);
	TH1D * h_Signal_JetPair_Mass_precuts = new TH1D("h_Signal_JetPair_Mass_precuts", "; Jet Pair Mass [GeV]; Events / 5 GeV", TMath::FloorNint(Zprime_Mass / 5.0), 0, Zprime_Mass);
	TH1D * h_Background_JetPair_Mass_precuts = new TH1D("h_Background_JetPair_Mass_precuts", "; Jet Pair Mass [GeV]; Events / 5 GeV", TMath::FloorNint(Zprime_Mass / 5.0), 0, Zprime_Mass);

	TH1D* h_Signal_JetNum_withLepton_precuts = new TH1D("h_Signal_JetNum_withLepton_precuts", "; Jet Size; Events", 14, 0, 14);
	TH1D* h_Background_JetNum_withLepton_precuts = new TH1D("h_Background_JetNum_withLepton_precuts", "; Jet Size; Events", 14, 0, 14);

	TH1D* h_Signal_Rfunction_precuts = new TH1D("h_Signal_Rfunction_precuts", "; R; Events", 100, 0, 1);
	TH1D* h_Background_Rfunction_precuts = new TH1D("h_Background_Rfunction_precuts", "; R; Events", 100, 0, 1);

	TH1D* h_Signal_btagger_precuts = new TH1D("h_Signal_btagger_precuts", "; BTag; Events", 2, -0.5, 1.5);
	TH1D* h_Background_btagger_precuts = new TH1D("h_Background_btagger_precuts", "; BTag; Events", 2, -0.5, 1.5);
	
	// define sub-histograms - post-cuts
	
	TH1D* h_SignalReconMass = new TH1D("Z'", "; WW Mass [GeV]; Events / 50 GeV", TMath::FloorNint((Zprime_Mass + 1000.0)/50.0), 0, Zprime_Mass+1000.0);
	TH1D* h_Background_WParton_ReconMass = new TH1D("V+jet", "; WW Mass [GeV]; Events / 50 GeV", TMath::FloorNint((Zprime_Mass + 1000.0)/50.0), 0, Zprime_Mass+1000.0);
	TH1D* h_Background_Diboson_ReconMass = new TH1D("SM Diboson", "; WW Mass [GeV]; Events / 50 GeV", TMath::FloorNint((Zprime_Mass + 1000.0)/50.0), 0, Zprime_Mass+1000.0);
	TH1D* h_Background_ttbar_ReconMass = new TH1D("t#bar{t}", "; WW Mass [GeV]; Events / 50 GeV", TMath::FloorNint((Zprime_Mass + 1000.0)/50.0), 0, Zprime_Mass+1000.0);
	TH1D* h_Background_SingleTop_ReconMass = new TH1D("Single Top", "; WW Mass [GeV]; Events / 50 GeV", TMath::FloorNint((Zprime_Mass + 1000.0)/50.0), 0, Zprime_Mass+1000.0);

	TH1D* h_SignalReconMass_Window = new TH1D("Z'", "; WW Mass [GeV]; Events / 50 GeV", 8, Zprime_Mass - Window_size, Zprime_Mass + Window_size);
	TH1D* h_Background_WParton_ReconMass_Window = new TH1D("V+jet", "; WW Mass [GeV]; Events / 50 GeV", 8, Zprime_Mass - Window_size, Zprime_Mass + Window_size);
	TH1D* h_Background_Diboson_ReconMass_Window = new TH1D("SM Diboson", "; WW Mass [GeV]; Events / 50 GeV", 8, Zprime_Mass - Window_size, Zprime_Mass + Window_size);
	TH1D* h_Background_ttbar_ReconMass_Window = new TH1D("t#bar{t}", "; WW Mass [GeV]; Events / 50 GeV", 8, Zprime_Mass - Window_size, Zprime_Mass + Window_size);
	TH1D* h_Background_SingleTop_ReconMass_Window = new TH1D("Single Top", "; WW Mass [GeV]; Events / 50 GeV", 8, Zprime_Mass - Window_size, Zprime_Mass + Window_size);

	TH1D* h_Signal_LeptonicW_pT = new TH1D("h_Signal_LeptonicW_pT", "; W p_{T} [GeV]; Events / 20 GeV", TMath::FloorNint(Zprime_Mass / 20.0), 0, Zprime_Mass);
	TH1D* h_Background_LeptonicW_pT = new TH1D("h_Background_LeptonicW_pT", "; W p_{T} [GeV]; Events / 20 GeV", TMath::FloorNint(Zprime_Mass / 20.0), 0, Zprime_Mass);
	TH1D* h_Signal_HadronicW_pT = new TH1D("h_Signal_HadronicW_pT", "; W p_{T} [GeV]; Events / 20 GeV", TMath::FloorNint(Zprime_Mass / 20.0), 0, Zprime_Mass);
	TH1D* h_Background_HadronicW_pT = new TH1D("h_Background_HadronicW_pT", "; W p_{T} [GeV]; Events / 20 GeV", TMath::FloorNint(Zprime_Mass / 20.0), 0, Zprime_Mass);

	TH1D* h_Signal_Lepton_pT = new TH1D("h_Signal_Lepton_pT", "; Lepton p_{T} [GeV]; Events / 20 GeV", TMath::FloorNint(Zprime_Mass / 20.0), 0, Zprime_Mass);
	TH1D* h_Background_Lepton_pT = new TH1D("h_Background_Lepton_pT", "; Lepton p_{T} [GeV]; Events / 20 GeV", TMath::FloorNint(Zprime_Mass / 20.0), 0, Zprime_Mass);
	TH1D* h_Signal_JetPair_Mass = new TH1D("h_Signal_JetPair_Mass", "; Jet Pair Mass [GeV]; Events / 5 GeV", TMath::FloorNint(Zprime_Mass / 20.0), 0, Zprime_Mass);
	TH1D* h_Background_JetPair_Mass = new TH1D("h_Background_JetPair_Mass", "; Jet Pair Mass [GeV]; Events / 5 GeV", TMath::FloorNint(Zprime_Mass / 20.0), 0, Zprime_Mass);

	TH1D* h_Signal_JetNum_withLepton = new TH1D("h_Signal_JetNum_withLepton", "; Jet Size; Events", 14, 0, 14);
	TH1D* h_Background_JetNum_withLepton = new TH1D("h_Background_JetNum_withLepton", "; Jet Size; Events", 14, 0, 14);

	TH1D* h_Signal_Rfunction = new TH1D("h_Signal_Rfunction", "; R; Events", 100, 0, 1);
	TH1D* h_Background_Rfunction = new TH1D("h_Background_Rfunction", "; R; Events", 100, 0, 1);

	TH1D* h_Signal_btagger = new TH1D("h_Signal_btagger", "; BTag; Events", 2, -0.5, 1.5);
	TH1D* h_Background_btagger = new TH1D("h_Background_btagger", "; BTag; Events", 2, -0.5, 1.5);

	TH1D* h_Histogram_IntTest = new TH1D("h_Histogram_IntTest", "; Jet Size; Events", 14, 0, 14);

	//------------------------------------

	// Run the selection

	// Reading Input Files

	std::vector <Double_t> CrossSections;
	CrossSections.push_back(Zprime_CrossSection);
	CrossSections.push_back(WParton_CrossSection);
	CrossSections.push_back(Diboson_CrossSection);
	CrossSections.push_back(ttbar_CrossSection);
	CrossSections.push_back(SingleTop_CrossSection);

	Double_t SignalNumber;
	Double_t BGNumber;
	Double_t WPartonNumber;
	Double_t DibosonNumber;
	Double_t ttbarNumber;
	Double_t SingleTopNumber;

	for (int i = 0; i < InputFiles.size(); i++) {
		if (i == 0) {
			std::cout << "-------------------------------------------------------------" << std::endl;
			std::cout << "Processing Signal File..." << std::endl;
			std::cout << "-------------------------------------------------------------" << std::endl;
		}
		else {
			std::cout << "-------------------------------------------------------------" << std::endl;
			std::cout << "Processing Background File " << i << "..." << std::endl;
			std::cout << "-------------------------------------------------------------" << std::endl;
		}

		ExRootTreeReader* reader = NULL;
		reader = InitReader(InputFiles.at(i));
		
		// Get pointers to branches used in this analysis
		bEvent = reader->UseBranch("Event");
		bJet = reader->UseBranch("Jet");
		bGenJet = reader->UseBranch("GenJet");
		bElectron = reader->UseBranch("Electron");
		bMuon = reader->UseBranch("Muon");
		bTruthLeptons = reader->UseBranch("TruthLeptonParticles");
		bMissingET = reader->UseBranch("MissingET");
		bGenMissingET = reader->UseBranch("GenMissingET");
		bTruthWZ = reader->UseBranch("TruthWZParticles");

		Long64_t numberOfEntries = reader->GetEntries();

		int nSelected = 0;

		std::cout << "-------------------------------------------------------------" << std::endl;
		std::cout << "Input: " << numberOfEntries << " events to process" << std::endl;

		// Loop over all events
		for (Int_t entry = 0; entry < numberOfEntries; ++entry) {

			// Load selected branches with data from specified event
			reader->ReadEntry(entry);

			HepMCEvent* event = (HepMCEvent*)bEvent->At(0);
			const float Event_Weight = (CrossSections.at(i) * Int_Luminosity) / numberOfEntries;

			h_EventCount->Fill(0.5);
			h_WeightCount->Fill(0.5, Event_Weight);

			if ((entry > 0 && entry % 10000 == 0) || Debug) {
				if (i == 0) {
					std::cout << "-------------------------------------------------------------" << std::endl;
					std::cout << "Processing Signal Event Number =  " << entry << std::endl;
					std::cout << "-------------------------------------------------------------" << std::endl;
				}
				if (i == 1) {
					std::cout << "-------------------------------------------------------------" << std::endl;
					std::cout << "Processing WParton Event Number =  " << entry << std::endl;
					std::cout << "-------------------------------------------------------------" << std::endl;
				}
				if (i == 2) {
					std::cout << "-------------------------------------------------------------" << std::endl;
					std::cout << "Processing Diboson Event Number =  " << entry << std::endl;
					std::cout << "-------------------------------------------------------------" << std::endl;
				}
				if (i == 3) {
					std::cout << "-------------------------------------------------------------" << std::endl;
					std::cout << "Processing ttbar Event Number =  " << entry << std::endl;
					std::cout << "-------------------------------------------------------------" << std::endl;
				}
				if (i == 4) {
					std::cout << "-------------------------------------------------------------" << std::endl;
					std::cout << "Processing SingleTop Event Number =  " << entry << std::endl;
					std::cout << "-------------------------------------------------------------" << std::endl;
				}
			}

			Double_t btagged = -0.5;

			//------------------------------------------------------------------
			// Jet Loop
			//------------------------------------------------------------------

			TLorentzVector JetPair;

			if (bJet->GetEntriesFast() >= 2) {
				Jet* jet1 = (Jet*)bJet->At(0);
				Jet* jet2 = (Jet*)bJet->At(1);

				if (jet1->BTag == 1 || jet2->BTag == 1) {
					btagged = 0.5;
				}

				TLorentzVector Vec_Jet1;
				TLorentzVector Vec_Jet2;
				Vec_Jet1.SetPtEtaPhiM(jet1->PT, jet1->Eta, jet1->Phi, jet1->Mass);
				Vec_Jet2.SetPtEtaPhiM(jet2->PT, jet2->Eta, jet2->Phi, jet2->Mass);

				JetPair = Vec_Jet1 + Vec_Jet2;

				//------------------------------------------------------------------
				// Lepton and MET Loop
				//------------------------------------------------------------------

				TLorentzVector Vec_Lepton;
				TLorentzVector Vec_MissingET;
				Int_t EtaCalc = 0;

				if (bElectron->GetEntriesFast() >= 1 || bMuon->GetEntriesFast() >= 1) {

					// Lepton
					if (bMuon->GetEntriesFast() == 0) {
						Electron* lepton = (Electron*)bElectron->At(0);
						Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, e_mass);
					}
					else if (bElectron->GetEntriesFast() == 0) {
						Muon* lepton = (Muon*)bMuon->At(0);
						Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, muon_mass);
					}
					else {
						Electron* electron = (Electron*)bElectron->At(0);
						Muon* muon = (Muon*)bMuon->At(0);
						if (electron->PT > muon->PT) {
							Electron* lepton = electron;
							Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, e_mass);
						}
						else {
							Muon* lepton = muon;
							Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, muon_mass);
						}
					}

					// MissingET 
					MissingET* missingET = (MissingET*)bMissingET->At(0);

					// Eta Calculation
					Double_t a = Vec_Lepton.E() - Vec_Lepton.Pz();
					Double_t b = TMath::Sq(W_mass) + TMath::Sq(Vec_Lepton.Px() + (missingET->MET * TMath::Cos(missingET->Phi))) + TMath::Sq(Vec_Lepton.Py() + (missingET->MET * TMath::Sin(missingET->Phi)));
					b += TMath::Sq(Vec_Lepton.Pz()) - TMath::Sq(Vec_Lepton.E()) - TMath::Sq(missingET->MET);
					b /= (-1 * missingET->MET);
					Double_t c = Vec_Lepton.E() + Vec_Lepton.Pz();
					Double_t det = TMath::Sq(b) - (4 * a * c);

					// Real solution for eta
					if (det >= 0) {
						EtaCalc = 1;

						Double_t eta_plus = TMath::Log((-b + TMath::Sqrt(det)) / (2 * a));
						Double_t eta_minus = TMath::Log((-b - TMath::Sqrt(det)) / (2 * a));

						if (abs(eta_plus) < abs(eta_minus)) {
							Vec_MissingET.SetPtEtaPhiM(missingET->MET, eta_plus, missingET->Phi, 0);
						}
						else {
							Vec_MissingET.SetPtEtaPhiM(missingET->MET, eta_minus, missingET->Phi, 0);
						}
					}

					// Imaginary solution for eta
					else {
						Double_t eta[1000]{}, deltaM[1000]{};
						Int_t i_min = 0;
						for (Int_t i = 0; i < 1000; i++) {
							eta[i] = -5.0 + (i * (10.0 / 1000));
							Double_t W_px = Vec_Lepton.Px() + Vec_MissingET.Px();
							Double_t W_py = Vec_Lepton.Py() + Vec_MissingET.Py();
							Double_t W_pz = Vec_Lepton.Pz() + Vec_MissingET.Pt() * TMath::SinH(eta[i]);
							Double_t W_E = Vec_Lepton.E() + Vec_MissingET.Pt() * TMath::CosH(eta[i]);
							Double_t W_m = TMath::Sqrt(TMath::Sq(W_E) - TMath::Sq(W_px) - TMath::Sq(W_py) - TMath::Sq(W_pz));
							deltaM[i] = abs(W_m - W_mass);
							if (deltaM[i] < deltaM[i_min]) {
								i_min = i;
							}
						}
						Vec_MissingET.SetPtEtaPhiM(missingET->MET, eta[i_min], missingET->Phi, 0);

						if (deltaM[i_min] < MET_DeltaM_cut) {
							EtaCalc = 1;
						}
					}

					//------------------------------------------------------------------
					// Z' Reconstruction
					//------------------------------------------------------------------

					TLorentzVector LeptonandMET = Vec_Lepton + Vec_MissingET;
					TLorentzVector Zprime = JetPair + LeptonandMET;
					Double_t Rfunction;
					if (JetPair.Pt() < LeptonandMET.Pt()) {
						Rfunction = JetPair.Pt() / Zprime.M();
					}
					else {
						Rfunction = LeptonandMET.Pt() / Zprime.M();
					}
					// Setup histogram - pre cuts
					if (i == 0) {
						h_SignalReconMass_precuts->Fill(Zprime.M(), Event_Weight);
						h_SignalReconMass_Window_precuts->Fill(Zprime.M(), Event_Weight);
						h_Signal_LeptonicW_pT_precuts->Fill(LeptonandMET.Pt(), Event_Weight);
						h_Signal_HadronicW_pT_precuts->Fill(JetPair.Pt(), Event_Weight);
						h_Signal_Lepton_pT_precuts->Fill(Vec_Lepton.Pt(), Event_Weight);
						h_Signal_JetPair_Mass_precuts->Fill(JetPair.M(), Event_Weight);
						h_Signal_JetNum_withLepton_precuts->Fill(bJet->GetEntriesFast(), Event_Weight);
						h_Signal_Rfunction_precuts->Fill(Rfunction, Event_Weight);
						h_Signal_btagger_precuts->Fill(btagged, Event_Weight);
						for (Int_t j = 0; j < 80; j++) {
							if (JetPair.M() > W_mass - JetMassCutWidth_list[j] && JetPair.M() < W_mass + JetMassCutWidth_list[j]) {
								JetMassCutWidthSignalCount[j] += Event_Weight;
								if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
									JetMassCutWidthSignalCount_Window[j] += Event_Weight;
								}
							}
						}
						for (Int_t j = 0; j < 8; j++) {
							if (bJet->GetEntriesFast() < JetNumLimit_list[j]) {
								JetNumSignalCount[j] += Event_Weight;
								if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
									JetNumSignalCount_Window[j] += Event_Weight;
								}
							}
						}
						for (Int_t j = 0; j < TMath::FloorNint(Zprime_Mass / 2.0); j++) {
							if (Vec_Lepton.Pt() > LeptonpTCut_list[j]) {
								LeptonpTSignalCount[j] += Event_Weight;
								if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
									LeptonpTSignalCount_Window[j] += Event_Weight;
								}
							}
						}
						for (Int_t j = 0; j < TMath::FloorNint(Zprime_Mass / 2.0); j++) {
							if (LeptonandMET.Pt() > LeptonicWpTCut_list[j]) {
								LeptonicWpT_noRSignalCount[j] += Event_Weight;
								if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
									LeptonicWpT_noRSignalCount_Window[j] += Event_Weight;
								}
							}
						}
						for (Int_t j = 0; j < TMath::FloorNint(Zprime_Mass / 2.0); j++) {
							if (JetPair.Pt() > HadronicWpTCut_list[j]) {
								HadronicWpT_noRSignalCount[j] += Event_Weight;
								if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
									HadronicWpT_noRSignalCount_Window[j] += Event_Weight;
								}
							}
						}
						if (Rfunction > Rfunction_cut) {
							for (Int_t j = 0; j < TMath::FloorNint(Zprime_Mass / 2.0); j++) {
								if (LeptonandMET.Pt() > LeptonicWpTCut_list[j]) {
									LeptonicWpTSignalCount[j] += Event_Weight;
									if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
										LeptonicWpTSignalCount_Window[j] += Event_Weight;
									}
								}
							}
							for (Int_t j = 0; j < TMath::FloorNint(Zprime_Mass / 2.0); j++) {
								if (JetPair.Pt() > HadronicWpTCut_list[j]) {
									HadronicWpTSignalCount[j] += Event_Weight;
									if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
										HadronicWpTSignalCount_Window[j] += Event_Weight;
									}
								}
							}
						}
						for (Int_t j = 0; j < 50; j++) {
							if (Rfunction > Rfunction_noWpTCut_list[j]) {
								Rfunction_noWpTSignalCount[j] += Event_Weight;
								if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
									Rfunction_noWpTSignalCount_Window[j] += Event_Weight;
								}
							}
						}
						if (LeptonandMET.Pt() > LeptonicW_pTcut && JetPair.Pt() > HadronicW_pTcut) {
							for (Int_t j = 0; j < 50; j++) {
								if (Rfunction > RfunctionCut_list[j]) {
									RfunctionSignalCount[j] += Event_Weight;
									if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
										RfunctionSignalCount_Window[j] += Event_Weight;
									}
								}
							}
						}
						for (Int_t j = 0; j < TMath::FloorNint(Zprime_Mass / 2.0) - 1; j++) {
							if (Zprime.M() >= Zprime_Mass - Window_list[j] && Zprime.M() <= Zprime_Mass + Window_list[j]) {
								Window_SignalCount[j] += Event_Weight;
							}
						}
					}
					else if (i > 0) {
						h_Background_LeptonicW_pT_precuts->Fill(LeptonandMET.Pt(), Event_Weight);
						h_Background_HadronicW_pT_precuts->Fill(JetPair.Pt(), Event_Weight);
						h_Background_Lepton_pT_precuts->Fill(Vec_Lepton.Pt(), Event_Weight);
						h_Background_JetPair_Mass_precuts->Fill(JetPair.M(), Event_Weight);
						h_Background_LeptonicW_pT_precuts->Fill(LeptonandMET.Pt(), Event_Weight);
						h_Background_HadronicW_pT_precuts->Fill(JetPair.Pt(), Event_Weight);
						h_Background_Lepton_pT_precuts->Fill(Vec_Lepton.Pt(), Event_Weight);
						h_Background_JetPair_Mass_precuts->Fill(JetPair.M(), Event_Weight);
						h_Background_JetNum_withLepton_precuts->Fill(bJet->GetEntriesFast(), Event_Weight);
						h_Background_Rfunction_precuts->Fill(Rfunction, Event_Weight);
						h_Background_btagger_precuts->Fill(btagged, Event_Weight);
						if (i == 1) {
							h_Background_WParton_ReconMass_precuts->Fill(Zprime.M(), Event_Weight);
							h_Background_WParton_ReconMass_Window_precuts->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 2) {
							h_Background_Diboson_ReconMass_precuts->Fill(Zprime.M(), Event_Weight);
							h_Background_Diboson_ReconMass_Window_precuts->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 3) {
							h_Background_ttbar_ReconMass_precuts->Fill(Zprime.M(), Event_Weight);
							h_Background_ttbar_ReconMass_Window_precuts->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 4) {
							h_Background_SingleTop_ReconMass_precuts->Fill(Zprime.M(), Event_Weight);
							h_Background_SingleTop_ReconMass_Window_precuts->Fill(Zprime.M(), Event_Weight);
						}
						for (Int_t j = 0; j < 80; j++) {
							if (JetPair.M() > W_mass - JetMassCutWidth_list[j] && JetPair.M() < W_mass + JetMassCutWidth_list[j]) {
								JetMassCutWidthBackgroundCount[j] += Event_Weight;
								if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
									JetMassCutWidthBackgroundCount_Window[j] += Event_Weight;
								}
							}
						}
						for (Int_t j = 0; j < 8; j++) {
							if (bJet->GetEntriesFast() < JetNumLimit_list[j]) {
								JetNumBackgroundCount[j] += Event_Weight;
								if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
									JetNumBackgroundCount_Window[j] += Event_Weight;
								}
							}
						}
						for (Int_t j = 0; j < TMath::FloorNint(Zprime_Mass / 2.0); j++) {
							if (Vec_Lepton.Pt() > LeptonpTCut_list[j]) {
								LeptonpTBackgroundCount[j] += Event_Weight;
								if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
									LeptonpTBackgroundCount_Window[j] += Event_Weight;
								}
							}
						}
						for (Int_t j = 0; j < TMath::FloorNint(Zprime_Mass / 2.0); j++) {
							if (LeptonandMET.Pt() > LeptonicWpTCut_list[j]) {
								LeptonicWpT_noRBackgroundCount[j] += Event_Weight;
								if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
									LeptonicWpT_noRBackgroundCount_Window[j] += Event_Weight;
								}
							}
						}
						for (Int_t j = 0; j < TMath::FloorNint(Zprime_Mass / 2.0); j++) {
							if (JetPair.Pt() > HadronicWpTCut_list[j]) {
								HadronicWpT_noRBackgroundCount[j] += Event_Weight;
								if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
									HadronicWpT_noRBackgroundCount_Window[j] += Event_Weight;
								}
							}
						}
						if (Rfunction > Rfunction_cut) {
							for (Int_t j = 0; j < TMath::FloorNint(Zprime_Mass / 2.0); j++) {
								if (LeptonandMET.Pt() > LeptonicWpTCut_list[j]) {
									LeptonicWpTBackgroundCount[j] += Event_Weight;
									if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
										LeptonicWpTBackgroundCount_Window[j] += Event_Weight;
									}
								}
							}
							for (Int_t j = 0; j < TMath::FloorNint(Zprime_Mass / 2.0); j++) {
								if (JetPair.Pt() > HadronicWpTCut_list[j]) {
									HadronicWpTBackgroundCount[j] += Event_Weight;
									if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
										HadronicWpTBackgroundCount_Window[j] += Event_Weight;
									}
								}
							}
						}
						for (Int_t j = 0; j < 50; j++) {
							if (Rfunction > Rfunction_noWpTCut_list[j]) {
								Rfunction_noWpTBackgroundCount[j] += Event_Weight;
								if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
									Rfunction_noWpTBackgroundCount_Window[j] += Event_Weight;
								}
							}
						}
						if (LeptonandMET.Pt() > LeptonicW_pTcut && JetPair.Pt() > HadronicW_pTcut) {
							for (Int_t j = 0; j < 50; j++) {
								if (Rfunction > RfunctionCut_list[j]) {
									RfunctionBackgroundCount[j] += Event_Weight;
									if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
										RfunctionBackgroundCount_Window[j] += Event_Weight;
									}
								}
							}
						}
						for (Int_t j = 0; j < TMath::FloorNint(Zprime_Mass / 2.0) - 1; j++) {
							if (Zprime.M() >= Zprime_Mass - Window_list[j] && Zprime.M() <= Zprime_Mass + Window_list[j]) {
								Window_BackgroundCount[j] += Event_Weight;
							}
						}
					}
					if (JetPair.M() > JetPairMass_lowcut && JetPair.M() < JetPairMass_highcut && Vec_MissingET.Pt() > MET_cut && EtaCalc == 1 && jet1->BTag == 0 && jet2->BTag == 0 && bJet->GetEntriesFast() < Jet_NumLimit && Vec_Lepton.Pt() > Lepton_pTcut && LeptonandMET.Pt() > LeptonicW_pTcut && JetPair.Pt() > HadronicW_pTcut && Rfunction > Rfunction_cut){
						// Setup histogram - post cuts
						if (i == 0) {
							h_SignalReconMass->Fill(Zprime.M(), Event_Weight);
							if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
								h_SignalReconMass_Window->Fill(Zprime.M(), Event_Weight);
								SignalNumber += Event_Weight;
							}
							h_Signal_LeptonicW_pT->Fill(LeptonandMET.Pt(), Event_Weight);
							h_Signal_HadronicW_pT->Fill(JetPair.Pt(), Event_Weight);
							h_Signal_Lepton_pT->Fill(Vec_Lepton.Pt(), Event_Weight);
							h_Signal_JetPair_Mass->Fill(JetPair.M(), Event_Weight);
							h_Signal_JetNum_withLepton->Fill(bJet->GetEntriesFast(), Event_Weight);
							if (LeptonandMET.Pt() > LeptonicW_pTcut && JetPair.Pt() > HadronicW_pTcut) {
								h_Signal_Rfunction->Fill(Rfunction, Event_Weight);
							}
							h_Signal_btagger->Fill(btagged, Event_Weight);
						}
						else if (i > 0) {
							if (Zprime.M() >= Zprime_Mass - Window_size && Zprime.M() <= Zprime_Mass + Window_size) {
								BGNumber += Event_Weight;
								if (i == 1) {
									WPartonNumber += Event_Weight;
								}
								else if (i == 2) {
									DibosonNumber += Event_Weight;
								}
								else if (i == 3) {
									ttbarNumber += Event_Weight;
								}
								else if (i == 4) {
									SingleTopNumber += Event_Weight;
								}
							}
							h_Background_LeptonicW_pT->Fill(LeptonandMET.Pt(), Event_Weight);
							h_Background_HadronicW_pT->Fill(JetPair.Pt(), Event_Weight);
							h_Background_Lepton_pT->Fill(Vec_Lepton.Pt(), Event_Weight);
							h_Background_JetPair_Mass->Fill(JetPair.M(), Event_Weight);
							h_Background_JetNum_withLepton->Fill(bJet->GetEntriesFast(), Event_Weight);
							if (LeptonandMET.Pt() > LeptonicW_pTcut && JetPair.Pt() > HadronicW_pTcut) {
								h_Background_Rfunction->Fill(Rfunction, Event_Weight);
							}
							h_Background_btagger->Fill(btagged, Event_Weight);
							if (i == 1) {
								h_Background_WParton_ReconMass->Fill(Zprime.M(), Event_Weight);
								h_Background_WParton_ReconMass_Window->Fill(Zprime.M(), Event_Weight);
							}
							else if (i == 2) {
								h_Background_Diboson_ReconMass->Fill(Zprime.M(), Event_Weight);
								h_Background_Diboson_ReconMass_Window->Fill(Zprime.M(), Event_Weight);
							}
							else if (i == 3) {
								h_Background_ttbar_ReconMass->Fill(Zprime.M(), Event_Weight);
								h_Background_ttbar_ReconMass_Window->Fill(Zprime.M(), Event_Weight);
							}
							else if (i == 4) {
								h_Background_SingleTop_ReconMass->Fill(Zprime.M(), Event_Weight);
								h_Background_SingleTop_ReconMass_Window->Fill(Zprime.M(), Event_Weight);
							}
						}
						h_Histogram_IntTest->Fill(2, Event_Weight);
					
					} // Applying cuts

				} //Lepton and MET Loop

			} // Jet Loop

		} // Loop over all events

		delete reader;
	}

	Double_t Significance = SignalNumber / TMath::Sqrt(BGNumber);
	std::cout << SignalNumber << std::endl;
	std::cout << WPartonNumber << std::endl;
	std::cout << DibosonNumber << std::endl;
	std::cout << ttbarNumber << std::endl;
	std::cout << SingleTopNumber << std::endl;
	//std::cout << BGNumber << std::endl;
	//std::cout << Significance << std::endl;

	Double_t JetMassCutWidthSignificance[80]{};
	for (Int_t i = 0; i < 80; i++) {
		if (JetMassCutWidthSignalCount[i] == 0 || JetMassCutWidthBackgroundCount[i] == 0) {
			JetMassCutWidthSignificance[i] = 0.0;
		}
		else {
			JetMassCutWidthSignificance[i] = JetMassCutWidthSignalCount[i] / TMath::Sqrt(JetMassCutWidthBackgroundCount[i]);
		}
	}
	Double_t JetMassCutWidthSignificance_Window[80]{};
	for (Int_t i = 0; i < 80; i++) {
		if (JetMassCutWidthSignalCount_Window[i] == 0 || JetMassCutWidthBackgroundCount_Window[i] == 0) {
			JetMassCutWidthSignificance_Window[i] = 0.0;
		}
		else {
			JetMassCutWidthSignificance_Window[i] = JetMassCutWidthSignalCount_Window[i] / TMath::Sqrt(JetMassCutWidthBackgroundCount_Window[i]);
		}
	}
	Double_t JetNumSignificance[8]{};
	for (Int_t i = 0; i < 8; i++) {
		if (JetNumSignalCount[i] == 0) {
			JetNumSignificance[i] = 0.0;
		}
		else if (JetNumBackgroundCount[i] == 0) {
			JetNumSignificance[i] = 0.0;
		}
		else {
			JetNumSignificance[i] = JetNumSignalCount[i] / TMath::Sqrt(JetNumBackgroundCount[i]);
		}
	}
	Double_t JetNumSignificance_Window[8]{};
	for (Int_t i = 0; i < 8; i++) {
		if (JetNumSignalCount_Window[i] == 0) {
			JetNumSignificance_Window[i] = 0.0;
		}
		else if (JetNumBackgroundCount_Window[i] == 0) {
			JetNumSignificance_Window[i] = 0.0;
		}
		else {
			JetNumSignificance_Window[i] = JetNumSignalCount_Window[i] / TMath::Sqrt(JetNumBackgroundCount_Window[i]);
		}
	}

	Double_t LeptonpTSignificance[TMath::FloorNint(Zprime_Mass/2.0)]{};
	for (Int_t i = 0; i < Zprime_Mass/2.0; i++) {
		if (LeptonpTSignalCount[i] == 0) {
			LeptonpTSignificance[i] = 0.0;
		}
		else if (LeptonpTBackgroundCount[i] == 0) {
			LeptonpTSignificance[i] = 0.0;
		}
		else {
			LeptonpTSignificance[i] = LeptonpTSignalCount[i] / TMath::Sqrt(LeptonpTBackgroundCount[i]);
		}
	}
	Double_t LeptonpTSignificance_Window[TMath::FloorNint(Zprime_Mass/2.0)]{};
	for (Int_t i = 0; i < Zprime_Mass/2.0; i++) {
		if (LeptonpTSignalCount_Window[i] == 0) {
			LeptonpTSignificance_Window[i] = 0.0;
		}
		else if (LeptonpTBackgroundCount_Window[i] == 0) {
			LeptonpTSignificance_Window[i] = 0.0;
		}
		else {
			LeptonpTSignificance_Window[i] = LeptonpTSignalCount_Window[i] / TMath::Sqrt(LeptonpTBackgroundCount_Window[i]);
		}
	}

	Double_t LeptonicWpTSignificance[TMath::FloorNint(Zprime_Mass/2.0)]{};
	for (Int_t i = 0; i < Zprime_Mass/2.0; i++) {
		if (LeptonicWpTSignalCount[i] == 0) {
			LeptonicWpTSignificance[i] = 0.0;
		}
		else if (LeptonicWpTBackgroundCount[i] == 0) {
			LeptonicWpTSignificance[i] = 0.0;
		}
		else {
			LeptonicWpTSignificance[i] = LeptonicWpTSignalCount[i] / TMath::Sqrt(LeptonicWpTBackgroundCount[i]);
		}
	}
	Double_t LeptonicWpTSignificance_Window[TMath::FloorNint(Zprime_Mass/2.0)]{};
	for (Int_t i = 0; i < Zprime_Mass/2.0; i++) {
		if (LeptonicWpTSignalCount_Window[i] == 0) {
			LeptonicWpTSignificance_Window[i] = 0.0;
		}
		else if (LeptonicWpTBackgroundCount_Window[i] == 0) {
			LeptonicWpTSignificance_Window[i] = 0.0;
		}
		else {
			LeptonicWpTSignificance_Window[i] = LeptonicWpTSignalCount_Window[i] / TMath::Sqrt(LeptonicWpTBackgroundCount_Window[i]);
		}
	}

	Double_t HadronicWpTSignificance[TMath::FloorNint(Zprime_Mass/2.0)]{};
	for (Int_t i = 0; i < Zprime_Mass/2.0; i++) {
		if (HadronicWpTSignalCount[i] == 0) {
			HadronicWpTSignificance[i] = 0.0;
		}
		else if (HadronicWpTBackgroundCount[i] == 0) {
			HadronicWpTSignificance[i] = 0.0;
		}
		else {
			HadronicWpTSignificance[i] = HadronicWpTSignalCount[i] / TMath::Sqrt(HadronicWpTBackgroundCount[i]);
		}
	}
	Double_t HadronicWpTSignificance_Window[TMath::FloorNint(Zprime_Mass/2.0)]{};
	for (Int_t i = 0; i < Zprime_Mass/2.0; i++) {
		if (HadronicWpTSignalCount_Window[i] == 0) {
			HadronicWpTSignificance_Window[i] = 0.0;
		}
		else if (HadronicWpTBackgroundCount_Window[i] == 0) {
			HadronicWpTSignificance_Window[i] = 0.0;
		}
		else {
			HadronicWpTSignificance_Window[i] = HadronicWpTSignalCount_Window[i] / TMath::Sqrt(HadronicWpTBackgroundCount_Window[i]);
		}
	}

	Double_t LeptonicWpT_noRSignificance[TMath::FloorNint(Zprime_Mass/2.0)]{};
	for (Int_t i = 0; i < Zprime_Mass/2.0; i++) {
		if (LeptonicWpT_noRSignalCount[i] == 0) {
			LeptonicWpT_noRSignificance[i] = 0.0;
		}
		else if (LeptonicWpT_noRBackgroundCount[i] == 0) {
			LeptonicWpT_noRSignificance[i] = 0.0;
		}
		else {
			LeptonicWpT_noRSignificance[i] = LeptonicWpT_noRSignalCount[i] / TMath::Sqrt(LeptonicWpT_noRBackgroundCount[i]);
		}
	}
	Double_t LeptonicWpT_noRSignificance_Window[TMath::FloorNint(Zprime_Mass/2.0)]{};
	for (Int_t i = 0; i < Zprime_Mass/2.0; i++) {
		if (LeptonicWpT_noRSignalCount_Window[i] == 0) {
			LeptonicWpT_noRSignificance_Window[i] = 0.0;
		}
		else if (LeptonicWpT_noRBackgroundCount_Window[i] == 0) {
			LeptonicWpT_noRSignificance_Window[i] = 0.0;
		}
		else {
			LeptonicWpT_noRSignificance_Window[i] = LeptonicWpT_noRSignalCount_Window[i] / TMath::Sqrt(LeptonicWpT_noRBackgroundCount_Window[i]);
		}
	}

	Double_t HadronicWpT_noRSignificance[TMath::FloorNint(Zprime_Mass/2.0)]{};
	for (Int_t i = 0; i < Zprime_Mass/2.0; i++) {
		if (HadronicWpT_noRSignalCount[i] == 0) {
			HadronicWpT_noRSignificance[i] = 0.0;
		}
		else if (HadronicWpT_noRBackgroundCount[i] == 0) {
			HadronicWpT_noRSignificance[i] = 0.0;
		}
		else {
			HadronicWpT_noRSignificance[i] = HadronicWpT_noRSignalCount[i] / TMath::Sqrt(HadronicWpT_noRBackgroundCount[i]);
		}
	}
	Double_t HadronicWpT_noRSignificance_Window[TMath::FloorNint(Zprime_Mass/2.0)]{};
	for (Int_t i = 0; i < Zprime_Mass/2.0; i++) {
		if (HadronicWpT_noRSignalCount_Window[i] == 0) {
			HadronicWpT_noRSignificance_Window[i] = 0.0;
		}
		else if (HadronicWpT_noRBackgroundCount_Window[i] == 0) {
			HadronicWpT_noRSignificance_Window[i] = 0.0;
		}
		else {
			HadronicWpT_noRSignificance_Window[i] = HadronicWpT_noRSignalCount_Window[i] / TMath::Sqrt(HadronicWpT_noRBackgroundCount_Window[i]);
		}
	}

	Double_t RfunctionSignificance[50]{};
	for (Int_t i = 0; i < 50; i++) {
		if (RfunctionSignalCount[i] == 0) {
			RfunctionSignificance[i] = 0.0;
		}
		else if (RfunctionBackgroundCount[i] == 0) {
			RfunctionSignificance[i] = 0.0;
		}
		else {
			RfunctionSignificance[i] = RfunctionSignalCount[i] / TMath::Sqrt(RfunctionBackgroundCount[i]);
		}
	}
	Double_t RfunctionSignificance_Window[50]{};
	for (Int_t i = 0; i < 50; i++) {
		if (RfunctionSignalCount_Window[i] == 0) {
			RfunctionSignificance_Window[i] = 0.0;
		}
		else if (RfunctionBackgroundCount_Window[i] == 0) {
			RfunctionSignificance_Window[i] = 0.0;
		}
		else {
			RfunctionSignificance_Window[i] = RfunctionSignalCount_Window[i] / TMath::Sqrt(RfunctionBackgroundCount_Window[i]);
		}
	}

	Double_t Rfunction_noWpTSignificance[50]{};
	for (Int_t i = 0; i < 50; i++) {
		if (Rfunction_noWpTSignalCount[i] == 0) {
			Rfunction_noWpTSignificance[i] = 0.0;
		}
		else if (Rfunction_noWpTBackgroundCount[i] == 0) {
			Rfunction_noWpTSignificance[i] = 0.0;
		}
		else {
			Rfunction_noWpTSignificance[i] = Rfunction_noWpTSignalCount[i] / TMath::Sqrt(Rfunction_noWpTBackgroundCount[i]);
		}
	}
	Double_t Rfunction_noWpTSignificance_Window[50]{};
	for (Int_t i = 0; i < 50; i++) {
		if (Rfunction_noWpTSignalCount_Window[i] == 0) {
			Rfunction_noWpTSignificance_Window[i] = 0.0;
		}
		else if (Rfunction_noWpTBackgroundCount_Window[i] == 0) {
			Rfunction_noWpTSignificance_Window[i] = 0.0;
		}
		else {
			Rfunction_noWpTSignificance_Window[i] = Rfunction_noWpTSignalCount_Window[i] / TMath::Sqrt(Rfunction_noWpTBackgroundCount_Window[i]);
		}
	}
	Double_t Window_Significance[TMath::FloorNint(Zprime_Mass / 2.0) - 1]{};
	for (Int_t i = 0; i < TMath::FloorNint(Zprime_Mass / 2.0) - 1; i++) {
		if (Window_SignalCount[i] == 0 || Window_BackgroundCount[i] == 0) {
			Window_Significance[i] = 0.0;
		}
		else {
			Window_Significance[i] = Window_SignalCount[i] / TMath::Sqrt(Window_BackgroundCount[i]);
		}
	}

	TGraph* h_Significance_JetMassCutWidth = new TGraph(80, JetMassCutWidth_list, JetMassCutWidthSignificance);
	h_Significance_JetMassCutWidth->SetNameTitle("h_Significance_JetMassCutWidth", "; m_{JJ} Cut Width [GeV]; Significance");
	h_Significance_JetMassCutWidth->Draw("AEP");

	TGraph* h_Significance_JetMassCutWidth_Window = new TGraph(80, JetMassCutWidth_list, JetMassCutWidthSignificance_Window);
	h_Significance_JetMassCutWidth_Window->SetNameTitle("h_Significance_JetMassCutWidth_Window", "; m_{JJ} Cut Width [GeV]; Significance");
	h_Significance_JetMassCutWidth_Window->Draw("AEP");

	TGraph * h_Significance_JetNum = new TGraph(8, JetNumLimit_list, JetNumSignificance);
	h_Significance_JetNum->SetNameTitle("h_Significance_JetNum", "; Jet Size Limit; Significance");
	h_Significance_JetNum->Draw("AEP");

	TGraph * h_Significance_JetNum_Window = new TGraph(8, JetNumLimit_list, JetNumSignificance_Window);
	h_Significance_JetNum_Window->SetNameTitle("h_Significance_JetNum_Window", "; Jet Size Limit; Significance");
	h_Significance_JetNum_Window->Draw("AEP");

	TGraph* h_Significance_LeptonpT = new TGraph(TMath::FloorNint(Zprime_Mass/2.0), LeptonpTCut_list, LeptonpTSignificance);
	h_Significance_LeptonpT->SetNameTitle("h_Significance_LeptonpT", "; Lepton p_{T} Cut [GeV]; Significance");
	h_Significance_LeptonpT->Draw("AEP");

	TGraph* h_Significance_LeptonpT_Window = new TGraph(TMath::FloorNint(Zprime_Mass/2.0), LeptonpTCut_list, LeptonpTSignificance_Window);
	h_Significance_LeptonpT_Window->SetNameTitle("h_Significance_LeptonpT_Window", "; Lepton p_{T} Cut [GeV]; Significance");
	h_Significance_LeptonpT_Window->Draw("AEP");

	TGraph* h_Significance_LeptonicWpT = new TGraph(TMath::FloorNint(Zprime_Mass/2.0), LeptonicWpTCut_list, LeptonicWpTSignificance);
	h_Significance_LeptonicWpT->SetNameTitle("h_Significance_LeptonicWpT", "; Leptonic W p_{T} Cut [GeV]; Significance");
	h_Significance_LeptonicWpT->Draw("AEP");

	TGraph* h_Significance_LeptonicWpT_Window = new TGraph(TMath::FloorNint(Zprime_Mass/2.0), LeptonicWpTCut_list, LeptonicWpTSignificance_Window);
	h_Significance_LeptonicWpT_Window->SetNameTitle("h_Significance_LeptonicWpT_Window", "; Leptonic W p_{T} Cut [GeV]; Significance");
	h_Significance_LeptonicWpT_Window->Draw("AEP");

	TGraph* h_Significance_HadronicWpT = new TGraph(TMath::FloorNint(Zprime_Mass/2.0), HadronicWpTCut_list, HadronicWpTSignificance);
	h_Significance_HadronicWpT->SetNameTitle("h_Significance_HadronicWpT", "; Hadronic W p_{T} Cut [GeV]; Significance");
	h_Significance_HadronicWpT->Draw("AEP");

	TGraph* h_Significance_HadronicWpT_Window = new TGraph(TMath::FloorNint(Zprime_Mass/2.0), HadronicWpTCut_list, HadronicWpTSignificance_Window);
	h_Significance_HadronicWpT_Window->SetNameTitle("h_Significance_HadronicWpT_Window", "; Hadronic W p_{T} Cut [GeV]; Significance");
	h_Significance_HadronicWpT_Window->Draw("AEP");

	TGraph* h_Significance_LeptonicWpT_noR = new TGraph(TMath::FloorNint(Zprime_Mass/2.0), LeptonicWpT_noRCut_list, LeptonicWpT_noRSignificance);
	h_Significance_LeptonicWpT_noR->SetNameTitle("h_Significance_LeptonicWpT_noR", "; Leptonic W p_{T} Cut [GeV]; Significance");
	h_Significance_LeptonicWpT_noR->Draw("AEP");

	TGraph* h_Significance_LeptonicWpT_noR_Window = new TGraph(TMath::FloorNint(Zprime_Mass/2.0), LeptonicWpT_noRCut_list, LeptonicWpT_noRSignificance_Window);
	h_Significance_LeptonicWpT_noR_Window->SetNameTitle("h_Significance_LeptonicWpT_noR_Window", "; Leptonic W p_{T} Cut [GeV]; Significance");
	h_Significance_LeptonicWpT_noR_Window->Draw("AEP");

	TGraph* h_Significance_HadronicWpT_noR = new TGraph(TMath::FloorNint(Zprime_Mass/2.0), HadronicWpT_noRCut_list, HadronicWpT_noRSignificance);
	h_Significance_HadronicWpT_noR->SetNameTitle("h_Significance_HadronicWpT_noR", "; Hadronic W p_{T} Cut [GeV]; Significance");
	h_Significance_HadronicWpT_noR->Draw("AEP");

	TGraph* h_Significance_HadronicWpT_noR_Window = new TGraph(TMath::FloorNint(Zprime_Mass/2.0), HadronicWpT_noRCut_list, HadronicWpT_noRSignificance_Window);
	h_Significance_HadronicWpT_noR_Window->SetNameTitle("h_Significance_HadronicWpT_noR_Window", "; Hadronic W p_{T} Cut [GeV]; Significance");
	h_Significance_HadronicWpT_noR_Window->Draw("AEP");

	TGraph* h_Significance_Rfunction = new TGraph(50, RfunctionCut_list, RfunctionSignificance);
	h_Significance_Rfunction->SetNameTitle("h_Significance_Rfunction", "; R_{p_{T}/m}; Significance");
	h_Significance_Rfunction->Draw("AEP");

	TGraph* h_Significance_Rfunction_Window = new TGraph(50, RfunctionCut_list, RfunctionSignificance_Window);
	h_Significance_Rfunction_Window->SetNameTitle("h_Significance_Rfunction_Window", "; R_{p_{T}/m}; Significance");
	h_Significance_Rfunction_Window->Draw("AEP");

	TGraph* h_Significance_Rfunction_noWpT = new TGraph(50, Rfunction_noWpTCut_list, Rfunction_noWpTSignificance);
	h_Significance_Rfunction_noWpT->SetNameTitle("h_Significance_Rfunction_noWpT", "; R_{p_{T}/m}; Significance");
	h_Significance_Rfunction_noWpT->Draw("AEP");

	TGraph* h_Significance_Rfunction_noWpT_Window = new TGraph(50, Rfunction_noWpTCut_list, Rfunction_noWpTSignificance_Window);
	h_Significance_Rfunction_noWpT_Window->SetNameTitle("h_Significance_Rfunction_noWpT_Window", "; R_{p_{T}/m}; Significance");
	h_Significance_Rfunction_noWpT_Window->Draw("AEP");

	TGraph* h_Significance_Window = new TGraph(TMath::FloorNint(Zprime_Mass / 2.0)-1, Window_list, Window_Significance);
	h_Significance_Window->SetNameTitle("h_Significance_Window", "; Mass Window Half-Width [GeV]; Significance");
	h_Significance_Window->Draw("AEP");

	h_Background_SingleTop_ReconMass_precuts->SetOption("HIST");
	h_Background_ttbar_ReconMass_precuts->SetOption("HIST");
	h_Background_Diboson_ReconMass_precuts->SetOption("HIST");
	h_Background_WParton_ReconMass_precuts->SetOption("HIST");
	h_SignalReconMass_precuts->SetOption("HIST");

	h_Background_SingleTop_ReconMass_Window_precuts->SetOption("HIST");
	h_Background_ttbar_ReconMass_Window_precuts->SetOption("HIST");
	h_Background_Diboson_ReconMass_Window_precuts->SetOption("HIST");
	h_Background_WParton_ReconMass_Window_precuts->SetOption("HIST");
	h_SignalReconMass_Window_precuts->SetOption("HIST");

	h_Signal_LeptonicW_pT_precuts->SetOption("HIST");
	h_Background_LeptonicW_pT_precuts->SetOption("HIST");
	h_Signal_HadronicW_pT_precuts->SetOption("HIST");
	h_Background_HadronicW_pT_precuts->SetOption("HIST");

	h_Signal_Lepton_pT_precuts->SetOption("HIST");
	h_Background_Lepton_pT_precuts->SetOption("HIST");
	h_Signal_JetPair_Mass_precuts->SetOption("HIST");
	h_Background_JetPair_Mass_precuts->SetOption("HIST");

	h_Signal_JetNum_withLepton_precuts->SetOption("HIST");
	h_Background_JetNum_withLepton_precuts->SetOption("HIST");

	h_Signal_Rfunction_precuts->SetOption("HIST");
	h_Background_Rfunction_precuts->SetOption("HIST");

	h_Signal_btagger_precuts->SetOption("HIST");
	h_Background_btagger_precuts->SetOption("HIST");

	h_Background_SingleTop_ReconMass->SetOption("HIST");
	h_Background_ttbar_ReconMass->SetOption("HIST");
	h_Background_Diboson_ReconMass->SetOption("HIST");
	h_Background_WParton_ReconMass->SetOption("HIST");
	h_SignalReconMass->SetOption("HIST");

	h_Background_SingleTop_ReconMass_Window->SetOption("HIST");
	h_Background_ttbar_ReconMass_Window->SetOption("HIST");
	h_Background_Diboson_ReconMass_Window->SetOption("HIST");
	h_Background_WParton_ReconMass_Window->SetOption("HIST");
	h_SignalReconMass_Window->SetOption("HIST");

	h_Signal_LeptonicW_pT->SetOption("HIST");
	h_Background_LeptonicW_pT->SetOption("HIST");
	h_Signal_HadronicW_pT->SetOption("HIST");
	h_Background_HadronicW_pT->SetOption("HIST");

	h_Signal_Lepton_pT->SetOption("HIST");
	h_Background_Lepton_pT->SetOption("HIST");
	h_Signal_JetPair_Mass->SetOption("HIST");
	h_Background_JetPair_Mass->SetOption("HIST");

	h_Signal_JetNum_withLepton->SetOption("HIST");
	h_Background_JetNum_withLepton->SetOption("HIST");

	h_Signal_Rfunction->SetOption("HIST");
	h_Background_Rfunction->SetOption("HIST");

	h_Signal_btagger->SetOption("HIST");
	h_Background_btagger->SetOption("HIST");

	h_Histogram_IntTest->SetOption("HIST");

	h_ZprimeMass_precuts->Add(h_Background_SingleTop_ReconMass_precuts);
	h_ZprimeMass_precuts->Add(h_Background_ttbar_ReconMass_precuts);
	h_ZprimeMass_precuts->Add(h_Background_Diboson_ReconMass_precuts);
	h_ZprimeMass_precuts->Add(h_Background_WParton_ReconMass_precuts);
	h_ZprimeMass_precuts->Add(h_SignalReconMass_precuts);

	h_ZprimeMass_Window_precuts->Add(h_Background_SingleTop_ReconMass_Window_precuts);
	h_ZprimeMass_Window_precuts->Add(h_Background_ttbar_ReconMass_Window_precuts);
	h_ZprimeMass_Window_precuts->Add(h_Background_Diboson_ReconMass_Window_precuts);
	h_ZprimeMass_Window_precuts->Add(h_Background_WParton_ReconMass_Window_precuts);
	h_ZprimeMass_Window_precuts->Add(h_SignalReconMass_Window_precuts);
	
	h_ZprimeMass->Add(h_Background_SingleTop_ReconMass);
	h_ZprimeMass->Add(h_Background_ttbar_ReconMass);
	h_ZprimeMass->Add(h_Background_Diboson_ReconMass);
	h_ZprimeMass->Add(h_Background_WParton_ReconMass);
	h_ZprimeMass->Add(h_SignalReconMass);

	h_ZprimeMass_Window->Add(h_Background_SingleTop_ReconMass_Window);
	h_ZprimeMass_Window->Add(h_Background_ttbar_ReconMass_Window);
	h_ZprimeMass_Window->Add(h_Background_Diboson_ReconMass_Window);
	h_ZprimeMass_Window->Add(h_Background_WParton_ReconMass_Window);
	h_ZprimeMass_Window->Add(h_SignalReconMass_Window);

	TCanvas* c_WWprimeMass_precuts = new TCanvas("c_WWprimeMass_precuts", "", 1000, 1000);
	gStyle->SetPalette(1);
	c_WWprimeMass_precuts->cd();
	c_WWprimeMass_precuts->DrawFrame(0, 0, 1, 1);
	h_ZprimeMass_precuts->Draw("PFC");
	gPad->BuildLegend(0.75, 0.75, 0.95, 0.95, "");
	TLatex Text_precuts;
	Text_precuts.SetTextFont(42);
	Text_precuts.SetTextSize(0.04);
	Text_precuts.SetTextAlign(12);
	Text_precuts.DrawLatex(90, h_ZprimeMass_precuts->GetMaximum() * 0.97, "#sqrt{s} = 13 TeV");
	Text_precuts.DrawLatex(90, h_ZprimeMass_precuts->GetMaximum() * 0.85, "#int L dt = 139 fb^{-1}");
	Text_precuts.DrawLatex(90, h_ZprimeMass_precuts->GetMaximum() * 0.77, "DY Z'#rightarrow WW");

	TCanvas* c_WWprimeMass_Window_precuts = new TCanvas("c_WWprimeMass_Window_precuts", "", 1000, 1000);
	gStyle->SetPalette(1);
	c_WWprimeMass_Window_precuts->Divide(2, 1);
	c_WWprimeMass_Window_precuts->cd(1);
	h_ZprimeMass_precuts->Draw("PFC");
	gPad->BuildLegend(0.75, 0.75, 0.95, 0.95, "");
	c_WWprimeMass_Window_precuts->cd(2);
	h_ZprimeMass_Window_precuts->Draw("PFC");
	gPad->BuildLegend(0.75, 0.75, 0.95, 0.95, "");

	TCanvas* c_WWprimeMass = new TCanvas("c_WWprimeMass", "", 1000, 1000);
	gStyle->SetPalette(1);
	c_WWprimeMass->cd();
	c_WWprimeMass->DrawFrame(0, 0, 1, 1);
	h_ZprimeMass->Draw("PFC");
	gPad->BuildLegend(0.75, 0.75, 0.95, 0.95, "");
	TLatex Text;
	Text.SetTextFont(42);
	Text.SetTextSize(0.04);
	Text.SetTextAlign(12);
	Text.DrawLatex(90, h_ZprimeMass->GetMaximum() * 0.97, "#sqrt{s} = 13 TeV");
	Text.DrawLatex(90, h_ZprimeMass->GetMaximum() * 0.85, "#int L dt = 139 fb^{-1}");
	Text.DrawLatex(90, h_ZprimeMass->GetMaximum() * 0.77, "DY Z'#rightarrow WW");
	TString Sig = "#sigma = ";
	Sig.Insert(9, Significance);
	//Text.DrawLatex(90, h_ZprimeMass->GetMaximum() * 0.69, Sig);

	TCanvas* c_WWprimeMass_Window = new TCanvas("c_WWprimeMass_Window", "", 1000, 1000);
	gStyle->SetPalette(1);
	c_WWprimeMass_Window->Divide(2, 1);
	c_WWprimeMass_Window->cd(1);
	h_ZprimeMass->Draw("PFC");
	gPad->BuildLegend(0.75, 0.75, 0.95, 0.95, "");
	c_WWprimeMass_Window->cd(2);
	h_ZprimeMass_Window->Draw("PFC");
	gPad->BuildLegend(0.75, 0.75, 0.95, 0.95, "");


	// Writing to File

	std::cout << "Events in EventCount: " << h_EventCount->GetEntries() << std::endl;

	std::cout << "Write to file..." << std::endl;

	OutputFile->cd();

	h_EventCount->Write();
	h_WeightCount->Write();

	h_ZprimeMass_precuts->Write();
	h_ZprimeMass_Window_precuts->Write();
	c_WWprimeMass_precuts->Write();
	c_WWprimeMass_Window_precuts->Write();

	h_SignalReconMass_precuts->Write();
	h_Background_WParton_ReconMass_precuts->Write();
	h_Background_Diboson_ReconMass_precuts->Write();
	h_Background_ttbar_ReconMass_precuts->Write();
	h_Background_SingleTop_ReconMass_precuts->Write();

	h_Signal_LeptonicW_pT_precuts->Write();
	h_Background_LeptonicW_pT_precuts->Write();
	h_Signal_HadronicW_pT_precuts->Write();
	h_Background_HadronicW_pT_precuts->Write();

	h_Signal_Lepton_pT_precuts->Write();
	h_Background_Lepton_pT_precuts->Write();
	h_Signal_JetPair_Mass_precuts->Write();
	h_Background_JetPair_Mass_precuts->Write();

	h_Signal_JetNum_withLepton_precuts->Write();
	h_Background_JetNum_withLepton_precuts->Write();

	h_Signal_Rfunction_precuts->Write();
	h_Background_Rfunction_precuts->Write();

	h_Signal_btagger_precuts->Write();
	h_Background_btagger_precuts->Write();

	h_Significance_JetMassCutWidth_Window->Write();
	//h_Significance_JetMassCutWidth->Write();
	
	h_Significance_JetNum_Window->Write();
	//h_Significance_JetNum->Write();

	h_Significance_LeptonpT_Window->Write();
	//h_Significance_LeptonpT->Write();

	h_Significance_LeptonicWpT_noR_Window->Write();
	//h_Significance_LeptonicWpT_noR->Write();

	h_Significance_HadronicWpT_noR_Window->Write();
	//h_Significance_HadronicWpT_noR->Write();

	h_Significance_LeptonicWpT_Window->Write();
	//h_Significance_LeptonicWpT->Write();

	h_Significance_HadronicWpT_Window->Write();
	//h_Significance_HadronicWpT->Write();

	h_Significance_Rfunction_noWpT_Window->Write();
	//h_Significance_Rfunction_noWpT->Write();

	h_Significance_Rfunction_Window->Write();
	//h_Significance_Rfunction->Write();

	h_Significance_Window->Write();

	h_ZprimeMass->Write();
	h_ZprimeMass_Window->Write();
	c_WWprimeMass->Write();
	c_WWprimeMass_Window->Write();

	h_SignalReconMass->Write();
	h_Background_WParton_ReconMass->Write();
	h_Background_Diboson_ReconMass->Write();
	h_Background_ttbar_ReconMass->Write();
	h_Background_SingleTop_ReconMass->Write();

	h_Signal_LeptonicW_pT->Write();
	h_Background_LeptonicW_pT->Write();
	h_Signal_HadronicW_pT->Write();
	h_Background_HadronicW_pT->Write();

	h_Signal_Lepton_pT->Write();
	h_Background_Lepton_pT->Write();
	h_Signal_JetPair_Mass->Write();
	h_Background_JetPair_Mass->Write();
	
	h_Signal_JetNum_withLepton->Write();
	h_Background_JetNum_withLepton->Write();

	h_Signal_Rfunction->Write();
	h_Background_Rfunction->Write();

	h_Signal_btagger->Write();
	h_Background_btagger->Write();

	h_Histogram_IntTest->Write();

	OutputFile->Close();

	std::cout << "Tidy..." << std::endl;

	std::cout << "Done!" << std::endl;

	return 0;

}

ExRootTreeReader * InitReader(const TString FilePath) {

	std::cout << "InitReader: " << FilePath << std::endl;

	TFile * f = TFile::Open(FilePath);

	TChain * Chain = new TChain("Delphes","");

	Chain->Add(FilePath);

	// Create object of class ExRootTreeReader
	ExRootTreeReader * r = new ExRootTreeReader(Chain);

	return r;
}
