
#include "Process.h"

int main(int argc, char* argv[]) {

	// Input Delphes File

	const TString OutputFileName = argv[1];

	OutputFile = new TFile(OutputFileName, "recreate");

	OutputFile->cd();

	THStack * h_WParton_Recon_mHatMin_500to800 = new THStack("h_WParton_Recon_mHatMin_500to800", "; WW Mass / GeV; Events / 25 GeV");
	THStack * h_WParton_Recon_pTHatMin_150to350 = new THStack("h_WParton_Recon_pTHatMin_150to350", "; WW Mass / GeV; Events / 25 GeV");

	THStack* h_WParton_Truth_mHatMin_500to800 = new THStack("h_WParton_Truth_mHatMin_500to800", "; WW Mass / GeV; Events / 25 GeV");
	THStack* h_WParton_Truth_pTHatMin_150to350 = new THStack("h_WParton_Truth_pTHatMin_150to350", "; WW Mass / GeV; Events / 25 GeV");

	TH1D* h_WParton_Recon_mHatMin_500 = new TH1D("h_WParton_Recon_mHatMin_500", "; WW Mass / GeV; Events / 25 GeV", 100, -4, 4);
	TH1D* h_WParton_Recon_mHatMin_600 = new TH1D("h_WParton_Recon_mHatMin_600", "; WW Mass / GeV; Events / 25 GeV", 100, -4, 4);
	TH1D* h_WParton_Recon_mHatMin_700 = new TH1D("h_WParton_Recon_mHatMin_700", "; WW Mass / GeV; Events / 25 GeV", 100, -4, 4);
	TH1D* h_WParton_Recon_mHatMin_800 = new TH1D("h_WParton_Recon_mHatMin_800", "; WW Mass / GeV; Events / 25 GeV", 100, -4, 4);
	TH1D* h_WParton_Recon_pTHatMin_150 = new TH1D("h_WParton_Recon_pTHatMin_150", "; WW Mass / GeV; Events / 25 GeV", 100, -4, 4);
	TH1D* h_WParton_Recon_pTHatMin_200 = new TH1D("h_WParton_Recon_pTHatMin_200", "; WW Mass / GeV; Events / 25 GeV", 100, -4, 4);
	TH1D* h_WParton_Recon_pTHatMin_250 = new TH1D("h_WParton_Recon_pTHatMin_250", "; WW Mass / GeV; Events / 25 GeV", 100, -4, 4);
	TH1D* h_WParton_Recon_pTHatMin_300 = new TH1D("h_WParton_Recon_pTHatMin_300", "; WW Mass / GeV; Events / 25 GeV", 100, -4, 4);
	TH1D* h_WParton_Recon_pTHatMin_350 = new TH1D("h_WParton_Recon_pTHatMin_350", "; WW Mass / GeV; Events / 25 GeV", 100, -4, 4);

	TH1D* h_WParton_Truth_mHatMin_500 = new TH1D("h_WParton_Truth_mHatMin_500", "; WW Mass / GeV; Events / 25 GeV", 100, -4, 4);
	TH1D* h_WParton_Truth_mHatMin_600 = new TH1D("h_WParton_Truth_mHatMin_600", "; WW Mass / GeV; Events / 25 GeV", 100, -4, 4);
	TH1D* h_WParton_Truth_mHatMin_700 = new TH1D("h_WParton_Truth_mHatMin_700", "; WW Mass / GeV; Events / 25 GeV", 100, -4, 4);
	TH1D* h_WParton_Truth_mHatMin_800 = new TH1D("h_WParton_Truth_mHatMin_800", "; WW Mass / GeV; Events / 25 GeV", 100, -4, 4);
	TH1D* h_WParton_Truth_pTHatMin_150 = new TH1D("h_WParton_Truth_pTHatMin_150", "; WW Mass / GeV; Events / 25 GeV", 100, -4, 4);
	TH1D* h_WParton_Truth_pTHatMin_200 = new TH1D("h_WParton_Truth_pTHatMin_200", "; WW Mass / GeV; Events / 25 GeV", 100, -4, 4);
	TH1D* h_WParton_Truth_pTHatMin_250 = new TH1D("h_WParton_Truth_pTHatMin_250", "; WW Mass / GeV; Events / 25 GeV", 100, -4, 4);
	TH1D* h_WParton_Truth_pTHatMin_300 = new TH1D("h_WParton_Truth_pTHatMin_300", "; WW Mass / GeV; Events / 25 GeV", 100, -4, 4);
	TH1D* h_WParton_Truth_pTHatMin_350 = new TH1D("h_WParton_Truth_pTHatMin_350", "; WW Mass / GeV; Events / 25 GeV", 100, -4, 4);

	h_WParton_Recon_mHatMin_500->FillRandom("gaus", 10000);
	h_WParton_Recon_mHatMin_600->FillRandom("gaus", 7000);
	h_WParton_Recon_mHatMin_700->FillRandom("gaus", 5000);
	h_WParton_Recon_mHatMin_800->FillRandom("gaus", 3000);
	h_WParton_Recon_pTHatMin_150->FillRandom("gaus", 10000);
	h_WParton_Recon_pTHatMin_200->FillRandom("gaus", 8000);
	h_WParton_Recon_pTHatMin_250->FillRandom("gaus", 6000);
	h_WParton_Recon_pTHatMin_300->FillRandom("gaus", 4500);
	h_WParton_Recon_pTHatMin_350->FillRandom("gaus", 3000);

	h_WParton_Truth_mHatMin_500->FillRandom("gaus", 10000);
	h_WParton_Truth_mHatMin_600->FillRandom("gaus", 7000);
	h_WParton_Truth_mHatMin_700->FillRandom("gaus", 5000);
	h_WParton_Truth_mHatMin_800->FillRandom("gaus", 1600);
	h_WParton_Truth_pTHatMin_150->FillRandom("gaus", 11000);
	h_WParton_Truth_pTHatMin_200->FillRandom("gaus", 7500);
	h_WParton_Truth_pTHatMin_250->FillRandom("gaus", 5000);
	h_WParton_Truth_pTHatMin_300->FillRandom("gaus", 3700);
	h_WParton_Truth_pTHatMin_350->FillRandom("gaus", 1000);

	h_WParton_Recon_mHatMin_500->SetOption("HIST");
	h_WParton_Recon_mHatMin_600->SetOption("HIST");
	h_WParton_Recon_mHatMin_700->SetOption("HIST");
	h_WParton_Recon_mHatMin_800->SetOption("HIST");
	h_WParton_Recon_pTHatMin_150->SetOption("HIST");
	h_WParton_Recon_pTHatMin_200->SetOption("HIST");
	h_WParton_Recon_pTHatMin_250->SetOption("HIST");
	h_WParton_Recon_pTHatMin_300->SetOption("HIST");
	h_WParton_Recon_pTHatMin_350->SetOption("HIST");

	h_WParton_Truth_mHatMin_500->SetOption("HIST");
	h_WParton_Truth_mHatMin_600->SetOption("HIST");
	h_WParton_Truth_mHatMin_700->SetOption("HIST");
	h_WParton_Truth_mHatMin_800->SetOption("HIST");
	h_WParton_Truth_pTHatMin_150->SetOption("HIST");
	h_WParton_Truth_pTHatMin_200->SetOption("HIST");
	h_WParton_Truth_pTHatMin_250->SetOption("HIST");
	h_WParton_Truth_pTHatMin_300->SetOption("HIST");
	h_WParton_Truth_pTHatMin_350->SetOption("HIST");

	h_WParton_Recon_mHatMin_500->SetTitle("#hat{m}_{min} = 500 GeV");
	h_WParton_Recon_mHatMin_600->SetTitle("#hat{m}_{min} = 600 GeV");
	h_WParton_Recon_mHatMin_700->SetTitle("#hat{m}_{min} = 700 GeV");
	h_WParton_Recon_mHatMin_800->SetTitle("#hat{m}_{min} = 800 GeV");
	h_WParton_Recon_pTHatMin_150->SetTitle("#hat{p_{T}}_{min} = 150 GeV");
	h_WParton_Recon_pTHatMin_200->SetTitle("#hat{p_{T}}_{min} = 200 GeV");
	h_WParton_Recon_pTHatMin_250->SetTitle("#hat{p_{T}}_{min} = 250 GeV");
	h_WParton_Recon_pTHatMin_300->SetTitle("#hat{p_{T}}_{min} = 300 GeV");
	h_WParton_Recon_pTHatMin_350->SetTitle("#hat{p_{T}}_{min} = 350 GeV");

	h_WParton_Truth_mHatMin_500->SetTitle("#hat{m}_{min} = 500 GeV");
	h_WParton_Truth_mHatMin_600->SetTitle("#hat{m}_{min} = 600 GeV");
	h_WParton_Truth_mHatMin_700->SetTitle("#hat{m}_{min} = 700 GeV");
	h_WParton_Truth_mHatMin_800->SetTitle("#hat{m}_{min} = 800 GeV");
	h_WParton_Truth_pTHatMin_150->SetTitle("#hat{p_{T}}_{min} = 150 GeV");
	h_WParton_Truth_pTHatMin_200->SetTitle("#hat{p_{T}}_{min} = 200 GeV");
	h_WParton_Truth_pTHatMin_250->SetTitle("#hat{p_{T}}_{min} = 250 GeV");
	h_WParton_Truth_pTHatMin_300->SetTitle("#hat{p_{T}}_{min} = 300 GeV");
	h_WParton_Truth_pTHatMin_350->SetTitle("#hat{p_{T}}_{min} = 350 GeV");

	h_WParton_Recon_mHatMin_500to800->Add(h_WParton_Recon_mHatMin_500);
	h_WParton_Recon_mHatMin_500to800->Add(h_WParton_Recon_mHatMin_600);
	h_WParton_Recon_mHatMin_500to800->Add(h_WParton_Recon_mHatMin_700);
	h_WParton_Recon_mHatMin_500to800->Add(h_WParton_Recon_mHatMin_800);
	h_WParton_Recon_pTHatMin_150to350->Add(h_WParton_Recon_pTHatMin_150);
	h_WParton_Recon_pTHatMin_150to350->Add(h_WParton_Recon_pTHatMin_200);
	h_WParton_Recon_pTHatMin_150to350->Add(h_WParton_Recon_pTHatMin_250);
	h_WParton_Recon_pTHatMin_150to350->Add(h_WParton_Recon_pTHatMin_300);
	h_WParton_Recon_pTHatMin_150to350->Add(h_WParton_Recon_pTHatMin_350);

	h_WParton_Truth_mHatMin_500to800->Add(h_WParton_Truth_mHatMin_500);
	h_WParton_Truth_mHatMin_500to800->Add(h_WParton_Truth_mHatMin_600);
	h_WParton_Truth_mHatMin_500to800->Add(h_WParton_Truth_mHatMin_700);
	h_WParton_Truth_mHatMin_500to800->Add(h_WParton_Truth_mHatMin_800);
	h_WParton_Truth_pTHatMin_150to350->Add(h_WParton_Truth_pTHatMin_150);
	h_WParton_Truth_pTHatMin_150to350->Add(h_WParton_Truth_pTHatMin_200);
	h_WParton_Truth_pTHatMin_150to350->Add(h_WParton_Truth_pTHatMin_250);
	h_WParton_Truth_pTHatMin_150to350->Add(h_WParton_Truth_pTHatMin_300);
	h_WParton_Truth_pTHatMin_150to350->Add(h_WParton_Truth_pTHatMin_350);

	TStyle* MyStyle = new TStyle("MyStyle", "My Style");
	MyStyle->SetLabelSize(0.07, "XY");
	MyStyle->SetTitleOffset(1.25, "Y");
	MyStyle->SetTitleSize(0.07, "XY");
	MyStyle->SetCanvasBorderMode(0);
	MyStyle->SetCanvasColor(10);
	MyStyle->SetPadBorderMode(0);
	MyStyle->SetPadColor(10);
	MyStyle->SetPadBottomMargin(0.15);
	MyStyle->SetPadLeftMargin(0.15);
	MyStyle->SetPaperSize(18, 24);
	MyStyle->SetStatFont(42);
	MyStyle->SetStatBorderSize(1);
	MyStyle->SetStatColor(10);
	MyStyle->SetStatFontSize(0.08);
	MyStyle->SetTitleBorderSize(1);
	MyStyle->SetTitleFont(62);
	MyStyle->SetTitleFontSize(0.08);
	MyStyle->SetTitleColor(10);
	MyStyle->SetOptStat(10);
	gROOT->SetStyle("MyStyle");

	TCanvas* c_WParton_ReconandTruth_HigherPhaseSpaceCuts = new TCanvas("c_WParton_ReconandTruth_HigherPhaseSpaceCuts", "Reconstructed and Truth V+Jet Backgrounds", 1000, 1000);
	c_WParton_ReconandTruth_HigherPhaseSpaceCuts->Divide(2, 2);
	c_WParton_ReconandTruth_HigherPhaseSpaceCuts->cd(1);
	gStyle->SetPalette(1);/*
	h_WParton_Truth_mHatMin_500to800->GetXaxis()->SetTitleSize(0.07);*/
	h_WParton_Truth_mHatMin_500to800->DrawClone("PFC NOSTACK");/*
	h_WParton_Truth_mHatMin_500to800->GetXaxis()->SetLabelSize(0.07);
	h_WParton_Truth_mHatMin_500to800->GetYaxis()->SetTitleSize(0.07);
	h_WParton_Truth_mHatMin_500to800->GetYaxis()->SetLabelSize(0.07);
	h_WParton_Truth_mHatMin_500to800->GetYaxis()->SetTitleOffset(1.25);*/
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Truth");

	c_WParton_ReconandTruth_HigherPhaseSpaceCuts->cd(2);
	gStyle->SetPalette(2);
	h_WParton_Truth_pTHatMin_150to350->DrawClone("PFC NOSTACK");/*
	h_WParton_Truth_pTHatMin_150to350->GetXaxis()->SetTitleSize(0.07);
	h_WParton_Truth_pTHatMin_150to350->GetXaxis()->SetLabelSize(0.07);
	h_WParton_Truth_pTHatMin_150to350->GetYaxis()->SetTitleSize(0.07);
	h_WParton_Truth_pTHatMin_150to350->GetYaxis()->SetLabelSize(0.07);
	h_WParton_Truth_pTHatMin_150to350->GetYaxis()->SetTitleOffset(1.25);*/
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Truth");

	c_WParton_ReconandTruth_HigherPhaseSpaceCuts->cd(3);
	gStyle->SetPalette(1);
	h_WParton_Recon_mHatMin_500to800->DrawClone("PFC NOSTACK");/*
	h_WParton_Recon_mHatMin_500to800->GetXaxis()->SetTitleSize(0.07);
	h_WParton_Recon_mHatMin_500to800->GetXaxis()->SetLabelSize(0.07);
	h_WParton_Recon_mHatMin_500to800->GetYaxis()->SetTitleSize(0.07);
	h_WParton_Recon_mHatMin_500to800->GetYaxis()->SetLabelSize(0.07);
	h_WParton_Recon_mHatMin_500to800->GetYaxis()->SetTitleOffset(1.25);*/
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Reconstructed");

	c_WParton_ReconandTruth_HigherPhaseSpaceCuts->cd(4);
	gStyle->SetPalette(2);
	h_WParton_Recon_pTHatMin_150to350->DrawClone("PFC NOSTACK");/*
	h_WParton_Recon_pTHatMin_150to350->GetXaxis()->SetTitleSize(0.07);
	h_WParton_Recon_pTHatMin_150to350->GetXaxis()->SetLabelSize(0.07);
	h_WParton_Recon_pTHatMin_150to350->GetYaxis()->SetTitleSize(0.07);
	h_WParton_Recon_pTHatMin_150to350->GetYaxis()->SetLabelSize(0.07);
	h_WParton_Recon_pTHatMin_150to350->GetYaxis()->SetTitleOffset(1.25);*/
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Reconstructed");

	// Writing to File

	std::cout << "Write to file..." << std::endl;

	OutputFile->cd();

	c_WParton_ReconandTruth_HigherPhaseSpaceCuts->Write();

	OutputFile->Close();

	std::cout << "Tidy..." << std::endl;

	std::cout << "Done!" << std::endl;

	return 0;

}