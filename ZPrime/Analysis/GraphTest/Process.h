#include "TFile.h"
#include "TH2.h"
#include "TF1.h"
#include "THStack.h"
#include "TLorentzVector.h"
#include "TRandom3.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TMath.h"
#include "TColor.h"
#include "TStyle.h"
#include "TVector.h"
#include "TError.h"
#include "TSystemDirectory.h"
#include "TSystemFile.h"
#include "TVector3.h"
#include "TArrayI.h"
#include "TGraph.h"

#include <iostream>
#include <utility>
#include <vector>

#include "TROOT.h"
#include "TSystem.h"
#include "TApplication.h"

#include "TString.h"
#include "TRandom3.h"
#include "TClonesArray.h"

#include "TLorentzVector.h"

#include "classes/DelphesClasses.h"

#include "ExRootAnalysis/ExRootTreeReader.h"
#include "ExRootAnalysis/ExRootTreeWriter.h"
#include "ExRootAnalysis/ExRootTreeBranch.h"
#include "ExRootAnalysis/ExRootResult.h"
#include "ExRootAnalysis/ExRootUtilities.h"

// Plots

TClonesArray * bEvent;
TClonesArray * bJet;
TClonesArray * bGenJet;
TClonesArray * bElectron;
TClonesArray * bMuon;
TClonesArray * bTruthLeptons;
TClonesArray * bMissingET;
TClonesArray * bGenMissingET;
TClonesArray * bTruthWZ;

// Output
TFile * OutputFile;

int main(int argc, char* argv[]);
