import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rc('xtick', labelsize=25) 
matplotlib.rc('ytick', labelsize=25)

e_mass = 0.00051099895
muon_mass = 0.1056583755
W_mass = 80.38

class LorentzVector:
    def __init__(self, pT, eta, phi, M):
        self.pT = pT
        self.eta = eta
        self.phi = phi
        self.M = M
        self.px = self.pT*np.cos(self.phi)
        self.py = self.pT*np.sin(self.phi)
        self.pz = self.pT*np.sinh(self.eta)
        self.p = self.pT*np.cosh(self.eta)
        self.E = np.sqrt(self.p**2 + self.M**2)
        
    def __repr__(self):
        return '{0}({1}, {2}, {3}, {4})'.format(self.__class__.__name__, self.pT, self.eta, self.phi, self.M)
    
    def __str__(self):
        return '{0}({1}, {2}, {3}, {4})'.format(self.__class__.__name__, self.pT, self.eta, self.phi, self.M)
    
    def __pos__(self):
        """
        Return a copy of this vector with the '+' operator applied to
        each element.
        """
        return self.__class__(+self.pT, +self.eta, +self.phi, +self.M)

    def __add__(self, v):
        """
        Return the addition of this vector with the vector 'v'.
        """
        px = self.px + v.px
        py = self.py + v.py
        pz = self.pz + v.pz
        E = self.E + v.E
        pT = np.sqrt(px**2 + py**2)
        p = np.sqrt(pT**2 + pz**2)
        phi = np.arctan(py/px)
        eta = np.arctanh(pz/p)
        M = np.sqrt(E**2 - px**2 - py**2 - pz**2)
        print(px, py, pz, E, pT, p, phi, eta, M)
        return self.__class__(pT, eta, phi, M)

lepton = [351.554, 0.0968438, -2.06567, 353.204]
missingET = [336.741, -1.23509, 1.09755]
genmissingET = [213.211, 2.07992, 1.02563]

a = lepton[3] - (lepton[0]*np.sinh(lepton[1]))
b = W_mass**2
b += (lepton[0]*np.cos(lepton[2]) + missingET[0]*np.cos(missingET[2]))**2
b += (lepton[0]*np.sin(lepton[2]) + missingET[0]*np.sin(missingET[2]))**2
b += (lepton[0]*np.sinh(lepton[1]))**2
b -= lepton[3]**2
b -= missingET[0]**2
b /= (-1*missingET[0])
c = lepton[3] + (lepton[0]*np.sinh(lepton[1]))
det = b**2 - (4*a*c)
if det > 0:
    eta_plus = np.log((-b+np.sqrt(det))/(2*a))
    eta_minus = np.log((-b-np.sqrt(det))/(2*a))
    # print(eta_plus, eta_minus)
    # print(np.cosh(eta_plus), np.cosh(eta_minus))
    # print(np.sinh(eta_plus), np.sinh(eta_minus))
    
    W_px = lepton[0]*np.cos(lepton[2]) + missingET[0]*np.cos(missingET[2])
    W_py = lepton[0]*np.sin(lepton[2]) + missingET[0]*np.sin(missingET[2])
    W_pz_plus = lepton[0]*np.sinh(lepton[1]) + missingET[0]*np.sinh(eta_plus)
    W_pz_minus = lepton[0]*np.sinh(lepton[1]) + missingET[0]*np.sinh(eta_minus)
    W_E_plus = lepton[0]*np.cosh(lepton[1]) + missingET[0]*np.cosh(eta_plus)
    W_E_minus = lepton[0]*np.cosh(lepton[1]) + missingET[0]*np.cosh(eta_minus)
    W_m_plus = np.sqrt(W_E_plus**2 - W_px**2 - W_py**2 - W_pz_plus**2)
    W_m_minus = np.sqrt(W_E_minus**2 - W_px**2 - W_py**2 - W_pz_minus**2)
    
    print('eta_plus\teta_minus')
    print(f'{eta_plus:.6f}\t{eta_minus:.6f}')
    # print([W_pz_plus, W_E_plus, W_m_plus])
    # print([W_pz_minus, W_E_minus, W_m_minus])
    
    theta_plus = 2*np.arctan(np.exp(-eta_plus))*(180/np.pi)
    theta_minus = 2*np.arctan(np.exp(-eta_minus))*(180/np.pi)
    theta_lep = 2*np.arctan(np.exp(-lepton[1]))*(180/np.pi)
    
    print(f'{theta_plus:.6f}\t{theta_minus:.6f}\t{(abs(theta_plus-theta_minus)):.6f}')
    
    print(f'{abs(eta_plus-lepton[1]):.6f}\t{abs(eta_minus-lepton[1]):.6f}')
    print(f'{abs(theta_plus-theta_lep):.6f}\t{abs(theta_minus-theta_lep):.6f}')
    
# vec_lepton = LorentzVector(262.647, 2.46594, -2.00572, e_mass)
# vec_missingET = LorentzVector(297.913, 0, -2.0952, 0)
# vec_genmissingET = LorentzVector(52.8236, 2.05287, -2.61479, 0)
# eta_range = np.arange(-5, 5, 0.01)
# deltaM = []
# for eta in eta_range:
#     vec_missingET.eta = eta
#     leptonandMET = vec_lepton + vec_missingET
#     deltaM.append(np.abs(leptonandMET.M - W_mass))

# fig = plt.figure(figsize=[8, 8])
# ax = fig.add_subplot(1, 1, 1)
# ax.plot(eta_range, np.array(deltaM))
# plt.show()

eta_range = np.arange(-5, 5, 0.01)
deltaM = []
# troughs = 0
# diff = 0
# min_x = 0
# max_x = 0
for eta in eta_range:
    W_px = lepton[0]*np.cos(lepton[2]) + missingET[0]*np.cos(missingET[2])
    W_py = lepton[0]*np.sin(lepton[2]) + missingET[0]*np.sin(missingET[2])
    W_pz = lepton[0]*np.sinh(lepton[1]) + missingET[0]*np.sinh(eta)
    W_E = lepton[0]*np.cosh(lepton[1]) + missingET[0]*np.cosh(eta)
    W_m = np.sqrt(W_E**2 - W_px**2 - W_py**2 - W_pz**2)
    deltaM.append(np.abs(W_m-W_mass))
    # if len(deltaM) > 2:
    #     diff = deltaM[-2] - deltaM[-3]
    #     if troughs == 0 and deltaM[-1] > deltaM[-2] and diff < 0:
    #             min_x = eta[-1]
    #             troughs += 1
    #     if troughs == 1 and deltaM[-1] > deltaM[-2] and diff < 0:
    #             max_x = eta[-1]
    #             troughs += 1
    
fig = plt.figure(figsize=[8, 8])
ax = fig.add_subplot(1, 1, 1)
ax.plot(eta_range, np.array(deltaM))
xmin, xmax = 2, 3
# ax.set_xlim(xmin, xmax)
# ax.set_ylim(0, W_mass)
ax.set_xlabel(r'$\eta$', fontsize=25)
ax.set_ylabel(r'$\left|\Delta M \right|$', fontsize=25)
plt.show()

eta_opt = eta_range[np.argmin(deltaM)]
print('{0:.2f}\t{1:.2f}'.format(eta_opt, np.min(deltaM)))

def Poisson(obs, exp):
    lambda_tothe_k = float(exp)**float(obs)
    e_tothe_minusr = np.exp(-exp)
    k_factorial = np.math.factorial(np.int(obs))
    return (lambda_tothe_k*e_tothe_minusr)/k_factorial

luminosity = np.arange(1, 31, 1)
signal = luminosity
background = luminosity*2
significance = []
for i, s in enumerate(signal):
    significance.append(Poisson(s+background[i], background[i]))
    
# fig = plt.figure(figsize=[8, 8])
# ax = fig.add_subplot(1, 1, 1)
# ax.plot(luminosity, significance)
# ax.set_yscale('log')
# plt.show()
