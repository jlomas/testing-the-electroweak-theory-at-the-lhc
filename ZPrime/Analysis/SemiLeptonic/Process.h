#include "TFile.h"
#include "TH2.h"
#include "TF1.h"
#include "THStack.h"
#include "TLorentzVector.h"
#include "TRandom3.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TMath.h"
#include "TColor.h"
#include "TStyle.h"
#include "TVector.h"
#include "TError.h"
#include "TSystemDirectory.h"
#include "TSystemFile.h"
#include "TVector3.h"
#include "TArrayI.h"
#include "TGraph.h"

#include <iostream>
#include <utility>
#include <vector>

#include "TROOT.h"
#include "TSystem.h"
#include "TApplication.h"

#include "TString.h"
#include "TRandom3.h"
#include "TClonesArray.h"

#include "TLorentzVector.h"

#include "classes/DelphesClasses.h"

#include "ExRootAnalysis/ExRootTreeReader.h"
#include "ExRootAnalysis/ExRootTreeWriter.h"
#include "ExRootAnalysis/ExRootTreeBranch.h"
#include "ExRootAnalysis/ExRootResult.h"
#include "ExRootAnalysis/ExRootUtilities.h"

// Plots

TClonesArray * bEvent;
TClonesArray * bJet;
TClonesArray * bGenJet;
TClonesArray * bElectron;
TClonesArray * bMuon;
TClonesArray * bTruthLeptons;
TClonesArray * bMissingET;
TClonesArray * bGenMissingET;
TClonesArray * bParticle;
TClonesArray * bTruthWZ;

// Output
TFile * OutputFile;

TH1D * h_EventCount;
TH1D * h_WeightCount;

TH1D * h_ZprimeMass_Basic;

TH1D * h_JetPairMass;
TH1D * h_JetPairMass_fromW;
TH1D * h_JetSelectedPt;
TH1D * h_JetUnselectedPt;
TH1D * h_LeptonandMETMass;
TH1D* h_ZprimeMass;

TH2D * h_JetDeltaPt_pT;
TH2D * h_JetSigmaPt_pT;
TH2D * h_JetDeltaPhi_pT;
TH2D * h_JetDeltaEta_pT;
TH1D * h_JetDeltaPt_0_20;
TH1D * h_JetDeltaPt_20_30;
TH1D * h_JetDeltaPt_30_40;
TH1D * h_JetDeltaPt_40_50;
TH1D * h_JetDeltaPt_50_60;
TH1D * h_JetDeltaPt_60_80;
TH1D * h_JetDeltaPt_80_100;
TH1D * h_JetDeltaPhi_0_20;
TH1D * h_JetDeltaPhi_20_30;
TH1D * h_JetDeltaPhi_30_40;
TH1D * h_JetDeltaPhi_40_50;
TH1D * h_JetDeltaPhi_50_60;
TH1D * h_JetDeltaPhi_60_80;
TH1D * h_JetDeltaPhi_80_100;

TCanvas* c_LeptonfromLeptonSources;
THStack* h_LeptonfromLeptonSources;
TH1D* h_ElectronfromLeptonSources;
TH1D* h_MuonfromLeptonSources;
TH1D * h_LeptonfromLepton_pTLoss;
TH2D * h_LeptonfromLepton_pTLossvsPartLevels;
TH1D * h_TruthLeptonfromW_pT;
TH1D * h_TruthLeptonnotfromW_pT;
TH1D * h_TruthElectronfromW_pT;
TH1D * h_TruthElectronnotfromW_pT;
TH1D * h_TruthMuonfromW_pT;
TH1D * h_TruthMuonnotfromW_pT;
THStack * h_TruthLeptonfromW_Stack_pT;
THStack * h_TruthLeptonnotfromW_Stack_pT;
TCanvas * c_TruthLeptonfromW_Stack_pT;
TCanvas * c_TruthLeptonnotfromW_Stack_pT;
TCanvas * c_TruthLeptonSource_pT;

TH1D * h_ReconLeptonfromW_pT;
TH1D * h_ReconLeptonnotfromW_pT;
TH1D * h_ReconElectronfromW_pT;
TH1D * h_ReconElectronnotfromW_pT;
TH1D * h_ReconMuonfromW_pT;
TH1D * h_ReconMuonnotfromW_pT;
THStack * h_ReconLeptonfromW_Stack_pT;
THStack * h_ReconLeptonnotfromW_Stack_pT;
TCanvas * c_ReconLeptonfromW_Stack_pT;
TCanvas * c_ReconLeptonnotfromW_Stack_pT;
TCanvas * c_ReconLeptonSource_pT;
TH1D * h_ElectronSources;
TH1D * h_MuonSources;
THStack * h_LeptonSources;
TCanvas * c_LeptonSources;

TH1D * h_ReconLeptonfromW_PassCut_pT;
TH1D * h_ReconLeptonnotfromW_PassCut_pT;

TGraph * h_LeptonfromW_PassCutPerc;
TGraph * h_LeptonnotfromW_PassCutPerc;
TGraph * h_LeptonfromW_PassCutRatio;
TGraph * h_LeptonfromW_PassCutGoodMinusBad;

TH2D * h_LeptonDeltaPt_pT;
TH2D * h_LeptonSigmaPt_pT;
TH2D * h_LeptonDeltaPhi_pT;
TH2D * h_LeptonDeltaEta_pT;
TH1D * h_LeptonDeltaPt_0_20;
TH1D * h_LeptonDeltaPt_20_30;
TH1D * h_LeptonDeltaPt_30_40;
TH1D * h_LeptonDeltaPt_40_50;
TH1D * h_LeptonDeltaPt_50_60;
TH1D * h_LeptonDeltaPt_60_80;
TH1D * h_LeptonDeltaPt_80_100;
TH1D * h_LeptonDeltaPhi_0_20;
TH1D * h_LeptonDeltaPhi_20_30;
TH1D * h_LeptonDeltaPhi_30_40;
TH1D * h_LeptonDeltaPhi_40_50;
TH1D * h_LeptonDeltaPhi_50_60;
TH1D * h_LeptonDeltaPhi_60_80;
TH1D * h_LeptonDeltaPhi_80_100;

TH1D * h_LeptonTransverseMass;
TH1D * h_Zprime_withTransverseMass;

TH1D * h_MissingETEta_PlusvsMinus;
TH1D * h_MissingETEta_SmallvsLargeMag;
TH1D * h_MissingETEta_Truth;
TH1D * h_MissingETEta_Recon;
TH1D * h_MissingETEta_Comp;
TH1D * h_TruthMissingETEta_PlusvsMinus;
TH1D * h_TruthMissingETEta_SmallvsLargeMag;
TH1D * h_TruthMissingETEta_Recon;
TH1D * h_TruthMissingETEta_Comp;
TH1D * h_TruthfromWMissingETEta_PlusvsMinus;
TH1D * h_TruthfromWMissingETEta_SmallvsLargeMag;
TH1D * h_TruthfromWMissingETEta_Recon;
TH1D * h_TruthfromWMissingETEta_Comp;
TGraph * h_EtaGoodnessDist_Event1;
TH1D * h_MinPointMissingETEta_Recon;
TH1D * h_MinPointMissingETEta_Comp;
TH1D * h_MinPointMissingETEta_PosDetComp;
TH1D * h_MinPointMissingETEta_NegDetComp;
TH1D * h_MinPointMissingETEta_PosDetGoodvsBad;
TH1D * h_Zprime_withEtaCalc;
TH1D * h_Zprime_withEtaPlus;
TH1D * h_Zprime_withEtaMinus;
TH1D * h_Zprime_withEtaGood;
TH1D * h_Zprime_withEtaBad;
TH1D * h_Zprime_withTransverseLeptons;
TH1D * h_Zprime_withMinPointMissingETEta;
TH1D * h_Zprime_withMinPointMissingETEta_PosDet;
TH1D * h_Zprime_withMinPointMissingETEta_NegDet;

TH2D * h_METDeltaPt_MET;
TH2D * h_METSigmaPt_MET;
TH2D * h_METDeltaPhi_MET;
TH2D * h_METDeltaPt_MET_wlep;
TH2D * h_METSigmaPt_MET_wlep;
TH2D * h_METDeltaPhi_MET_wlep;
TH2D * h_METDeltaEta_MET_wlep;
TH2D * h_METDeltaEta_MET_wlep_calc;
TH1D * h_METDeltaPt_0_20;
TH1D * h_METDeltaPt_20_30;
TH1D * h_METDeltaPt_30_40;
TH1D * h_METDeltaPt_40_50;
TH1D * h_METDeltaPt_50_60;
TH1D * h_METDeltaPt_60_80;
TH1D * h_METDeltaPt_80_100;
TH1D * h_METDeltaPhi_0_20;
TH1D * h_METDeltaPhi_20_30;
TH1D * h_METDeltaPhi_30_40;
TH1D * h_METDeltaPhi_40_50;
TH1D * h_METDeltaPhi_50_60;
TH1D * h_METDeltaPhi_60_80;
TH1D * h_METDeltaPhi_80_100;
TH1D* h_METDeltaEtaCalc_0_20;
TH1D* h_METDeltaEtaCalc_20_30;
TH1D* h_METDeltaEtaCalc_30_40;
TH1D* h_METDeltaEtaCalc_40_50;
TH1D* h_METDeltaEtaCalc_50_60;
TH1D* h_METDeltaEtaCalc_60_80;
TH1D* h_METDeltaEtaCalc_80_100;

TH1D * h_METDeltapx;
TH1D * h_METDeltapy;
TH1D * h_METSigmapx;
TH1D * h_METSigmapy;

TH1D * h_MET_PosDet;
TH1D * h_MET_NegDet;
TH1D * h_MET_DeltaM_NegDet;
TH2D * h_MET_METvsDeltaM_NegDet;
TH2D * h_MET_DeltaM_NegDetvsZprimeMass;
TH1D * h_MET_ZprimeMass_DeltaM_NegDet_0_10;
TH1D * h_MET_ZprimeMass_DeltaM_NegDet_10_20;
TH1D * h_MET_ZprimeMass_DeltaM_NegDet_20_30;
TH1D * h_MET_ZprimeMass_DeltaM_NegDet_30_40;
TH1D * h_MET_ZprimeMass_DeltaM_NegDet_40_50;

ExRootTreeReader * InitReader(const TString FilePath);

void Process(ExRootTreeReader * treeReader);

void ClearBranches();

int main(int argc, char* argv[]);
