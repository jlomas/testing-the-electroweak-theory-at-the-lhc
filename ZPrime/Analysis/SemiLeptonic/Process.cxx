
#include "Process.h"

bool Debug = false;
Int_t ToPrint = 0;
Double_t e_mass = 0.00051099895;
Double_t muon_mass = 0.1056583755;
Double_t W_mass = 80.38;
Double_t Zprime_mass = 1000;

Double_t Lepton_pTCut = 41.25;

int main(int argc, char* argv[]) {

	// Input Delphes File

	const TString InputFile = argv[1];
	const TString OutputFileName = argv[2];

	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "Running Process"  << std::endl;
	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "InputFile = " << InputFile << std::endl;
	std::cout << "OutputFileName = " << OutputFileName << std::endl;
	std::cout << "-------------------------------------------------------------"  << std::endl;

	ExRootTreeReader * reader = NULL;
	reader = InitReader(InputFile);

	//------------------------------------
	// Declare the output
	//------------------------------------

	OutputFile = new TFile(OutputFileName,"recreate");

	OutputFile->cd();

	h_EventCount = new TH1D("h_EventCount","",1,0,1);
	h_WeightCount = new TH1D("h_WeightCount","",1,0,1);

	h_ZprimeMass_Basic = new TH1D("h_ZprimeMass_Basic", "; Z' Mass [GeV]; Events", 200, 0, 4000);

	h_JetPairMass = new TH1D("h_JetPairMass", "; Mass of Jet Pair [GeV]; Events", 200, 0, 2000);
	h_JetPairMass_fromW = new TH1D("h_JetPairMass_fromW", "; Mass of Jet Pair [GeV]; Events", 200, 0, 2000);
	h_JetSelectedPt = new TH1D("h_JetSelectedPt", "; p_{T} of Selected Jets [GeV]; Events", 200, 0, 1000);
	h_JetUnselectedPt = new TH1D("h_JetUnselectedPt", "; p_{T} of Unselected Jets [GeV]; Events", 200, 0, 1000);
	h_LeptonandMETMass = new TH1D("h_LeptonandMETMass", "; Mass of Lepton and MET [GeV]; Events", 200, 0, 2000);
	h_ZprimeMass = new TH1D("h_ZprimeMass", "; Z' Mass [GeV]; Events", 200, 0, 4000);

	h_JetDeltaPt_pT = new TH2D("h_JetDeltaPt_pT", "; p_{T} [GeV]; #Delta p_{T} [GeV]", 100, 0, 1500, 100, -500, 500);
	h_JetSigmaPt_pT = new TH2D("h_JetSigmaPt_pT", "; p_{T} [GeV]; #Delta p_{T} / p_{T}", 100, 0, 1500, 100, -5, 5);
	h_JetDeltaPhi_pT = new TH2D("h_JetDeltaPhi_pT", "; p_{T} [GeV]; #Delta #phi [rad]", 100, 0, 1500, 100, -3.2, 3.2);
	h_JetDeltaEta_pT = new TH2D("h_JetDeltaEta_pT", "; p_{T} [GeV]; #Delta #eta [rad]", 100, 0, 1500, 100, -0.5, 0.5);
	h_JetDeltaPt_0_20 = new TH1D("h_JetDeltaPt_0_20", "; Jet #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_JetDeltaPt_20_30 = new TH1D("h_JetDeltaPt_20_30", "; Jet #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_JetDeltaPt_30_40 = new TH1D("h_JetDeltaPt_30_40", "; Jet #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_JetDeltaPt_40_50 = new TH1D("h_JetDeltaPt_40_50", "; Jet #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_JetDeltaPt_50_60 = new TH1D("h_JetDeltaPt_50_60", "; Jet #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_JetDeltaPt_60_80 = new TH1D("h_JetDeltaPt_60_80", "; Jet #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_JetDeltaPt_80_100 = new TH1D("h_JetDeltaPt_80_100", "; Jet #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_JetDeltaPhi_0_20 = new TH1D("h_JetDeltaPhi_0_20", "; Jet #Delta#phi [rad]; Events", 200, -TMath::Pi(), TMath::Pi());
	h_JetDeltaPhi_20_30 = new TH1D("h_JetDeltaPhi_20_30", "; Jet #Delta#phi [rad]; Events", 200, -TMath::Pi(), TMath::Pi());
	h_JetDeltaPhi_30_40 = new TH1D("h_JetDeltaPhi_30_40", "; Jet #Delta#phi [rad]; Events", 200, -TMath::Pi(), TMath::Pi());
	h_JetDeltaPhi_40_50 = new TH1D("h_JetDeltaPhi_40_50", "; Jet #Delta#phi [rad]; Events", 200, -TMath::Pi(), TMath::Pi());
	h_JetDeltaPhi_50_60 = new TH1D("h_JetDeltaPhi_50_60", "; Jet #Delta#phi [rad]; Events", 200, -TMath::Pi(), TMath::Pi());
	h_JetDeltaPhi_60_80 = new TH1D("h_JetDeltaPhi_60_80", "; Jet #Delta#phi [rad]; Events", 200, -TMath::Pi(), TMath::Pi());
	h_JetDeltaPhi_80_100 = new TH1D("h_JetDeltaPhi_80_100", "; Jet #Delta#phi [rad]; Events", 200, -TMath::Pi(), TMath::Pi());

	h_LeptonfromLeptonSources = new THStack("h_LeptonfromLeptonSources", "; First Non-Lepton Mother Particle; Events");
	h_ElectronfromLeptonSources = new TH1D("Electrons", "; Lepton Source; Events", 5, 0.5, 5.5);
	h_MuonfromLeptonSources = new TH1D("Muons", "; Lepton Source; Events", 5, 0.5, 5.5);
	h_LeptonfromLepton_pTLoss = new TH1D("h_LeptonfromLepton_pTLoss", "; #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_LeptonfromLepton_pTLossvsPartLevels = new TH2D("h_LeptonfromLepton_pTLossvsPartLevels", "; Number of Decay Levels; #Delta p_{T} [GeV]", 6, 0, 6, 200, -500, 500);

	h_TruthLeptonfromW_pT = new TH1D("h_TruthLeptonfromW_pT", "; p_{T} [GeV]; Events / 10 GeV", 100, 0, 1000);
	h_TruthLeptonnotfromW_pT = new TH1D("h_TruthLeptonnotfromW_pT", "; p_{T} [GeV]; Events / 10 GeV", 100, 0, 1000);
	h_TruthElectronfromW_pT = new TH1D("Electrons", "; p_{T} [GeV]; Events / 10 GeV", 100, 0, 1000);
	h_TruthElectronnotfromW_pT = new TH1D("Electrons", "; p_{T} [GeV]; Events / 10 GeV", 100, 0, 1000);
	h_TruthMuonfromW_pT = new TH1D("Muons", "; p_{T} [GeV]; Events / 10 GeV", 100, 0, 1000);
	h_TruthMuonnotfromW_pT = new TH1D("Muons", "; p_{T} [GeV]; Events / 10 GeV", 100, 0, 1000);
	h_TruthLeptonfromW_Stack_pT = new THStack("h_TruthLeptonfromW_Stack_pT", "; p_{T} [GeV]; Events / 10 GeV");
	h_TruthLeptonnotfromW_Stack_pT = new THStack("h_TruthLeptonnotfromW_Stack_pT", "; p_{T} [GeV]; Events / 10 GeV");

	h_ReconLeptonfromW_pT = new TH1D("h_ReconLeptonfromW_pT", "; p_{T} [GeV]; Events / 10 GeV", 100, 0, 1000);
	h_ReconLeptonnotfromW_pT = new TH1D("h_ReconLeptonnotfromW_pT", "; p_{T} [GeV]; Events / 10 GeV", 100, 0, 1000);
	h_ReconElectronfromW_pT = new TH1D("Electrons", "; p_{T} [GeV]; Events / 10 GeV", 100, 0, 1000);
	h_ReconElectronnotfromW_pT = new TH1D("Electrons", "; p_{T} [GeV]; Events / 10 GeV", 100, 0, 1000);
	h_ReconMuonfromW_pT = new TH1D("Muons", "; p_{T} [GeV]; Events / 10 GeV", 100, 0, 1000);
	h_ReconMuonnotfromW_pT = new TH1D("Muons", "; p_{T} [GeV]; Events / 10 GeV", 100, 0, 1000);
	h_ReconLeptonfromW_Stack_pT = new THStack("h_ReconLeptonfromW_Stack_pT", "; p_{T} [GeV]; Events / 10 GeV");
	h_ReconLeptonnotfromW_Stack_pT = new THStack("h_ReconLeptonnotfromW_Stack_pT", "; p_{T} [GeV]; Events / 10 GeV");

	h_LeptonSources = new THStack("h_LeptonSources", "; Lepton Source; Events");
	h_ElectronSources = new TH1D("Electrons", "; Lepton Source; Events", 5, 0.5, 5.5);
	h_MuonSources = new TH1D("Muons", "; Lepton Source; Events", 5, 0.5, 5.5);


	h_ReconLeptonfromW_PassCut_pT = new TH1D("h_ReconLeptonfromW_PassCut_pT", "; p_{T} [GeV]; Events", 200, 0, 1000);
	h_ReconLeptonnotfromW_PassCut_pT = new TH1D("h_ReconLeptonnotfromW_PassCut_pT", "; p_{T} [GeV]; Events", 200, 0, 1000);

	h_LeptonDeltaPt_pT = new TH2D("h_LeptonDeltaPt_pT", "; p_{T} [GeV]; #Delta p_{T} [GeV]", 100, 0, 1500, 100, -500, 500);
	h_LeptonSigmaPt_pT = new TH2D("h_LeptonSigmaPt_pT", "; p_{T} [GeV]; #Delta p_{T} / p_{T}", 100, 0, 1500, 100, -5, 5);
	h_LeptonDeltaPhi_pT = new TH2D("h_LeptonDeltaPhi_pT", "; p_{T} [GeV]; #Delta #phi [rad]", 100, 0, 1500, 100, -3.2, 3.2);
	h_LeptonDeltaEta_pT = new TH2D("h_LeptonDeltaEta_pT", "; p_{T} [GeV]; #Delta #eta [rad]", 100, 0, 1500, 100, -0.5, 0.5);
	h_LeptonDeltaPt_0_20 = new TH1D("h_LeptonDeltaPt_0_20", "; Lepton #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_LeptonDeltaPt_20_30 = new TH1D("h_LeptonDeltaPt_20_30", "; Lepton #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_LeptonDeltaPt_30_40 = new TH1D("h_LeptonDeltaPt_30_40", "; Lepton #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_LeptonDeltaPt_40_50 = new TH1D("h_LeptonDeltaPt_40_50", "; Lepton #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_LeptonDeltaPt_50_60 = new TH1D("h_LeptonDeltaPt_50_60", "; Lepton #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_LeptonDeltaPt_60_80 = new TH1D("h_LeptonDeltaPt_60_80", "; Lepton #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_LeptonDeltaPt_80_100 = new TH1D("h_LeptonDeltaPt_80_100", "; Lepton #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_LeptonDeltaPhi_0_20 = new TH1D("h_LeptonDeltaPhi_0_20", "; Lepton #Delta#phi [rad]; Events", 200, -TMath::Pi(), TMath::Pi());
	h_LeptonDeltaPhi_20_30 = new TH1D("h_LeptonDeltaPhi_20_30", "; Lepton #Delta#phi [rad]; Events", 200, -TMath::Pi(), TMath::Pi());
	h_LeptonDeltaPhi_30_40 = new TH1D("h_LeptonDeltaPhi_30_40", "; Lepton #Delta#phi [rad]; Events", 200, -TMath::Pi(), TMath::Pi());
	h_LeptonDeltaPhi_40_50 = new TH1D("h_LeptonDeltaPhi_40_50", "; Lepton #Delta#phi [rad]; Events", 200, -TMath::Pi(), TMath::Pi());
	h_LeptonDeltaPhi_50_60 = new TH1D("h_LeptonDeltaPhi_50_60", "; Lepton #Delta#phi [rad]; Events", 200, -TMath::Pi(), TMath::Pi());
	h_LeptonDeltaPhi_60_80 = new TH1D("h_LeptonDeltaPhi_60_80", "; Lepton #Delta#phi [rad]; Events", 200, -TMath::Pi(), TMath::Pi());
	h_LeptonDeltaPhi_80_100 = new TH1D("h_LeptonDeltaPhi_80_100", "; Lepton #Delta#phi [rad]; Events", 200, -TMath::Pi(), TMath::Pi());
	
	h_LeptonTransverseMass = new TH1D("h_LeptonTransverseMass", "; Transverse Mass of Lepton and MET [GeV]; Events", 200, 0, 2000);
	h_Zprime_withTransverseMass = new TH1D("h_Zprime_withTransverseMass", "; Mass of Z' [GeV]; Events", 200, 0, 4000);
	
	h_MissingETEta_PlusvsMinus = new TH1D("h_MissingETEta_PlusvsMinus", "; Which of #eta_{+}/#eta_{-} gives the correct result; Events", 3, -1, 2);
	h_MissingETEta_SmallvsLargeMag = new TH1D("h_MissingETEta_SmallvsLargeMag", "; Which of the lower/higher magnitude values of #eta gives the correct result; Events", 3, -1, 2);
	h_MissingETEta_PlusvsMinus->SetMinimum(0);
	h_MissingETEta_SmallvsLargeMag->SetMinimum(0);
	h_MissingETEta_Truth = new TH1D("h_MissingETEta_Truth", "; Truth #eta of MissingET; Events", 200, -10, 10);
	h_MissingETEta_Recon = new TH1D("h_MissingETEta_Recon", "; Reconstructed #eta of MissingET; Events", 200, -10, 10);
	h_MissingETEta_Comp = new TH1D("h_MissingETEta_Comp", "; #Delta#eta between Truth/Reconstructed MissingET; Events", 200, -10, 10);
	
	h_TruthMissingETEta_PlusvsMinus = new TH1D("h_TruthMissingETEta_PlusvsMinus", "; Which of #eta_{+}/#eta_{-} gives the correct result; Events", 3, -1, 2);
	h_TruthMissingETEta_SmallvsLargeMag = new TH1D("h_TruthMissingETEta_SmallvsLargeMag", "; Which of the lower/higher magnitude values of #eta gives the correct result; Events", 3, -1, 2);
	h_TruthMissingETEta_PlusvsMinus->SetMinimum(0);
	h_TruthMissingETEta_SmallvsLargeMag->SetMinimum(0);
	h_TruthMissingETEta_Recon = new TH1D("h_TruthMissingETEta_Recon", "; Reconstructed #eta of MissingET; Events", 200, -10, 10);
	h_TruthMissingETEta_Comp = new TH1D("h_TruthMissingETEta_Comp", "; #Delta#eta between Truth/Reconstructed MissingET; Events", 200, -10, 10);
	
	h_TruthfromWMissingETEta_PlusvsMinus = new TH1D("h_TruthfromWMissingETEta_PlusvsMinus", "; Which of #eta_{+}/#eta_{-} gives the correct result; Events", 3, -1, 2);
	h_TruthfromWMissingETEta_SmallvsLargeMag = new TH1D("h_TruthfromWMissingETEta_SmallvsLargeMag", "; Which of the lower/higher magnitude values of #eta gives the correct result; Events", 3, -1, 2);
	h_TruthfromWMissingETEta_PlusvsMinus->SetMinimum(0);
	h_TruthfromWMissingETEta_SmallvsLargeMag->SetMinimum(0);
	h_TruthfromWMissingETEta_Recon = new TH1D("h_TruthfromWMissingETEta_Recon", "; Reconstructed #eta of MissingET; Events", 200, -10, 10);
	h_TruthfromWMissingETEta_Comp = new TH1D("h_TruthfromWMissingETEta_Comp", "; #Delta#eta between Truth/Reconstructed MissingET; Events", 200, -10, 10);
	
	h_MinPointMissingETEta_Recon = new TH1D("h_MinPointMissingETEta_Recon", "; Reconstructed #eta of MissingET; Events", 200, -10, 10);
	h_MinPointMissingETEta_Comp = new TH1D("h_MinPointMissingETEta_Comp", "; #Delta#eta between Truth/Reconstructed MissingET; Events", 200, -10, 10);
	h_MinPointMissingETEta_PosDetComp = new TH1D("h_MinPointMissingETEta_PosDetComp", "; #Delta#eta between Truth/Reconstructed MissingET; Events", 200, -10, 10);
	h_MinPointMissingETEta_NegDetComp = new TH1D("h_MinPointMissingETEta_NegDetComp", "; #Delta#eta between Truth/Reconstructed MissingET; Events", 200, -10, 10);
	h_MinPointMissingETEta_PosDetGoodvsBad = new TH1D("h_MinPointMissingETEta_PosDetGoodvsBad", "; Whether MinPoint calculation of Missing #eta gives the correct result or not; Events", 2, 0, 2);
	h_MinPointMissingETEta_PosDetGoodvsBad->SetMinimum(0);
	
	h_Zprime_withEtaCalc = new TH1D("h_Zprime_withEtaCalc", "; Mass of Z' [GeV]; Events", 200, 0, 4000);
	h_Zprime_withEtaPlus = new TH1D("h_Zprime_withEtaPlus", "; Mass of Z' [GeV]; Events", 200, 0, 4000);
	h_Zprime_withEtaMinus = new TH1D("h_Zprime_withEtaMinus", "; Mass of Z' [GeV]; Events", 200, 0, 4000);
	h_Zprime_withEtaGood = new TH1D("h_Zprime_withEtaGood", "; Mass of Z' [GeV]; Events", 200, 0, 4000);
	h_Zprime_withEtaBad = new TH1D("h_Zprime_withEtaBad", "; Mass of Z' [GeV]; Events", 200, 0, 4000);
	h_Zprime_withTransverseLeptons = new TH1D("h_Zprime_withTransverseLeptons", "; Mass of Z' [GeV]; Events", 200, 0, 4000);
	h_Zprime_withMinPointMissingETEta = new TH1D("h_Zprime_withMinPointMissingETEta", "; Mass of Z' [GeV]; Events", 200, 0, 4000);
	h_Zprime_withMinPointMissingETEta_PosDet = new TH1D("h_Zprime_withMinPointMissingETEta_PosDet", "; Mass of Z' [GeV]; Events", 200, 0, 4000);
	h_Zprime_withMinPointMissingETEta_NegDet = new TH1D("h_Zprime_withMinPointMissingETEta_NegDet", "; Mass of Z' [GeV]; Events", 200, 0, 4000);
	
	h_METDeltaPt_MET = new TH2D("h_METDeltaPt_MET", "; E_{T}^{Missing} [GeV]; #Delta p_{T} [GeV]", 100, 0, 1500, 100, -500, 500);
	h_METSigmaPt_MET = new TH2D("h_METSigmaPt_MET", "; E_{T}^{Missing} [GeV]; #Delta p_{T} / p_{T}", 100, 0, 1500, 100, -5, 5);
	h_METDeltaPhi_MET = new TH2D("h_METDeltaPhi_MET", "; E_{T}^{Missing} [GeV]; #Delta #phi [rad]", 100, 0, 1500, 100, -3.2, 3.2);
	h_METDeltaPt_MET_wlep = new TH2D("h_METDeltaPt_MET_wlep", "; E_{T}^{Missing} [GeV]; #Delta p_{T} [GeV]", 100, 0, 1500, 100, -500, 500);
	h_METSigmaPt_MET_wlep = new TH2D("h_METSigmaPt_MET_wlep", "; E_{T}^{Missing} [GeV]; #Delta p_{T} / p_{T}", 100, 0, 1500, 100, -5, 5);
	h_METDeltaPhi_MET_wlep = new TH2D("h_METDeltaPhi_MET_wlep", "; E_{T}^{Missing} [GeV]; #Delta#phi [rad]", 100, 0, 1500, 100, -3.2, 3.2);
	h_METDeltaEta_MET_wlep = new TH2D("h_METDeltaEta_MET_wlep", "; E_{T}^{Missing} [GeV]; #Delta#eta [rad]", 100, 0, 1500, 200, -10, 10);
	h_METDeltaEta_MET_wlep_calc = new TH2D("h_METDeltaEta_MET_wlep_calc", "; E_{T}^{Missing} [GeV]; #Delta#eta [rad]", 100, 0, 1500, 200, -10, 10);
	h_METDeltaPt_0_20 = new TH1D("h_METDeltaPt_0_20", "; E_{T}^{Missing} #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_METDeltaPt_20_30 = new TH1D("h_METDeltaPt_20_30", "; E_{T}^{Missing} #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_METDeltaPt_30_40 = new TH1D("h_METDeltaPt_30_40", "; E_{T}^{Missing} #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_METDeltaPt_40_50 = new TH1D("h_METDeltaPt_40_50", "; E_{T}^{Missing} #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_METDeltaPt_50_60 = new TH1D("h_METDeltaPt_50_60", "; E_{T}^{Missing} #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_METDeltaPt_60_80 = new TH1D("h_METDeltaPt_60_80", "; E_{T}^{Missing} #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_METDeltaPt_80_100 = new TH1D("h_METDeltaPt_80_100", "; E_{T}^{Missing} #Delta p_{T} [GeV]; Events", 200, -500, 500);
	h_METDeltaPhi_0_20 = new TH1D("h_METDeltaPhi_0_20", "; E_{T}^{Missing} #Delta#phi [rad]; Events", 200, -TMath::Pi(), TMath::Pi());
	h_METDeltaPhi_20_30 = new TH1D("h_METDeltaPhi_20_30", "; E_{T}^{Missing} #Delta#phi [rad]; Events", 200, -TMath::Pi(), TMath::Pi());
	h_METDeltaPhi_30_40 = new TH1D("h_METDeltaPhi_30_40", "; E_{T}^{Missing} #Delta#phi [rad]; Events", 200, -TMath::Pi(), TMath::Pi());
	h_METDeltaPhi_40_50 = new TH1D("h_METDeltaPhi_40_50", "; E_{T}^{Missing} #Delta#phi [rad]; Events", 200, -TMath::Pi(), TMath::Pi());
	h_METDeltaPhi_50_60 = new TH1D("h_METDeltaPhi_50_60", "; E_{T}^{Missing} #Delta#phi [rad]; Events", 200, -TMath::Pi(), TMath::Pi());
	h_METDeltaPhi_60_80 = new TH1D("h_METDeltaPhi_60_80", "; E_{T}^{Missing} #Delta#phi [rad]; Events", 200, -TMath::Pi(), TMath::Pi());
	h_METDeltaPhi_80_100 = new TH1D("h_METDeltaPhi_80_100", "; E_{T}^{Missing} #Delta#phi [rad]; Events", 200, -TMath::Pi(), TMath::Pi());
	h_METDeltaEtaCalc_0_20 = new TH1D("h_METDeltaEtaCalc_0_20", "; E_{T}^{Missing} #Delta#eta [rad]; Events", 200, -10, 10);
	h_METDeltaEtaCalc_20_30 = new TH1D("h_METDeltaEtaCalc_20_30", "; E_{T}^{Missing} #Delta#eta [rad]; Events", 200, -10, 10);
	h_METDeltaEtaCalc_30_40 = new TH1D("h_METDeltaEtaCalc_30_40", "; E_{T}^{Missing} #Delta#eta [rad]; Events", 200, -10, 10);
	h_METDeltaEtaCalc_40_50 = new TH1D("h_METDeltaEtaCalc_40_50", "; E_{T}^{Missing} #Delta#eta [rad]; Events", 200, -10, 10);
	h_METDeltaEtaCalc_50_60 = new TH1D("h_METDeltaEtaCalc_50_60", "; E_{T}^{Missing} #Delta#eta [rad]; Events", 200, -10, 10);
	h_METDeltaEtaCalc_60_80 = new TH1D("h_METDeltaEtaCalc_60_80", "; E_{T}^{Missing} #Delta#eta [rad]; Events", 200, -10, 10);
	h_METDeltaEtaCalc_80_100 = new TH1D("h_METDeltaEtaCalc_80_100", "; E_{T}^{Missing} #Delta#eta [rad]; Events", 200, -10, 10);
	
	h_METDeltapx = new TH1D("h_METDeltapx", "; E_{T}^{Missing} #Delta p_{x} [GeV]; Events", 200, -1000, 1000);
	h_METDeltapy = new TH1D("h_METDeltapy", "; E_{T}^{Missing} #Delta p_{y} [GeV]; Events", 200, -1000, 1000);
	h_METSigmapx = new TH1D("h_METSigmapx", "; E_{T}^{Missing} #Delta p_{x} / p_{x}; Events", 200, -50, 50);
	h_METSigmapy = new TH1D("h_METSigmapy", "; E_{T}^{Missing} #Delta p_{y} / p_{y}; Events", 200, -50, 50);
	
	h_MET_PosDet = new TH1D("h_MET_PosDet", "; E_{T}^{Missing} [GeV]; Events", 200, 0, 1000);
	h_MET_NegDet = new TH1D("h_MET_NegDet", "; E_{T}^{Missing} [GeV]; Events", 200, 0, 1000);
	h_MET_METvsDeltaM_NegDet = new TH2D("h_MET_METvsDeltaM_NegDet", "; E_{T}^{Missing} [GeV]; Minimum #Delta M [GeV]", 100, 0, 1500, 100, 0, 1000);
	h_MET_DeltaM_NegDet = new TH1D("h_MET_DeltaM_NegDet", "; #Delta M [GeV]; Events", 200, 0, 900);
	h_MET_DeltaM_NegDetvsZprimeMass = new TH2D("h_MET_DeltaM_NegDetvsZprimeMass", "; Minimum #Delta M [GeV]; #Delta M of Z' [GeV]", 100, 0, 1000, 100, -1000, 1000);
	h_MET_ZprimeMass_DeltaM_NegDet_0_10 = new TH1D("h_MET_ZprimeMass_DeltaM_NegDet_0_10", "; Mass of Z' [GeV]; Events", 200, 0, 2000);
	h_MET_ZprimeMass_DeltaM_NegDet_10_20 = new TH1D("h_MET_ZprimeMass_DeltaM_NegDet_10_20", "; Mass of Z' [GeV]; Events", 200, 0, 2000);
	h_MET_ZprimeMass_DeltaM_NegDet_20_30 = new TH1D("h_MET_ZprimeMass_DeltaM_NegDet_20_30", "; Mass of Z' [GeV]; Events", 200, 0, 2000);
	h_MET_ZprimeMass_DeltaM_NegDet_30_40 = new TH1D("h_MET_ZprimeMass_DeltaM_NegDet_30_40", "; Mass of Z' [GeV]; Events", 200, 0, 2000);
	h_MET_ZprimeMass_DeltaM_NegDet_40_50 = new TH1D("h_MET_ZprimeMass_DeltaM_NegDet_40_50", "; Mass of Z' [GeV]; Events", 200, 0, 2000);

	//------------------------------------

	// Run the selection
	Process(reader);

	std::cout << "Events in EventCount: " << h_EventCount->GetEntries() << std::endl;

	std::cout << "Write to file..." << std::endl;

	OutputFile->cd();

	h_EventCount->Write();
	h_WeightCount->Write();
	
	h_ZprimeMass_Basic->Write();
	
	h_JetPairMass->Write();
	h_JetPairMass_fromW->Write();
	h_JetSelectedPt->Write();
	h_JetUnselectedPt->Write();
	h_LeptonandMETMass->Write();
	h_ZprimeMass->Write();

	h_JetDeltaPt_pT->Write();
	h_JetSigmaPt_pT->Write();
	h_JetDeltaPhi_pT->Write();
	h_JetDeltaEta_pT->Write();
	h_JetDeltaPt_0_20->Write();
	h_JetDeltaPt_20_30->Write();
	h_JetDeltaPt_30_40->Write();
	h_JetDeltaPt_40_50->Write();
	h_JetDeltaPt_50_60->Write();
	h_JetDeltaPt_60_80->Write();
	h_JetDeltaPt_80_100->Write();
	h_JetDeltaPhi_0_20->Write();
	h_JetDeltaPhi_20_30->Write();
	h_JetDeltaPhi_30_40->Write();
	h_JetDeltaPhi_40_50->Write();
	h_JetDeltaPhi_50_60->Write();
	h_JetDeltaPhi_60_80->Write();
	h_JetDeltaPhi_80_100->Write();

	h_LeptonfromLepton_pTLoss->Write();
	h_LeptonfromLepton_pTLossvsPartLevels->Write();

	h_TruthLeptonfromW_pT->Write();
	h_TruthLeptonnotfromW_pT->Write();
	h_TruthElectronfromW_pT->Write();
	h_TruthElectronnotfromW_pT->Write();
	h_TruthMuonfromW_pT->Write();
	h_TruthMuonnotfromW_pT->Write();
	h_TruthLeptonfromW_Stack_pT->Write();
	h_TruthLeptonnotfromW_Stack_pT->Write();
	c_TruthLeptonfromW_Stack_pT->Write();
	c_TruthLeptonnotfromW_Stack_pT->Write();
	c_TruthLeptonSource_pT->Write();

	h_ReconLeptonfromW_pT->Write();
	h_ReconLeptonnotfromW_pT->Write();
	h_ReconElectronfromW_pT->Write();
	h_ReconElectronnotfromW_pT->Write();
	h_ReconMuonfromW_pT->Write();
	h_ReconMuonnotfromW_pT->Write();
	h_ReconLeptonfromW_Stack_pT->Write();
	h_ReconLeptonnotfromW_Stack_pT->Write();
	c_ReconLeptonfromW_Stack_pT->Write();
	c_ReconLeptonnotfromW_Stack_pT->Write();
	c_ReconLeptonSource_pT->Write();

	h_ElectronSources->Write();
	h_MuonSources->Write();
	h_LeptonSources->Write();
	c_LeptonSources->Write();

	h_ElectronfromLeptonSources->Write();
	h_MuonfromLeptonSources->Write();
	h_LeptonfromLeptonSources->Write();
	c_LeptonfromLeptonSources->Write();

	h_ReconLeptonfromW_PassCut_pT->Write();
	h_ReconLeptonnotfromW_PassCut_pT->Write();
	h_LeptonfromW_PassCutPerc->Write();
	h_LeptonnotfromW_PassCutPerc->Write();
	h_LeptonfromW_PassCutRatio->Write();
	h_LeptonfromW_PassCutGoodMinusBad->Write();

	h_LeptonDeltaPt_pT->Write();
	h_LeptonSigmaPt_pT->Write();
	h_LeptonDeltaPhi_pT->Write();
	h_LeptonDeltaEta_pT->Write();
	h_LeptonDeltaPt_0_20->Write();
	h_LeptonDeltaPt_20_30->Write();
	h_LeptonDeltaPt_30_40->Write();
	h_LeptonDeltaPt_40_50->Write();
	h_LeptonDeltaPt_50_60->Write();
	h_LeptonDeltaPt_60_80->Write();
	h_LeptonDeltaPt_80_100->Write();
	h_LeptonDeltaPhi_0_20->Write();
	h_LeptonDeltaPhi_20_30->Write();
	h_LeptonDeltaPhi_30_40->Write();
	h_LeptonDeltaPhi_40_50->Write();
	h_LeptonDeltaPhi_50_60->Write();
	h_LeptonDeltaPhi_60_80->Write();
	h_LeptonDeltaPhi_80_100->Write();
	
	h_LeptonTransverseMass->Write();
	h_Zprime_withTransverseMass->Write();

	h_MissingETEta_PlusvsMinus->Write();
	h_MissingETEta_SmallvsLargeMag->Write();
	h_MissingETEta_Truth->Write();
	h_MissingETEta_Recon->Write();
	h_MissingETEta_Comp->Write();

	h_TruthMissingETEta_PlusvsMinus->Write();
	h_TruthMissingETEta_SmallvsLargeMag->Write();
	h_TruthMissingETEta_Recon->Write();
	h_TruthMissingETEta_Comp->Write();

	h_TruthfromWMissingETEta_PlusvsMinus->Write();
	h_TruthfromWMissingETEta_SmallvsLargeMag->Write();
	h_TruthfromWMissingETEta_Recon->Write();
	h_TruthfromWMissingETEta_Comp->Write();

	h_EtaGoodnessDist_Event1->Write();

	h_MinPointMissingETEta_Recon->Write();
	h_MinPointMissingETEta_Comp->Write();
	h_MinPointMissingETEta_PosDetComp->Write();
	h_MinPointMissingETEta_NegDetComp->Write();
	h_MinPointMissingETEta_PosDetGoodvsBad->Write();

	h_Zprime_withEtaCalc->Write();
	h_Zprime_withEtaPlus->Write();
	h_Zprime_withEtaMinus->Write();
	h_Zprime_withEtaGood->Write();
	h_Zprime_withEtaBad->Write();
	h_Zprime_withTransverseLeptons->Write();
	h_Zprime_withMinPointMissingETEta->Write();
	h_Zprime_withMinPointMissingETEta_PosDet->Write();
	h_Zprime_withMinPointMissingETEta_NegDet->Write();

	h_METDeltaPt_MET->Write();
	h_METSigmaPt_MET->Write();
	h_METDeltaPhi_MET->Write();
	h_METDeltaPt_MET_wlep->Write();
	h_METSigmaPt_MET_wlep->Write();
	h_METDeltaPhi_MET_wlep->Write();
	h_METDeltaEta_MET_wlep->Write();
	h_METDeltaEta_MET_wlep_calc->Write();
	h_METDeltaPt_0_20->Write();
	h_METDeltaPt_20_30->Write();
	h_METDeltaPt_30_40->Write();
	h_METDeltaPt_40_50->Write();
	h_METDeltaPt_50_60->Write();
	h_METDeltaPt_60_80->Write();
	h_METDeltaPt_80_100->Write();
	h_METDeltaPhi_0_20->Write();
	h_METDeltaPhi_20_30->Write();
	h_METDeltaPhi_30_40->Write();
	h_METDeltaPhi_40_50->Write();
	h_METDeltaPhi_50_60->Write();
	h_METDeltaPhi_60_80->Write();
	h_METDeltaPhi_80_100->Write();
	h_METDeltaEtaCalc_0_20->Write();
	h_METDeltaEtaCalc_20_30->Write();
	h_METDeltaEtaCalc_30_40->Write();
	h_METDeltaEtaCalc_40_50->Write();
	h_METDeltaEtaCalc_50_60->Write();
	h_METDeltaEtaCalc_60_80->Write();
	h_METDeltaEtaCalc_80_100->Write();

	h_METDeltapx->Write();
	h_METDeltapy->Write();
	h_METSigmapx->Write();
	h_METSigmapy->Write();

	h_MET_PosDet->Write();
	h_MET_NegDet->Write();
	h_MET_DeltaM_NegDet->Write();
	h_MET_METvsDeltaM_NegDet->Write();
	h_MET_DeltaM_NegDetvsZprimeMass->Write();
	h_MET_ZprimeMass_DeltaM_NegDet_0_10->Write();
	h_MET_ZprimeMass_DeltaM_NegDet_10_20->Write();
	h_MET_ZprimeMass_DeltaM_NegDet_20_30->Write();
	h_MET_ZprimeMass_DeltaM_NegDet_30_40->Write();
	h_MET_ZprimeMass_DeltaM_NegDet_40_50->Write();

	OutputFile->Close();

	std::cout << "Tidy..." << std::endl;

	delete reader;

	std::cout << "Done!" << std::endl;

	return 0;

}

ExRootTreeReader * InitReader(const TString FilePath) {

	std::cout << "InitReader" << std::endl;

	TFile * f = TFile::Open(FilePath);

	TChain * Chain = new TChain("Delphes","");

	Chain->Add(FilePath);

	// Create object of class ExRootTreeReader
	ExRootTreeReader * r = new ExRootTreeReader(Chain);

	return r;
}

void Process(ExRootTreeReader* treeReader) {

	// Get pointers to branches used in this analysis
	bEvent = treeReader->UseBranch("Event");
	bJet = treeReader->UseBranch("Jet");
	bGenJet = treeReader->UseBranch("GenJet");
	bElectron = treeReader->UseBranch("Electron");
	bMuon = treeReader->UseBranch("Muon");
	bTruthLeptons = treeReader->UseBranch("TruthLeptonParticles");
	bMissingET = treeReader->UseBranch("MissingET");
	bGenMissingET = treeReader->UseBranch("GenMissingET");
	bParticle = treeReader->UseBranch("Particle");
	bTruthWZ = treeReader->UseBranch("TruthWZParticles");

	Long64_t numberOfEntries = treeReader->GetEntries();
	Int_t UsedEvent = 0;
	Int_t graph_n = 1000;
	Double_t eta[1000], deltaM[1000], eta_graph[1000], deltaM_graph[1000];

	Int_t l_nu = 0;
	Int_t nu_l = 0;
	Int_t PosLarge = 0;
	Int_t PosSmall = 0;
	Double_t FromW_Total = 0.0;
	Double_t FromW_Pass = 0.0;
	Double_t NotFromW_Total = 0.0;
	Double_t NotFromW_Pass = 0.0;

	Double_t ElectronWRecon = 0.0;
	Double_t ElectronWTruth = 0.0;
	Double_t ElectronOtherRecon = 0.0;
	Double_t ElectronOtherTruth = 0.0;
	Double_t MuonWRecon = 0.0;
	Double_t MuonWTruth = 0.0;
	Double_t MuonOtherRecon = 0.0;
	Double_t MuonOtherTruth = 0.0;

	Int_t MET_NegDet_PassCut = 0;

	std::vector<Double_t> LeptonfromW_pT;
	std::vector<Double_t> LeptonnotfromW_pT;

	int nSelected = 0;

	std::cout << "-------------------------------------------------------------" << std::endl;
	std::cout << "Input: " << numberOfEntries << " events to process" << std::endl;

	// Loop over all events
	for (Int_t entry = 0; entry < numberOfEntries; ++entry) {

		// Load selected branches with data from specified event
		treeReader->ReadEntry(entry);

		HepMCEvent* event = (HepMCEvent*)bEvent->At(0);
		const float Event_Weight = event->Weight;

		h_EventCount->Fill(0.5);
		h_WeightCount->Fill(0.5, Event_Weight);

		if ((entry > 0 && entry % 1000 == 0) || Debug) {
			std::cout << "-------------------------------------------------------------" << std::endl;
			std::cout << "Processing Event Number =  " << entry << std::endl;
			std::cout << "-------------------------------------------------------------" << std::endl;
		}

		//------------------------------------------------------------------
		// Jet Loop
		//------------------------------------------------------------------

		TLorentzVector JetPair;

		if (bJet->GetEntriesFast() >= 2) {
			Jet* jet1 = (Jet*)bJet->At(0);
			Jet* jet2 = (Jet*)bJet->At(1);

			//std::cout << jet1->Flavor << " " << jet2->Flavor << std::endl;

			TLorentzVector Vec_Jet1;
			TLorentzVector Vec_Jet2;
			Vec_Jet1.SetPtEtaPhiM(jet1->PT, jet1->Eta, jet1->Phi, jet1->Mass);
			Vec_Jet2.SetPtEtaPhiM(jet2->PT, jet2->Eta, jet2->Phi, jet2->Mass);

			JetPair = Vec_Jet1 + Vec_Jet2;
			h_JetPairMass->Fill(JetPair.M(), Event_Weight);

			if (JetPair.M() > 0 && JetPair.M() < 200) {
				h_JetSelectedPt->Fill(Vec_Jet1.Pt(), Event_Weight);
				h_JetSelectedPt->Fill(Vec_Jet2.Pt(), Event_Weight);
			}
			else {
				h_JetUnselectedPt->Fill(Vec_Jet1.Pt(), Event_Weight);
				h_JetUnselectedPt->Fill(Vec_Jet2.Pt(), Event_Weight);
			}

			// Jets from Ws

			for (int i = 0; i < bTruthWZ->GetEntriesFast(); ++i) {
				GenParticle* v = (GenParticle*)bTruthWZ->At(i);

				if (abs(v->PID) == 24) {
					GenParticle* D1 = (GenParticle*)bParticle->At(v->D1);
					GenParticle* D2 = (GenParticle*)bParticle->At(v->D2);
					Double_t DeltaR_11, DeltaR_12, DeltaR_21, DeltaR_22;
					if (abs(D1->PID) == 1 || abs(D1->PID) == 2 || abs(D1->PID) == 3 || abs(D1->PID) == 4 || abs(D1->PID) == 5) {
						Double_t DeltaPhi_11 = D1->Phi - jet1->Phi;
						if (DeltaPhi_11 > TMath::Pi()) {
							DeltaPhi_11 -= 2.0 * TMath::Pi();
						}
						else if (DeltaPhi_11 <= -TMath::Pi()) {
							DeltaPhi_11 += 2.0 * TMath::Pi();
						}
						DeltaR_11 = TMath::Hypot(DeltaPhi_11, D1->Eta - jet1->Eta);

						Double_t DeltaPhi_12 = D1->Phi - jet2->Phi;
						if (DeltaPhi_12 > TMath::Pi()) {
							DeltaPhi_12 -= 2.0 * TMath::Pi();
						}
						else if (DeltaPhi_12 <= -TMath::Pi()) {
							DeltaPhi_12 += 2.0 * TMath::Pi();
						}
						DeltaR_12 = TMath::Hypot(DeltaPhi_12, D1->Eta - jet2->Eta);
					}

					if (abs(D2->PID) == 1 || abs(D2->PID) == 2 || abs(D2->PID) == 3 || abs(D2->PID) == 4 || abs(D2->PID) == 5) {
						Double_t DeltaPhi_21 = D2->Phi - jet1->Phi;
						if (DeltaPhi_21 > TMath::Pi()) {
							DeltaPhi_21 -= 2.0 * TMath::Pi();
						}
						else if (DeltaPhi_21 <= -TMath::Pi()) {
							DeltaPhi_21 += 2.0 * TMath::Pi();
						}
						DeltaR_21 = TMath::Hypot(DeltaPhi_21, D2->Eta - jet1->Eta);

						Double_t DeltaPhi_22 = D2->Phi - jet2->Phi;
						if (DeltaPhi_22 > TMath::Pi()) {
							DeltaPhi_22 -= 2.0 * TMath::Pi();
						}
						else if (DeltaPhi_22 <= -TMath::Pi()) {
							DeltaPhi_22 += 2.0 * TMath::Pi();
						}
						DeltaR_22 = TMath::Hypot(DeltaPhi_22, D2->Eta - jet2->Eta);
					}

					if (DeltaR_11 < 0.4 && DeltaR_22 < 0.4 && abs(D1->PID) == jet1->Flavor && abs(D2->PID) == jet2->Flavor) {
						h_JetPairMass_fromW->Fill(JetPair.M(), Event_Weight);
						GenParticle* genjet1 = (GenParticle*)bParticle->At(v->D1);
						GenParticle* genjet2 = (GenParticle*)bParticle->At(v->D2);

						Double_t DeltaPhi_1 = genjet1->Phi - jet1->Phi;
						if (DeltaPhi_1 > TMath::Pi()) {
							DeltaPhi_1 -= 2.0 * TMath::Pi();
						}
						else if (DeltaPhi_1 <= -TMath::Pi()) {
							DeltaPhi_1 += 2.0 * TMath::Pi();
						}

						Double_t DeltaPhi_2 = genjet2->Phi - jet2->Phi;
						if (DeltaPhi_2 > TMath::Pi()) {
							DeltaPhi_2 -= 2.0 * TMath::Pi();
						}
						else if (DeltaPhi_2 <= -TMath::Pi()) {
							DeltaPhi_2 += 2.0 * TMath::Pi();
						}

						h_JetDeltaPt_pT->Fill(jet1->PT, jet1->PT - genjet1->PT, Event_Weight);
						h_JetSigmaPt_pT->Fill(jet1->PT, (jet1->PT - genjet1->PT) / genjet1->PT, Event_Weight);
						h_JetDeltaPhi_pT->Fill(jet1->PT, DeltaPhi_1, Event_Weight);
						h_JetDeltaEta_pT->Fill(jet1->PT, jet1->Eta - genjet1->Eta, Event_Weight);

						h_JetDeltaPt_pT->Fill(jet2->PT, jet2->PT - genjet2->PT, Event_Weight);
						h_JetSigmaPt_pT->Fill(jet2->PT, (jet2->PT - genjet2->PT) / genjet2->PT, Event_Weight);
						h_JetDeltaPhi_pT->Fill(jet2->PT, DeltaPhi_2, Event_Weight);
						h_JetDeltaEta_pT->Fill(jet2->PT, jet2->Eta - genjet2->Eta, Event_Weight);

						if (jet1->PT < 20) {
							h_JetDeltaPhi_0_20->Fill(DeltaPhi_1, Event_Weight);
							h_JetDeltaPt_0_20->Fill(jet1->PT - genjet1->PT, Event_Weight);
						}
						else if (jet1->PT < 30) {
							h_JetDeltaPhi_20_30->Fill(DeltaPhi_1, Event_Weight);
							h_JetDeltaPt_20_30->Fill(jet1->PT - genjet1->PT, Event_Weight);
						}
						else if (jet1->PT < 40) {
							h_JetDeltaPhi_30_40->Fill(DeltaPhi_1, Event_Weight);
							h_JetDeltaPt_30_40->Fill(jet1->PT - genjet1->PT, Event_Weight);
						}
						else if (jet1->PT < 50) {
							h_JetDeltaPhi_40_50->Fill(DeltaPhi_1, Event_Weight);
							h_JetDeltaPt_40_50->Fill(jet1->PT - genjet1->PT, Event_Weight);
						}
						else if (jet1->PT < 60) {
							h_JetDeltaPhi_50_60->Fill(DeltaPhi_1, Event_Weight);
							h_JetDeltaPt_50_60->Fill(jet1->PT - genjet1->PT, Event_Weight);
						}
						else if (jet1->PT < 80) {
							h_JetDeltaPhi_60_80->Fill(DeltaPhi_1, Event_Weight);
							h_JetDeltaPt_60_80->Fill(jet1->PT - genjet1->PT, Event_Weight);
						}
						else if (jet1->PT < 100) {
							h_JetDeltaPhi_80_100->Fill(DeltaPhi_1, Event_Weight);
							h_JetDeltaPt_80_100->Fill(jet1->PT - genjet1->PT, Event_Weight);
						}

						if (jet2->PT < 20) {
							h_JetDeltaPhi_0_20->Fill(DeltaPhi_2, Event_Weight);
							h_JetDeltaPt_0_20->Fill(jet2->PT - genjet2->PT, Event_Weight);
						}
						else if (jet2->PT < 30) {
							h_JetDeltaPhi_20_30->Fill(DeltaPhi_2, Event_Weight);
							h_JetDeltaPt_20_30->Fill(jet2->PT - genjet2->PT, Event_Weight);
						}
						else if (jet2->PT < 40) {
							h_JetDeltaPhi_30_40->Fill(DeltaPhi_2, Event_Weight);
							h_JetDeltaPt_30_40->Fill(jet2->PT - genjet2->PT, Event_Weight);
						}
						else if (jet2->PT < 50) {
							h_JetDeltaPhi_40_50->Fill(DeltaPhi_2, Event_Weight);
							h_JetDeltaPt_40_50->Fill(jet2->PT - genjet2->PT, Event_Weight);
						}
						else if (jet2->PT < 60) {
							h_JetDeltaPhi_50_60->Fill(DeltaPhi_2, Event_Weight);
							h_JetDeltaPt_50_60->Fill(jet2->PT - genjet2->PT, Event_Weight);
						}
						else if (jet2->PT < 80) {
							h_JetDeltaPhi_60_80->Fill(DeltaPhi_2, Event_Weight);
							h_JetDeltaPt_60_80->Fill(jet2->PT - genjet2->PT, Event_Weight);
						}
						else if (jet2->PT < 100) {
							h_JetDeltaPhi_80_100->Fill(DeltaPhi_2, Event_Weight);
							h_JetDeltaPt_80_100->Fill(jet2->PT - genjet2->PT, Event_Weight);
						}
					}
					if (DeltaR_12 < 0.4 && DeltaR_21 < 0.4 && abs(D1->PID) == jet2->Flavor && abs(D2->PID) == jet1->Flavor) {
						h_JetPairMass_fromW->Fill(JetPair.M(), Event_Weight);
						GenParticle* genjet2 = (GenParticle*)bParticle->At(v->D1);
						GenParticle* genjet1 = (GenParticle*)bParticle->At(v->D2);

						Double_t DeltaPhi_1 = genjet1->Phi - jet1->Phi;
						if (DeltaPhi_1 > TMath::Pi()) {
							DeltaPhi_1 -= 2.0 * TMath::Pi();
						}
						else if (DeltaPhi_1 <= -TMath::Pi()) {
							DeltaPhi_1 += 2.0 * TMath::Pi();
						}

						Double_t DeltaPhi_2 = genjet2->Phi - jet2->Phi;
						if (DeltaPhi_2 > TMath::Pi()) {
							DeltaPhi_2 -= 2.0 * TMath::Pi();
						}
						else if (DeltaPhi_2 <= -TMath::Pi()) {
							DeltaPhi_2 += 2.0 * TMath::Pi();
						}

						h_JetDeltaPt_pT->Fill(jet1->PT, jet1->PT - genjet1->PT, Event_Weight);
						h_JetSigmaPt_pT->Fill(jet1->PT, (jet1->PT - genjet1->PT) / genjet1->PT, Event_Weight);
						h_JetDeltaPhi_pT->Fill(jet1->PT, DeltaPhi_1, Event_Weight);
						h_JetDeltaEta_pT->Fill(jet1->PT, jet1->Eta - genjet1->Eta, Event_Weight);

						h_JetDeltaPt_pT->Fill(jet2->PT, jet2->PT - genjet2->PT, Event_Weight);
						h_JetSigmaPt_pT->Fill(jet2->PT, (jet2->PT - genjet2->PT) / genjet2->PT, Event_Weight);
						h_JetDeltaPhi_pT->Fill(jet2->PT, DeltaPhi_2, Event_Weight);
						h_JetDeltaEta_pT->Fill(jet2->PT, jet2->Eta - genjet2->Eta, Event_Weight);

						if (jet1->PT < 20) {
							h_JetDeltaPhi_0_20->Fill(DeltaPhi_1, Event_Weight);
							h_JetDeltaPt_0_20->Fill(jet1->PT - genjet1->PT, Event_Weight);
						}
						else if (jet1->PT < 30) {
							h_JetDeltaPhi_20_30->Fill(DeltaPhi_1, Event_Weight);
							h_JetDeltaPt_20_30->Fill(jet1->PT - genjet1->PT, Event_Weight);
						}
						else if (jet1->PT < 40) {
							h_JetDeltaPhi_30_40->Fill(DeltaPhi_1, Event_Weight);
							h_JetDeltaPt_30_40->Fill(jet1->PT - genjet1->PT, Event_Weight);
						}
						else if (jet1->PT < 50) {
							h_JetDeltaPhi_40_50->Fill(DeltaPhi_1, Event_Weight);
							h_JetDeltaPt_40_50->Fill(jet1->PT - genjet1->PT, Event_Weight);
						}
						else if (jet1->PT < 60) {
							h_JetDeltaPhi_50_60->Fill(DeltaPhi_1, Event_Weight);
							h_JetDeltaPt_50_60->Fill(jet1->PT - genjet1->PT, Event_Weight);
						}
						else if (jet1->PT < 80) {
							h_JetDeltaPhi_60_80->Fill(DeltaPhi_1, Event_Weight);
							h_JetDeltaPt_60_80->Fill(jet1->PT - genjet1->PT, Event_Weight);
						}
						else if (jet1->PT < 100) {
							h_JetDeltaPhi_80_100->Fill(DeltaPhi_1, Event_Weight);
							h_JetDeltaPt_80_100->Fill(jet1->PT - genjet1->PT, Event_Weight);
						}

						if (jet2->PT < 20) {
							h_JetDeltaPhi_0_20->Fill(DeltaPhi_2, Event_Weight);
							h_JetDeltaPt_0_20->Fill(jet2->PT - genjet2->PT, Event_Weight);
						}
						else if (jet2->PT < 30) {
							h_JetDeltaPhi_20_30->Fill(DeltaPhi_2, Event_Weight);
							h_JetDeltaPt_20_30->Fill(jet2->PT - genjet2->PT, Event_Weight);
						}
						else if (jet2->PT < 40) {
							h_JetDeltaPhi_30_40->Fill(DeltaPhi_2, Event_Weight);
							h_JetDeltaPt_30_40->Fill(jet2->PT - genjet2->PT, Event_Weight);
						}
						else if (jet2->PT < 50) {
							h_JetDeltaPhi_40_50->Fill(DeltaPhi_2, Event_Weight);
							h_JetDeltaPt_40_50->Fill(jet2->PT - genjet2->PT, Event_Weight);
						}
						else if (jet2->PT < 60) {
							h_JetDeltaPhi_50_60->Fill(DeltaPhi_2, Event_Weight);
							h_JetDeltaPt_50_60->Fill(jet2->PT - genjet2->PT, Event_Weight);
						}
						else if (jet2->PT < 80) {
							h_JetDeltaPhi_60_80->Fill(DeltaPhi_2, Event_Weight);
							h_JetDeltaPt_60_80->Fill(jet2->PT - genjet2->PT, Event_Weight);
						}
						else if (jet2->PT < 100) {
							h_JetDeltaPhi_80_100->Fill(DeltaPhi_2, Event_Weight);
							h_JetDeltaPt_80_100->Fill(jet2->PT - genjet2->PT, Event_Weight);
						}
					}
				}
			}

		} // Jet Loop


		//------------------------------------------------------------------
		// Lepton and MET Loop
		//------------------------------------------------------------------

		TLorentzVector LeptonandMET;
		Double_t LeptonTransverse_mass = 0;
		Double_t eta_plus;
		Double_t eta_minus;
		Double_t eta_good;
		Double_t eta_bad;
		Double_t Truth_eta_plus;
		Double_t Truth_eta_minus;
		Int_t EtaCalc = 0;
		TLorentzVector LeptonandMET_EtaPlus;
		TLorentzVector LeptonandMET_EtaMinus;
		TLorentzVector LeptonandMET_EtaGood;
		TLorentzVector LeptonandMET_EtaBad;
		TLorentzVector LeptonandMET_Transverse;
		TLorentzVector LeptonandMET_MinPoint;
		Int_t i_min = 0;

		// Lepton from/not from W Investigation - Truth

		for (int i = 0; i < bTruthLeptons->GetEntriesFast(); ++i) {

			GenParticle* lep = (GenParticle*)bTruthLeptons->At(i);

			if (abs(lep->PID) == 11 || abs(lep->PID) == 13) {
				GenParticle* M1 = (GenParticle*)bParticle->At(lep->M1);
				if (abs(M1->PID) == 24) {
					h_TruthLeptonfromW_pT->Fill(lep->PT, Event_Weight);
					if (abs(lep->PID) == 11) {
						h_TruthElectronfromW_pT->Fill(lep->PT, Event_Weight);
						ElectronWTruth += 1.0;
					}
					else if (abs(lep->PID) == 13) {
						h_TruthMuonfromW_pT->Fill(lep->PT, Event_Weight);
						MuonWTruth += 1.0;
					}
				}
				else if (abs(M1->PID) == 11 || abs(M1->PID) == 13 || abs(M1->PID) == 15) {
					Long64_t Part_Level = 1;
					GenParticle* First_NonLepton_M1 = (GenParticle*)bParticle->At(M1->M1);
					GenParticle* First_Lepton_D1 = (GenParticle*)bParticle->At(lep->M1);
					while (abs(First_NonLepton_M1->PID) == 11 || abs(First_NonLepton_M1->PID) == 13 || abs(First_NonLepton_M1->PID) == 15) {
						Part_Level++;
						First_Lepton_D1 = (GenParticle*)bParticle->At(First_Lepton_D1->M1);
						First_NonLepton_M1 = (GenParticle*)bParticle->At(First_NonLepton_M1->M1);
					}
					if (abs(First_NonLepton_M1->PID) == 24) {
						h_LeptonfromLepton_pTLoss->Fill(lep->PT - First_Lepton_D1->PT, Event_Weight);
						h_LeptonfromLepton_pTLossvsPartLevels->Fill(Part_Level, lep->PT - First_Lepton_D1->PT, Event_Weight);
						h_TruthLeptonfromW_pT->Fill(lep->PT, Event_Weight);
						if (abs(lep->PID) == 11) {
							h_TruthElectronfromW_pT->Fill(lep->PT, Event_Weight);
							ElectronWTruth += 1.0;
						}
						else if (abs(lep->PID) == 13) {
							h_TruthMuonfromW_pT->Fill(lep->PT, Event_Weight);
							MuonWTruth += 1.0;
						}
					}
					else {
						h_TruthLeptonnotfromW_pT->Fill(lep->PT, Event_Weight);
						//std::cout << First_NonLepton_M1->PID << std::endl;
						if (abs(lep->PID) == 11) {
							h_TruthElectronnotfromW_pT->Fill(lep->PT, Event_Weight);
							ElectronOtherTruth += 1.0;
						}
						else if (abs(lep->PID) == 13) {
							h_TruthMuonnotfromW_pT->Fill(lep->PT, Event_Weight);
							MuonOtherTruth += 1.0;
						}
					}
					if (abs(First_NonLepton_M1->PID) == 24) {
						if (abs(lep->PID) == 11) {
							h_ElectronfromLeptonSources->Fill(0.5, Event_Weight);
						}
						if (abs(lep->PID) == 13) {
							h_MuonfromLeptonSources->Fill(0.5, Event_Weight);
						}
					}
					else if (abs(First_NonLepton_M1->PID) == 111 || abs(First_NonLepton_M1->PID) == 113 || abs(First_NonLepton_M1->PID) == 221 || abs(First_NonLepton_M1->PID) == 223 || abs(First_NonLepton_M1->PID) == 310 || abs(First_NonLepton_M1->PID) == 331 || abs(First_NonLepton_M1->PID) == 333 || abs(First_NonLepton_M1->PID) == 411 || abs(First_NonLepton_M1->PID) == 421 || abs(First_NonLepton_M1->PID) == 431 || abs(First_NonLepton_M1->PID) == 443 || abs(First_NonLepton_M1->PID) == 511 || abs(First_NonLepton_M1->PID) == 521 || abs(First_NonLepton_M1->PID) == 531) {
						if (abs(lep->PID) == 11) {
							h_ElectronfromLeptonSources->Fill(1.5, Event_Weight);
						}
						if (abs(lep->PID) == 13) {
							h_MuonfromLeptonSources->Fill(1.5, Event_Weight);
						}
					}
					else if (abs(First_NonLepton_M1->PID) == 3112 || abs(First_NonLepton_M1->PID) == 3122 || abs(First_NonLepton_M1->PID) == 3212 || abs(First_NonLepton_M1->PID) == 4122 || abs(First_NonLepton_M1->PID) == 4132 || abs(First_NonLepton_M1->PID) == 4232 || abs(First_NonLepton_M1->PID) == 5122 || abs(First_NonLepton_M1->PID) == 5132 || abs(First_NonLepton_M1->PID) == 5232) {
						if (abs(lep->PID) == 11) {
							h_ElectronfromLeptonSources->Fill(2.5, Event_Weight);
						}
						if (abs(lep->PID) == 13) {
							h_MuonfromLeptonSources->Fill(2.5, Event_Weight);
						}
					}
					else if (abs(First_NonLepton_M1->PID) == 22) {
						if (abs(lep->PID) == 11) {
							h_ElectronfromLeptonSources->Fill(3.5, Event_Weight);
						}
						if (abs(lep->PID) == 13) {
							h_MuonfromLeptonSources->Fill(3.5, Event_Weight);
						}
					}
					else {
						if (abs(lep->PID) == 11) {
							h_ElectronfromLeptonSources->Fill(4.5, Event_Weight);
						}
						if (abs(lep->PID) == 13) {
							h_MuonfromLeptonSources->Fill(4.5, Event_Weight);
						}
					}
				}
				else {
					h_TruthLeptonnotfromW_pT->Fill(lep->PT, Event_Weight);
					if (abs(lep->PID) == 11) {
						h_TruthElectronnotfromW_pT->Fill(lep->PT, Event_Weight);
						ElectronOtherTruth += 1.0;
					}
					else if (abs(lep->PID) == 13) {
						h_TruthMuonnotfromW_pT->Fill(lep->PT, Event_Weight);
						MuonOtherTruth += 1.0;
					}
				}
			}
		}
		
		Double_t MET_EtaCalc;

		if (bElectron->GetEntriesFast() >= 1 || bMuon->GetEntriesFast() >= 1) {

			// Lepton
			TLorentzVector Vec_Lepton;

			if (bMuon->GetEntriesFast() == 0) {
				Electron* lepton = (Electron*)bElectron->At(0);
				Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, e_mass);
			}
			else if (bElectron->GetEntriesFast() == 0) {
				Muon* lepton = (Muon*)bMuon->At(0);
				Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, muon_mass);
			}
			else {
				Electron* electron = (Electron*)bElectron->At(0);
				Muon* muon = (Muon*)bMuon->At(0);
				if (electron->PT > muon->PT) {
					Electron* lepton = electron;
					Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, e_mass);
				}
				else {
					Muon* lepton = muon;
					Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, muon_mass);
				}
			}

			//------------------------------------------------------------------
			// Lepton Reconstruction Quality
			//------------------------------------------------------------------

			// Lepton from/not from W Investigation - Reconstructed

			for (int i = 0; i < bTruthLeptons->GetEntriesFast(); ++i) {

				GenParticle* lep = (GenParticle*)bTruthLeptons->At(i);
				bool FromW = false;
				bool FromMeson = false;
				bool FromBaryon = false;
				bool FromPhoton = false;
				bool FromOther = false;

				if (abs(lep->PID) == 11 || abs(lep->PID) == 13) {
					GenParticle* M1 = (GenParticle*)bParticle->At(lep->M1);
					if (abs(M1->PID) == 24) {
						FromW = true;
					}
					else if (abs(M1->PID) == 11 || abs(M1->PID) == 13 || abs(M1->PID) == 15) {
						Long64_t Part_Level = 1;
						GenParticle* First_NonLepton_M1 = (GenParticle*)bParticle->At(M1->M1);
						GenParticle* First_Lepton_D1 = (GenParticle*)bParticle->At(lep->M1);
						while (abs(First_NonLepton_M1->PID) == 11 || abs(First_NonLepton_M1->PID) == 13 || abs(First_NonLepton_M1->PID) == 15) {
							Part_Level++;
							First_Lepton_D1 = (GenParticle*)bParticle->At(First_Lepton_D1->M1);
							First_NonLepton_M1 = (GenParticle*)bParticle->At(First_NonLepton_M1->M1);
						}
						if (abs(First_NonLepton_M1->PID) == 24) {
							FromW = true;
						}
						else if (abs(First_NonLepton_M1->PID) == 111 || abs(First_NonLepton_M1->PID) == 113 || abs(First_NonLepton_M1->PID) == 221 || abs(First_NonLepton_M1->PID) == 223 || abs(First_NonLepton_M1->PID) == 310 || abs(First_NonLepton_M1->PID) == 331 || abs(First_NonLepton_M1->PID) == 333 || abs(First_NonLepton_M1->PID) == 411 || abs(First_NonLepton_M1->PID) == 421 || abs(First_NonLepton_M1->PID) == 431 || abs(First_NonLepton_M1->PID) == 443 || abs(First_NonLepton_M1->PID) == 511 || abs(First_NonLepton_M1->PID) == 521 || abs(First_NonLepton_M1->PID) == 531) {
							FromMeson = true;
						}
						else if (abs(First_NonLepton_M1->PID) == 3112 || abs(First_NonLepton_M1->PID) == 3122 || abs(First_NonLepton_M1->PID) == 3212 || abs(First_NonLepton_M1->PID) == 4122 || abs(First_NonLepton_M1->PID) == 4132 || abs(First_NonLepton_M1->PID) == 4232 || abs(First_NonLepton_M1->PID) == 5122 || abs(First_NonLepton_M1->PID) == 5132 || abs(First_NonLepton_M1->PID) == 5232) {
							FromBaryon = true;
						}
						else if (abs(First_NonLepton_M1->PID) == 22) {
							FromPhoton = true;
						}
						else {
							FromOther = true;
						}
					}
					else if (abs(M1->PID) == 111 || abs(M1->PID) == 113 || abs(M1->PID) == 221 || abs(M1->PID) == 223 || abs(M1->PID) == 310 || abs(M1->PID) == 331 || abs(M1->PID) == 333 || abs(M1->PID) == 411 || abs(M1->PID) == 421 || abs(M1->PID) == 431 || abs(M1->PID) == 443 || abs(M1->PID) == 511 || abs(M1->PID) == 521 || abs(M1->PID) == 531) {
						FromMeson = true;
					}
					else if (abs(M1->PID) == 3112 || abs(M1->PID) == 3122 || abs(M1->PID) == 3212 || abs(M1->PID) == 4122 || abs(M1->PID) == 4132 || abs(M1->PID) == 4232 || abs(M1->PID) == 5122 || abs(M1->PID) == 5132 || abs(M1->PID) == 5232) {
						FromBaryon = true;
					}
					else if (abs(M1->PID) == 22) {
						FromPhoton = true;
					}
					else {
						FromOther = true;
					}
				}

				Double_t DeltaPhi = lep->Phi - Vec_Lepton.Phi();
				if (DeltaPhi > TMath::Pi()) {
					DeltaPhi -= 2.0 * TMath::Pi();
				}
				else if (DeltaPhi <= -TMath::Pi()) {
					DeltaPhi += 2.0 * TMath::Pi();
				}
				Double_t DeltaR = TMath::Hypot(DeltaPhi, lep->Eta - Vec_Lepton.Eta());

				if (DeltaR < 0.3 && (abs(lep->PID) == 11 || abs(lep->PID) == 13)) {
					if (FromW) {
						h_ReconLeptonfromW_pT->Fill(lep->PT, Event_Weight);
						if (abs(lep->PID) == 11) {
							h_ReconElectronfromW_pT->Fill(lep->PT, Event_Weight);
							h_ElectronSources->Fill(0.5, Event_Weight);
							ElectronWRecon += 1.0;
						}
						else if (abs(lep->PID) == 13) {
							h_ReconMuonfromW_pT->Fill(lep->PT, Event_Weight);
							h_MuonSources->Fill(0.5, Event_Weight);
							MuonWRecon += 1.0;
						}
						FromW_Total++;
						if (lep->PT >= Lepton_pTCut) {
							h_ReconLeptonfromW_PassCut_pT->Fill(lep->PT, Event_Weight);
							FromW_Pass++;
						}
						LeptonfromW_pT.push_back(lep->PT);
					}
					else {
						h_ReconLeptonnotfromW_pT->Fill(lep->PT, Event_Weight);
						if (abs(lep->PID) == 11) {
							h_ReconElectronnotfromW_pT->Fill(lep->PT, Event_Weight);
							ElectronOtherRecon += 1.0;
						}
						else if (abs(lep->PID) == 13) {
							h_ReconMuonnotfromW_pT->Fill(lep->PT, Event_Weight);
							MuonOtherRecon += 1.0;
						}
						NotFromW_Total++;
						if (lep->PT >= Lepton_pTCut) {
							h_ReconLeptonnotfromW_PassCut_pT->Fill(lep->PT, Event_Weight);
							NotFromW_Pass++;
						}
						LeptonnotfromW_pT.push_back(lep->PT);
					}
					if (FromMeson) {
						if (abs(lep->PID) == 11) {
							h_ElectronSources->Fill(1.5, Event_Weight);
						}
						else if (abs(lep->PID) == 13) {
							h_MuonSources->Fill(1.5, Event_Weight);
						}
					}
					if (FromBaryon) {
						if (abs(lep->PID) == 11) {
							h_ElectronSources->Fill(2.5, Event_Weight);
						}
						else if (abs(lep->PID) == 13) {
							h_MuonSources->Fill(2.5, Event_Weight);
						}
					}
					if (FromPhoton) {
						if (abs(lep->PID) == 11) {
							h_ElectronSources->Fill(3.5, Event_Weight);
						}
						else if (abs(lep->PID) == 13) {
							h_MuonSources->Fill(3.5, Event_Weight);
						}
					}
					if (FromOther) {
						if (abs(lep->PID) == 11) {
							h_ElectronSources->Fill(4.5, Event_Weight);
						}
						else if (abs(lep->PID) == 13) {
							h_MuonSources->Fill(4.5, Event_Weight);
						}
					}

					// Reconstruction Quality

					h_LeptonDeltaPt_pT->Fill(Vec_Lepton.Pt(), Vec_Lepton.Pt() - lep->PT, Event_Weight);
					h_LeptonSigmaPt_pT->Fill(Vec_Lepton.Pt(), (Vec_Lepton.Pt() - lep->PT) / lep->PT, Event_Weight);
					h_LeptonDeltaPhi_pT->Fill(Vec_Lepton.Pt(), DeltaPhi, Event_Weight);
					h_LeptonDeltaEta_pT->Fill(Vec_Lepton.Pt(), Vec_Lepton.Eta() - lep->Eta, Event_Weight);

					if (Vec_Lepton.Pt() < 20) {
						h_LeptonDeltaPhi_0_20->Fill(DeltaPhi, Event_Weight);
						h_LeptonDeltaPt_0_20->Fill(Vec_Lepton.Pt() - lep->PT, Event_Weight);
					}
					else if (Vec_Lepton.Pt() < 30) {
						h_LeptonDeltaPhi_20_30->Fill(DeltaPhi, Event_Weight);
						h_LeptonDeltaPt_20_30->Fill(Vec_Lepton.Pt() - lep->PT, Event_Weight);
					}
					else if (Vec_Lepton.Pt() < 40) {
						h_LeptonDeltaPhi_30_40->Fill(DeltaPhi, Event_Weight);
						h_LeptonDeltaPt_30_40->Fill(Vec_Lepton.Pt() - lep->PT, Event_Weight);
					}
					else if (Vec_Lepton.Pt() < 50) {
						h_LeptonDeltaPhi_40_50->Fill(DeltaPhi, Event_Weight);
						h_LeptonDeltaPt_40_50->Fill(Vec_Lepton.Pt() - lep->PT, Event_Weight);
					}
					else if (Vec_Lepton.Pt() < 60) {
						h_LeptonDeltaPhi_50_60->Fill(DeltaPhi, Event_Weight);
						h_LeptonDeltaPt_50_60->Fill(Vec_Lepton.Pt() - lep->PT, Event_Weight);
					}
					else if (Vec_Lepton.Pt() < 80) {
						h_LeptonDeltaPhi_60_80->Fill(DeltaPhi, Event_Weight);
						h_LeptonDeltaPt_60_80->Fill(Vec_Lepton.Pt() - lep->PT, Event_Weight);
					}
					else if (Vec_Lepton.Pt() < 100) {
						h_LeptonDeltaPhi_80_100->Fill(DeltaPhi, Event_Weight);
						h_LeptonDeltaPt_80_100->Fill(Vec_Lepton.Pt() - lep->PT, Event_Weight);
					}
				}
			}

			// MissingET 

			MissingET* missingET = (MissingET*)bMissingET->At(0);

			TLorentzVector Vec_MissingET;
			Vec_MissingET.SetPtEtaPhiM(missingET->MET, missingET->Eta, missingET->Phi, 0);

			LeptonandMET = Vec_Lepton + Vec_MissingET;
			h_LeptonandMETMass->Fill(LeptonandMET.M(), Event_Weight);

			// MissingET Eta Calculation

			MissingET* genmissingET = (MissingET*)bGenMissingET->At(0);
			h_MissingETEta_Truth->Fill(genmissingET->Eta, Event_Weight);

			Double_t a = Vec_Lepton.E() - Vec_Lepton.Pz();
			Double_t b = TMath::Sq(W_mass) + TMath::Sq(Vec_Lepton.Px() + (missingET->MET * TMath::Cos(missingET->Phi))) + TMath::Sq(Vec_Lepton.Py() + (missingET->MET * TMath::Sin(missingET->Phi)));
			b += TMath::Sq(Vec_Lepton.Pz()) - TMath::Sq(Vec_Lepton.E()) - TMath::Sq(missingET->MET);
			b /= (-1 * missingET->MET);
			Double_t c = Vec_Lepton.E() + Vec_Lepton.Pz();
			Double_t det = TMath::Sq(b) - (4 * a * c);

			TLorentzVector Vec_MissingET_EtaPlus;
			TLorentzVector Vec_MissingET_EtaMinus;
			TLorentzVector Vec_MissingET_EtaGood;
			TLorentzVector Vec_MissingET_EtaBad;
			if (det > 0) {
				EtaCalc = 1;

				eta_plus = TMath::Log((-b + TMath::Sqrt(det)) / (2 * a));
				eta_minus = TMath::Log((-b - TMath::Sqrt(det)) / (2 * a));

				Vec_MissingET_EtaPlus.SetPtEtaPhiM(missingET->MET, eta_plus, missingET->Phi, 0);
				Vec_MissingET_EtaMinus.SetPtEtaPhiM(missingET->MET, eta_minus, missingET->Phi, 0);

				LeptonandMET_EtaPlus = Vec_Lepton + Vec_MissingET_EtaPlus;
				LeptonandMET_EtaMinus = Vec_Lepton + Vec_MissingET_EtaMinus;

				if (abs(eta_plus - genmissingET->Eta) < abs(eta_minus - genmissingET->Eta)) {
					eta_good = eta_plus;
					eta_bad = eta_minus;
					h_MissingETEta_Recon->Fill(eta_plus, Event_Weight);
					h_MissingETEta_PlusvsMinus->Fill(1.0, Event_Weight);
					h_MissingETEta_Comp->Fill(eta_plus - genmissingET->Eta);
					Vec_MissingET_EtaGood.SetPtEtaPhiM(missingET->MET, eta_plus, missingET->Phi, 0);
					Vec_MissingET_EtaBad.SetPtEtaPhiM(missingET->MET, eta_minus, missingET->Phi, 0);
					if (abs(eta_plus) < abs(eta_minus)) {
						h_MissingETEta_SmallvsLargeMag->Fill(-1.0, Event_Weight);
					}
					else {
						h_MissingETEta_SmallvsLargeMag->Fill(1.0, Event_Weight);
					}
				}
				else {
					eta_good = eta_minus;
					eta_bad = eta_plus;
					h_MissingETEta_Recon->Fill(eta_minus, Event_Weight);
					h_MissingETEta_PlusvsMinus->Fill(-1.0, Event_Weight);
					h_MissingETEta_Comp->Fill(eta_minus - genmissingET->Eta);
					Vec_MissingET_EtaGood.SetPtEtaPhiM(missingET->MET, eta_minus, missingET->Phi, 0);
					Vec_MissingET_EtaBad.SetPtEtaPhiM(missingET->MET, eta_plus, missingET->Phi, 0);
					if (abs(eta_plus) < abs(eta_minus)) {
						h_MissingETEta_SmallvsLargeMag->Fill(1.0, Event_Weight);
					}
					else {
						h_MissingETEta_SmallvsLargeMag->Fill(-1.0, Event_Weight);
					}
				}
				LeptonandMET_EtaGood = Vec_Lepton + Vec_MissingET_EtaGood;
				LeptonandMET_EtaBad = Vec_Lepton + Vec_MissingET_EtaBad;

				h_MET_PosDet->Fill(genmissingET->MET, Event_Weight);
				if (abs(eta_plus) < abs(eta_minus)) {
					MET_EtaCalc = Vec_MissingET_EtaPlus.Eta();
				}
				else {
					MET_EtaCalc = Vec_MissingET_EtaMinus.Eta();
				}
				h_METDeltaEta_MET_wlep_calc->Fill(missingET->MET, MET_EtaCalc - genmissingET->Eta, Event_Weight);
			}
			else {
				h_MissingETEta_PlusvsMinus->Fill(0.0, Event_Weight);
				h_MissingETEta_SmallvsLargeMag->Fill(0.0, Event_Weight);
				h_MET_NegDet->Fill(genmissingET->MET, Event_Weight);
			}

			if (entry < ToPrint) {
				std::cout << "-------------------------------------------------------------" << std::endl;
				std::cout << "Event " << entry << std::endl;
				std::cout << "-------------------------------------------------------------" << std::endl;
				std::cout << "lepton = [" << Vec_Lepton.Pt() << ", " << Vec_Lepton.Eta() << ", " << Vec_Lepton.Phi() << ", " << Vec_Lepton.E() << "]" << std::endl;
				std::cout << "missingET = [" << missingET->MET << ", " << missingET->Eta << ", " << missingET->Phi << "]" << std::endl;
				std::cout << "genmissingET = [" << genmissingET->MET << ", " << genmissingET->Eta << ", " << genmissingET->Phi << "]" << std::endl;
			}

			// MissingET Eta Goodness Distribution

			for (Int_t i = 0; i < graph_n; i++) {
				eta[i] = -5.0 + (i * (10.0 / graph_n));
				Double_t W_px = Vec_Lepton.Px() + Vec_MissingET.Px();
				Double_t W_py = Vec_Lepton.Py() + Vec_MissingET.Py();
				Double_t W_pz = Vec_Lepton.Pz() + Vec_MissingET.Pt() * TMath::SinH(eta[i]);
				Double_t W_E = Vec_Lepton.E() + Vec_MissingET.Pt() * TMath::CosH(eta[i]);
				Double_t W_m = TMath::Sqrt(TMath::Sq(W_E) - TMath::Sq(W_px) - TMath::Sq(W_py) - TMath::Sq(W_pz));
				deltaM[i] = abs(W_m - W_mass);
				if (deltaM[i] < deltaM[i_min]) {
					i_min = i;
				}
				if (UsedEvent == 0) {
					eta_graph[i] = eta[i];
					deltaM_graph[i] = deltaM[i];
				}
			}

			if (UsedEvent == 0) {
				UsedEvent += 1;
				std::cout << "Processing Event Number =  " << entry << std::endl;
				std::cout << "UsedEvent =  " << UsedEvent << std::endl;
			}

			Double_t eta_min = eta[i_min];

			TLorentzVector Vec_MissingET_MinPoint;
			Vec_MissingET_MinPoint.SetPtEtaPhiM(missingET->MET, eta[i_min], missingET->Phi, 0);

			LeptonandMET_MinPoint = Vec_Lepton + Vec_MissingET_MinPoint;

			h_MinPointMissingETEta_Recon->Fill(eta_min, Event_Weight);
			h_MinPointMissingETEta_Comp->Fill(eta_min - genmissingET->Eta, Event_Weight);
			if (det > 0) {
				h_MinPointMissingETEta_PosDetComp->Fill(eta_min - genmissingET->Eta, Event_Weight);
				if (abs(eta_min - eta_good) < abs(eta_min - eta_bad)) {
					h_MinPointMissingETEta_PosDetGoodvsBad->Fill(1.0, Event_Weight);
				}
				else {
					h_MinPointMissingETEta_PosDetGoodvsBad->Fill(0.0, Event_Weight);
				}
			}
			else {
				h_MinPointMissingETEta_NegDetComp->Fill(eta_min - genmissingET->Eta, Event_Weight);
			}

			if (det < 0) {
				h_MET_METvsDeltaM_NegDet->Fill(missingET->MET, deltaM[i_min], Event_Weight);
				h_MET_DeltaM_NegDet->Fill(deltaM[i_min], Event_Weight);
			}

			// MissingET Eta Calculation at Truth Level

			GenParticle* lep = (GenParticle*)bTruthLeptons->At(0);
			for (int i = 0; i < bTruthLeptons->GetEntriesFast(); i++) {
				GenParticle* curr_lep = (GenParticle*)bTruthLeptons->At(i);
				if ((abs(curr_lep->PID) == 11 || abs(curr_lep->PID) == 13) && abs(curr_lep->PT) > abs(lep->PT)) {
					lep = curr_lep;
					if (entry < ToPrint) {
						std::cout << "Lepton " << i + 1 << ": PID = " << lep->PID << ", pT = " << lep->PT << std::endl;
					}
				}
			}

			TLorentzVector TruthVec_Lepton;
			TruthVec_Lepton.SetPtEtaPhiM(lep->PT, lep->Eta, lep->Phi, lep->Mass);

			Double_t Truth_a = TruthVec_Lepton.E() - TruthVec_Lepton.Pz();
			Double_t Truth_b = TMath::Sq(W_mass) + TMath::Sq(TruthVec_Lepton.Px() + (genmissingET->MET * TMath::Cos(genmissingET->Phi))) + TMath::Sq(TruthVec_Lepton.Py() + (genmissingET->MET * TMath::Sin(genmissingET->Phi)));
			Truth_b += TMath::Sq(TruthVec_Lepton.Pz()) - TMath::Sq(TruthVec_Lepton.E()) - TMath::Sq(genmissingET->MET);
			Truth_b /= (-1 * genmissingET->MET);
			Double_t Truth_c = TruthVec_Lepton.E() + TruthVec_Lepton.Pz();
			Double_t Truth_det = TMath::Sq(Truth_b) - (4 * Truth_a * Truth_c);

			TLorentzVector TruthVec_MissingET_EtaPlus;
			TLorentzVector TruthVec_MissingET_EtaMinus;
			if (Truth_det > 0) {

				Truth_eta_plus = TMath::Log((-Truth_b + TMath::Sqrt(Truth_det)) / (2 * Truth_a));
				Truth_eta_minus = TMath::Log((-Truth_b - TMath::Sqrt(Truth_det)) / (2 * Truth_a));

				if (abs(Truth_eta_plus - genmissingET->Eta) < abs(Truth_eta_minus - genmissingET->Eta)) {
					h_TruthMissingETEta_Recon->Fill(Truth_eta_plus, Event_Weight);
					h_TruthMissingETEta_PlusvsMinus->Fill(1.0, Event_Weight);
					h_TruthMissingETEta_Comp->Fill(Truth_eta_plus - genmissingET->Eta);
					if (abs(Truth_eta_plus) < abs(Truth_eta_minus)) {
						h_TruthMissingETEta_SmallvsLargeMag->Fill(-1.0, Event_Weight);
					}
					else {
						h_TruthMissingETEta_SmallvsLargeMag->Fill(1.0, Event_Weight);
					}
				}
				else {
					h_TruthMissingETEta_Recon->Fill(Truth_eta_minus, Event_Weight);
					h_TruthMissingETEta_PlusvsMinus->Fill(-1.0, Event_Weight);
					h_TruthMissingETEta_Comp->Fill(Truth_eta_minus - genmissingET->Eta);
					if (abs(Truth_eta_plus) < abs(Truth_eta_minus)) {
						h_TruthMissingETEta_SmallvsLargeMag->Fill(1.0, Event_Weight);
					}
					else {
						h_TruthMissingETEta_SmallvsLargeMag->Fill(-1.0, Event_Weight);
					}
				}
			}
			else {
				h_TruthMissingETEta_PlusvsMinus->Fill(0.0, Event_Weight);
				h_TruthMissingETEta_SmallvsLargeMag->Fill(0.0, Event_Weight);
			}

			// Transverse Mass
			Double_t Phi_12 = abs(Vec_MissingET.Phi() - Vec_Lepton.Phi());
			if (Phi_12 > TMath::Pi()) {
				Phi_12 = (2 * TMath::Pi()) - Phi_12;
			}
			LeptonTransverse_mass = TMath::Sqrt(2 * abs(Vec_MissingET.Pt()) * abs(Vec_Lepton.Pt()) * (1 - TMath::Cos(Phi_12)));

			h_LeptonTransverseMass->Fill(LeptonTransverse_mass, Event_Weight);

			// Transeverse Lepton Components

			TLorentzVector Vec_Lepton_Transverse;
			Vec_Lepton_Transverse.SetPtEtaPhiM(Vec_Lepton.Pt(), 0, Vec_Lepton.Phi(), Vec_Lepton.M());

			TLorentzVector Vec_MissingET_Transverse;
			Vec_MissingET_Transverse.SetPtEtaPhiM(Vec_MissingET.Pt(), 0, Vec_MissingET.Phi(), Vec_MissingET.M());

			LeptonandMET_Transverse = Vec_Lepton_Transverse + Vec_MissingET_Transverse;


		} //Lepton and MET Loop

		//------------------------------------------------------------------
		// MissingET Eta Calculation at Truth Level from Ws
		//------------------------------------------------------------------

		Double_t TruthfromW_eta_plus;
		Double_t TruthfromW_eta_minus;

		for (int i = 0; i < bTruthWZ->GetEntriesFast(); ++i) {
			GenParticle* v = (GenParticle*)bTruthWZ->At(i);

			if (abs(v->PID) == 24) {
				GenParticle* D1 = (GenParticle*)bParticle->At(v->D1);
				GenParticle* D2 = (GenParticle*)bParticle->At(v->D2);
				if ((abs(D1->PID) == 11 && abs(D2->PID) == 12) || (abs(D1->PID) == 13 && abs(D2->PID) == 14)) {
					l_nu++;
					TLorentzVector Vec_Lepton;
					Vec_Lepton.SetPtEtaPhiM(D1->PT, D1->Eta, D1->Phi, D1->Mass);

					Double_t a = Vec_Lepton.E() - Vec_Lepton.Pz();
					Double_t b = TMath::Sq(W_mass) + TMath::Sq(Vec_Lepton.Px() + (D2->PT * TMath::Cos(D2->Phi))) + TMath::Sq(Vec_Lepton.Py() + (D2->PT * TMath::Sin(D2->Phi)));
					b += TMath::Sq(Vec_Lepton.Pz()) - TMath::Sq(Vec_Lepton.E()) - TMath::Sq(D2->PT);
					b /= (-1 * D2->PT);
					Double_t c = Vec_Lepton.E() + Vec_Lepton.Pz();
					Double_t det = TMath::Sq(b) - (4 * a * c);

					if (det > 0) {
						TruthfromW_eta_plus = TMath::Log((-b + TMath::Sqrt(det)) / (2 * a));
						TruthfromW_eta_minus = TMath::Log((-b - TMath::Sqrt(det)) / (2 * a));
						if (abs(TruthfromW_eta_plus) > abs(TruthfromW_eta_minus)) {
							PosLarge++;
						}
						else {
							PosSmall++;
						}

						if (abs(TruthfromW_eta_plus - D2->Eta) < abs(TruthfromW_eta_minus - D2->Eta)) {
							h_TruthfromWMissingETEta_Recon->Fill(TruthfromW_eta_plus, Event_Weight);
							h_TruthfromWMissingETEta_PlusvsMinus->Fill(1.0, Event_Weight);
							h_TruthfromWMissingETEta_Comp->Fill(TruthfromW_eta_plus - D2->Eta);
							if (abs(TruthfromW_eta_plus) < abs(TruthfromW_eta_minus)) {
								h_TruthfromWMissingETEta_SmallvsLargeMag->Fill(-1.0, Event_Weight);
							}
							else {
								h_TruthfromWMissingETEta_SmallvsLargeMag->Fill(1.0, Event_Weight);
							}
						}
						else {
							h_TruthfromWMissingETEta_Recon->Fill(TruthfromW_eta_minus, Event_Weight);
							h_TruthfromWMissingETEta_PlusvsMinus->Fill(-1.0, Event_Weight);
							h_TruthfromWMissingETEta_Comp->Fill(TruthfromW_eta_minus - D2->Eta);
							if (abs(TruthfromW_eta_plus) < abs(TruthfromW_eta_minus)) {
								h_TruthfromWMissingETEta_SmallvsLargeMag->Fill(1.0, Event_Weight);
							}
							else {
								h_TruthfromWMissingETEta_SmallvsLargeMag->Fill(-1.0, Event_Weight);
							}
						}
					}
					else {
						h_TruthfromWMissingETEta_PlusvsMinus->Fill(0.0, Event_Weight);
						h_TruthfromWMissingETEta_SmallvsLargeMag->Fill(0.0, Event_Weight);
					}
				}
				else if ((abs(D1->PID) == 12 && abs(D2->PID) == 11) || (abs(D1->PID) == 14 && abs(D2->PID) == 13)) {
					nu_l++;
					TLorentzVector Vec_Lepton;
					Vec_Lepton.SetPtEtaPhiM(D2->PT, D2->Eta, D2->Phi, D2->Mass);

					Double_t a = Vec_Lepton.E() - Vec_Lepton.Pz();
					Double_t b = TMath::Sq(W_mass) + TMath::Sq(Vec_Lepton.Px() + (D1->PT * TMath::Cos(D1->Phi))) + TMath::Sq(Vec_Lepton.Py() + (D1->PT * TMath::Sin(D1->Phi)));
					b += TMath::Sq(Vec_Lepton.Pz()) - TMath::Sq(Vec_Lepton.E()) - TMath::Sq(D1->PT);
					b /= (-1 * D1->PT);
					Double_t c = Vec_Lepton.E() + Vec_Lepton.Pz();
					Double_t det = TMath::Sq(b) - (4 * a * c);

					if (det > 0) {
						TruthfromW_eta_plus = TMath::Log((-b + TMath::Sqrt(det)) / (2 * a));
						TruthfromW_eta_minus = TMath::Log((-b - TMath::Sqrt(det)) / (2 * a));
						if (abs(TruthfromW_eta_plus) > abs(TruthfromW_eta_minus)) {
							PosLarge++;
						}
						else {
							PosSmall++;
						}

						if (abs(TruthfromW_eta_plus - D1->Eta) < abs(TruthfromW_eta_minus - D1->Eta)) {
							h_TruthfromWMissingETEta_Recon->Fill(TruthfromW_eta_plus, Event_Weight);
							h_TruthfromWMissingETEta_PlusvsMinus->Fill(1.0, Event_Weight);
							h_TruthfromWMissingETEta_Comp->Fill(TruthfromW_eta_plus - D1->Eta);
							if (abs(TruthfromW_eta_plus) < abs(TruthfromW_eta_minus)) {
								h_TruthfromWMissingETEta_SmallvsLargeMag->Fill(-1.0, Event_Weight);
							}
							else {
								h_TruthfromWMissingETEta_SmallvsLargeMag->Fill(1.0, Event_Weight);
							}
						}
						else {
							h_TruthfromWMissingETEta_Recon->Fill(TruthfromW_eta_minus, Event_Weight);
							h_TruthfromWMissingETEta_PlusvsMinus->Fill(-1.0, Event_Weight);
							h_TruthfromWMissingETEta_Comp->Fill(TruthfromW_eta_minus - D1->Eta);
							if (abs(TruthfromW_eta_plus) < abs(TruthfromW_eta_minus)) {
								h_TruthfromWMissingETEta_SmallvsLargeMag->Fill(1.0, Event_Weight);
							}
							else {
								h_TruthfromWMissingETEta_SmallvsLargeMag->Fill(-1.0, Event_Weight);
							}
						}
					}
					else {
						h_TruthfromWMissingETEta_PlusvsMinus->Fill(0.0, Event_Weight);
						h_TruthfromWMissingETEta_SmallvsLargeMag->Fill(0.0, Event_Weight);
					}
				}
			}

		}

		//------------------------------------------------------------------
		// MET Reconstruction Quality
		//------------------------------------------------------------------

		MissingET* missingET = (MissingET*)bMissingET->At(0);
		MissingET* genmissingET = (MissingET*)bGenMissingET->At(0);

		TLorentzVector Vec_MissingET;
		TLorentzVector Vec_GenMissingET;
		Vec_MissingET.SetPtEtaPhiM(missingET->MET, missingET->Eta, missingET->Phi, 0);
		Vec_GenMissingET.SetPtEtaPhiM(genmissingET->MET, genmissingET->Eta, genmissingET->Phi, 0);

		Double_t DeltaPhi = Vec_MissingET.Phi() - Vec_GenMissingET.Phi();
		if (DeltaPhi > TMath::Pi()) {
			DeltaPhi -= 2.0 * TMath::Pi();
		}
		else if (DeltaPhi <= -TMath::Pi()) {
			DeltaPhi += 2.0 * TMath::Pi();
		}

		h_METDeltaPt_MET->Fill(missingET->MET, missingET->MET - genmissingET->MET, Event_Weight);
		h_METSigmaPt_MET->Fill(missingET->MET, (missingET->MET - genmissingET->MET) / genmissingET->MET, Event_Weight);
		h_METDeltaPhi_MET->Fill(missingET->MET, DeltaPhi, Event_Weight);

		if (bElectron->GetEntriesFast() >= 1 || bMuon->GetEntriesFast() >= 1) {
			h_METDeltaPt_MET_wlep->Fill(missingET->MET, missingET->MET - genmissingET->MET, Event_Weight);
			h_METSigmaPt_MET_wlep->Fill(missingET->MET, (missingET->MET - genmissingET->MET) / genmissingET->MET, Event_Weight);
			h_METDeltaPhi_MET_wlep->Fill(missingET->MET, DeltaPhi, Event_Weight);
			h_METDeltaEta_MET_wlep->Fill(missingET->MET, missingET->Eta - genmissingET->Eta, Event_Weight);

			if (missingET->MET < 20) {
				h_METDeltaPhi_0_20->Fill(DeltaPhi, Event_Weight);
				h_METDeltaPt_0_20->Fill(missingET->MET - genmissingET->MET, Event_Weight);
				h_METDeltaEtaCalc_0_20->Fill(MET_EtaCalc - genmissingET->Eta, Event_Weight);
			}
			else if (missingET->MET < 30) {
				h_METDeltaPhi_20_30->Fill(DeltaPhi, Event_Weight);
				h_METDeltaPt_20_30->Fill(missingET->MET - genmissingET->MET, Event_Weight);
				h_METDeltaEtaCalc_20_30->Fill(MET_EtaCalc - genmissingET->Eta, Event_Weight);
			}
			else if (missingET->MET < 40) {
				h_METDeltaPhi_30_40->Fill(DeltaPhi, Event_Weight);
				h_METDeltaPt_30_40->Fill(missingET->MET - genmissingET->MET, Event_Weight);
				h_METDeltaEtaCalc_30_40->Fill(MET_EtaCalc - genmissingET->Eta, Event_Weight);
			}
			else if (missingET->MET < 50) {
				h_METDeltaPhi_40_50->Fill(DeltaPhi, Event_Weight);
				h_METDeltaPt_40_50->Fill(missingET->MET - genmissingET->MET, Event_Weight);
				h_METDeltaEtaCalc_40_50->Fill(MET_EtaCalc - genmissingET->Eta, Event_Weight);
			}
			else if (missingET->MET < 60) {
				h_METDeltaPhi_50_60->Fill(DeltaPhi, Event_Weight);
				h_METDeltaPt_50_60->Fill(missingET->MET - genmissingET->MET, Event_Weight);
				h_METDeltaEtaCalc_50_60->Fill(MET_EtaCalc - genmissingET->Eta, Event_Weight);
			}
			else if (missingET->MET < 80) {
				h_METDeltaPhi_60_80->Fill(DeltaPhi, Event_Weight);
				h_METDeltaPt_60_80->Fill(missingET->MET - genmissingET->MET, Event_Weight);
				h_METDeltaEtaCalc_60_80->Fill(MET_EtaCalc - genmissingET->Eta, Event_Weight);
			}
			else if (missingET->MET < 100) {
				h_METDeltaPhi_80_100->Fill(DeltaPhi, Event_Weight);
				h_METDeltaPt_80_100->Fill(missingET->MET - genmissingET->MET, Event_Weight);
				h_METDeltaEtaCalc_80_100->Fill(MET_EtaCalc - genmissingET->Eta, Event_Weight);
			}
		}

		h_METDeltapx->Fill(Vec_MissingET.Px() - Vec_GenMissingET.Px(), Event_Weight);
		h_METDeltapy->Fill(Vec_MissingET.Py() - Vec_GenMissingET.Py(), Event_Weight);
		h_METSigmapx->Fill((Vec_MissingET.Px() - Vec_GenMissingET.Px()) / Vec_GenMissingET.Px(), Event_Weight);
		h_METSigmapy->Fill((Vec_MissingET.Py() - Vec_GenMissingET.Py()) / Vec_GenMissingET.Py(), Event_Weight);

		//------------------------------------------------------------------
		// Z' Reconstruction
		//------------------------------------------------------------------

		if (bJet->GetEntriesFast() >= 2 && (bElectron->GetEntriesFast() >= 1 || bMuon->GetEntriesFast() >= 1)) {
			TLorentzVector ZprimeBasic = JetPair + LeptonandMET;
			h_ZprimeMass_Basic->Fill(ZprimeBasic.M(), Event_Weight);
		}

		if (JetPair.M() > 0 && JetPair.M() < 200 && LeptonandMET.M() > 0 && LeptonandMET.M() < 200) {
			TLorentzVector Zprime = JetPair + LeptonandMET;
			h_ZprimeMass->Fill(Zprime.M(), Event_Weight);
		}

		if (JetPair.M() > 0 && JetPair.M() < 200 && LeptonTransverse_mass > 0 && LeptonTransverse_mass < W_mass) {
			TLorentzVector Zprime_withTransverseMass = JetPair + LeptonandMET;
			h_Zprime_withTransverseMass->Fill(Zprime_withTransverseMass.M(), Event_Weight);
		}

		if (JetPair.M() > 0 && JetPair.M() < 200 && EtaCalc == 1) {
			TLorentzVector ZprimeEtaPlus = JetPair + LeptonandMET_EtaPlus;
			TLorentzVector ZprimeEtaMinus = JetPair + LeptonandMET_EtaMinus;
			TLorentzVector ZprimeEtaGood = JetPair + LeptonandMET_EtaGood;
			TLorentzVector ZprimeEtaBad = JetPair + LeptonandMET_EtaBad;

			h_Zprime_withEtaCalc->Fill(ZprimeEtaPlus.M(), Event_Weight);
			h_Zprime_withEtaPlus->Fill(ZprimeEtaPlus.M(), Event_Weight);
			h_Zprime_withEtaCalc->Fill(ZprimeEtaMinus.M(), Event_Weight);
			h_Zprime_withEtaMinus->Fill(ZprimeEtaMinus.M(), Event_Weight);
			h_Zprime_withEtaGood->Fill(ZprimeEtaGood.M(), Event_Weight);
			h_Zprime_withEtaBad->Fill(ZprimeEtaBad.M(), Event_Weight);
			if (entry < ToPrint) {
				std::cout << "eta_plus = " << eta_plus << ", eta_minus = " << eta_minus << std::endl;
				std::cout << "ZprimeEtaPlus = " << ZprimeEtaPlus.M() << ", ZprimeEtaMinus = " << ZprimeEtaMinus.M() << std::endl;
			}
		}

		if (JetPair.M() > 0 && JetPair.M() < 200 && LeptonandMET_Transverse.M() > 0 && LeptonandMET_Transverse.M() < 200) {
			TLorentzVector Zprime_withTransverseLeptons = JetPair + LeptonandMET_Transverse;
			h_Zprime_withTransverseLeptons->Fill(Zprime_withTransverseLeptons.M(), Event_Weight);
		}

		if (JetPair.M() > 0 && JetPair.M() < 200) {
			TLorentzVector ZprimeMinPoint = JetPair + LeptonandMET_MinPoint;
			h_Zprime_withMinPointMissingETEta->Fill(ZprimeMinPoint.M(), Event_Weight);
			if (EtaCalc == 1) {
				h_Zprime_withMinPointMissingETEta_PosDet->Fill(ZprimeMinPoint.M(), Event_Weight);
			}
			else {
				h_Zprime_withMinPointMissingETEta_NegDet->Fill(ZprimeMinPoint.M(), Event_Weight);
				h_MET_DeltaM_NegDetvsZprimeMass->Fill(deltaM[i_min], ZprimeMinPoint.M() - Zprime_mass, Event_Weight);
				if (deltaM[i_min] <= 30) {
					MET_NegDet_PassCut++;
				}
				if (deltaM[i_min] < 10) {
					h_MET_ZprimeMass_DeltaM_NegDet_0_10->Fill(ZprimeMinPoint.M(), Event_Weight);
				}
				else if (deltaM[i_min] < 20) {
					h_MET_ZprimeMass_DeltaM_NegDet_10_20->Fill(ZprimeMinPoint.M(), Event_Weight);
				}
				else if (deltaM[i_min] < 30) {
					h_MET_ZprimeMass_DeltaM_NegDet_20_30->Fill(ZprimeMinPoint.M(), Event_Weight);
				}
				else if (deltaM[i_min] < 40) {
					h_MET_ZprimeMass_DeltaM_NegDet_30_40->Fill(ZprimeMinPoint.M(), Event_Weight);
				}
				else if (deltaM[i_min] < 50) {
					h_MET_ZprimeMass_DeltaM_NegDet_40_50->Fill(ZprimeMinPoint.M(), Event_Weight);
				}
			}
		}

	} // Loop over all events

	// Eta Calculation Plot

	h_EtaGoodnessDist_Event1 = new TGraph(graph_n, eta_graph, deltaM_graph);
	h_EtaGoodnessDist_Event1->SetNameTitle("h_EtaGoodnessDist_Event1", "; #eta of MissingET; #Delta M");
	h_EtaGoodnessDist_Event1->SetMinimum(0);
	h_EtaGoodnessDist_Event1->SetMaximum(W_mass);
	h_EtaGoodnessDist_Event1->Draw("AEP");

	//std::cout << "l_nu = " << l_nu << ", nu_l = " << nu_l << std::endl;
	//std::cout << "PosLarge = " << PosLarge << ", PosSmall = " << PosSmall << std::endl;

	// Lepton pT Cut Calculation
	
	Double_t FromW_PassPercentage = (FromW_Pass / FromW_Total) * 100;
	Double_t NotFromW_PassPercentage = (NotFromW_Pass / NotFromW_Total) * 100;
	std::cout << "FromW_PassPercentage = " << FromW_PassPercentage << "% , NotFromW_PassPercentage = " << NotFromW_PassPercentage << "%" << std::endl;

	Double_t pT_cut[1000], FromWPerc[1000], NotFromWPerc[1000], Ratio[1000], FromWCount[1000], NotFromWCount[1000], GoodMinusBad[1000];
	Double_t Ratio_max = 0.0;
	Double_t pT_cut_atRatio_max, FromWPerc_atRatio_max, NotFromWPerc_atRatio_max;
	Double_t GoodMinusBad_max = 0.0;
	Double_t pT_cut_atGoodMinusBad_max, FromWPerc_atGoodMinusBad_max, NotFromWPerc_atGoodMinusBad_max;
	
	std::cout << "LeptonfromW_pT.size() = " << LeptonfromW_pT.size() << ", LeptonnotfromW_pT.size() = " << LeptonnotfromW_pT.size() << std::endl;

	for (Int_t i = 1; i < 1000; i++) {
		pT_cut[i] = (250.0 / 1000.0) * i;
		for (int j = 0; j < LeptonfromW_pT.size(); ++j) {
			if (LeptonfromW_pT.at(j) > pT_cut[i]) {
				FromWCount[i]++;
			}
		}
		FromWPerc[i] = (FromWCount[i]/LeptonfromW_pT.size()) * 100.0;
		for (int j = 0; j < LeptonnotfromW_pT.size(); ++j) {
			if (LeptonnotfromW_pT.at(j) > pT_cut[i]) {
				NotFromWCount[i]++;
			}
		}
		NotFromWPerc[i] = (NotFromWCount[i]/LeptonnotfromW_pT.size()) * 100.0;
		if (NotFromWPerc[i] > 0.0 && NotFromWPerc[i] <= 100.0 && FromWPerc[i] <= 100.0) {
			Ratio[i] = FromWPerc[i] / NotFromWPerc[i];
			GoodMinusBad[i] = FromWCount[i] - NotFromWCount[i];
			if (Ratio[i] > Ratio_max) {
				pT_cut_atRatio_max = pT_cut[i];
				Ratio_max = Ratio[i];
				FromWPerc_atRatio_max = FromWPerc[i];
				NotFromWPerc_atRatio_max = NotFromWPerc[i];
			}
			if (GoodMinusBad[i] > GoodMinusBad_max) {
				pT_cut_atGoodMinusBad_max = pT_cut[i];
				GoodMinusBad_max = GoodMinusBad[i];
				FromWPerc_atGoodMinusBad_max = FromWPerc[i];
				NotFromWPerc_atGoodMinusBad_max = NotFromWPerc[i];
			}
		}
		else {
			FromWPerc[i] = FromWPerc[i - 1];
			NotFromWPerc[i] = NotFromWPerc[i - 1];
			Ratio[i] = Ratio[i - 1];
			FromWCount[i] = FromWCount[i - 1];
			NotFromWCount[i] = NotFromWCount[i - 1];
			GoodMinusBad[i] = GoodMinusBad[i - 1];
		}
	}

	std::cout << "pT_cut_atRatio_max = " << pT_cut_atRatio_max << " GeV, Ratio_max = " << Ratio_max << std::endl;
	std::cout << "FromWPerc_atRatio_max = " << FromWPerc_atRatio_max << "%, NotFromWPerc_atRatio_max = " << NotFromWPerc_atRatio_max << "%" << std::endl;

	std::cout << "pT_cut_atGoodMinusBad_max = " << pT_cut_atGoodMinusBad_max << " GeV, GoodMinusBad_max = " << GoodMinusBad_max << std::endl;
	std::cout << "FromWPerc_atGoodMinusBad_max = " << FromWPerc_atGoodMinusBad_max << "%, NotFromWPerc_atGoodMinusBad_max = " << NotFromWPerc_atGoodMinusBad_max << "%" << std::endl;
	
	h_LeptonfromW_PassCutPerc = new TGraph(1000, pT_cut, FromWPerc);
	h_LeptonfromW_PassCutPerc->SetNameTitle("h_LeptonfromW_PassCutPerc", "; Lepton p_{T} Cut [GeV]; Leptons from a W which pass the cut / %");
	h_LeptonfromW_PassCutPerc->SetMinimum(0);
	h_LeptonfromW_PassCutPerc->SetMaximum(100);
	h_LeptonfromW_PassCutPerc->SetLineColor(416);
	h_LeptonfromW_PassCutPerc->SetLineWidth(3);
	h_LeptonfromW_PassCutPerc->Draw("AEP");

	h_LeptonnotfromW_PassCutPerc = new TGraph(1000, pT_cut, NotFromWPerc);
	h_LeptonnotfromW_PassCutPerc->SetNameTitle("h_LeptonnotfromW_PassCutPerc", "; Lepton p_{T} Cut [GeV]; Leptons not from a W which pass the cut / %");
	h_LeptonnotfromW_PassCutPerc->SetMinimum(0);
	h_LeptonnotfromW_PassCutPerc->SetMaximum(100);
	h_LeptonnotfromW_PassCutPerc->SetLineColor(632);
	h_LeptonnotfromW_PassCutPerc->SetLineWidth(3);
	h_LeptonnotfromW_PassCutPerc->Draw("AEP");

	for (Int_t i = 1; i < 1000; i++) {
		Ratio[i] *= (100.0 / Ratio_max);
		GoodMinusBad[i] *= (100.0 / GoodMinusBad_max);
	}

	h_LeptonfromW_PassCutRatio = new TGraph(1000, pT_cut, Ratio);
	h_LeptonfromW_PassCutRatio->SetNameTitle("h_LeptonfromW_PassCutRatio", "; Lepton p_{T} Cut [GeV]; Ratio of Leptons from:not from a W which pass the cut");
	h_LeptonfromW_PassCutRatio->SetMinimum(0);
	h_LeptonfromW_PassCutRatio->SetMaximum(100);
	h_LeptonfromW_PassCutRatio->SetLineColor(600);
	h_LeptonfromW_PassCutRatio->SetLineWidth(3);
	h_LeptonfromW_PassCutRatio->Draw("AEP");

	h_LeptonfromW_PassCutGoodMinusBad = new TGraph(1000, pT_cut, GoodMinusBad);
	h_LeptonfromW_PassCutGoodMinusBad->SetNameTitle("h_LeptonfromW_PassCutGoodMinusBad", "; Lepton p_{T} Cut [GeV]; Leptons (from - not from) a W which pass the cut");
	h_LeptonfromW_PassCutGoodMinusBad->SetMinimum(0);
	h_LeptonfromW_PassCutGoodMinusBad->SetMaximum(100);
	h_LeptonfromW_PassCutGoodMinusBad->SetLineColor(616);
	h_LeptonfromW_PassCutGoodMinusBad->SetLineWidth(3);
	h_LeptonfromW_PassCutGoodMinusBad->Draw("AEP");

	h_TruthElectronfromW_pT->SetOption("HIST");
	h_TruthElectronnotfromW_pT->SetOption("HIST");
	h_TruthMuonfromW_pT->SetOption("HIST");
	h_TruthMuonnotfromW_pT->SetOption("HIST");
	
	h_TruthLeptonfromW_Stack_pT->Add(h_TruthElectronfromW_pT);
	h_TruthLeptonfromW_Stack_pT->Add(h_TruthMuonfromW_pT);
	h_TruthLeptonnotfromW_Stack_pT->Add(h_TruthElectronnotfromW_pT);
	h_TruthLeptonnotfromW_Stack_pT->Add(h_TruthMuonnotfromW_pT);

	c_TruthLeptonfromW_Stack_pT = new TCanvas("c_TruthLeptonfromW_Stack_pT", "", 1000, 1000);
	c_TruthLeptonfromW_Stack_pT->cd();
	gStyle->SetPalette(1);
	h_TruthLeptonfromW_Stack_pT->Draw("PFC");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "");


	c_TruthLeptonnotfromW_Stack_pT = new TCanvas("c_TruthLeptonnotfromW_Stack_pT", "", 1000, 1000);
	c_TruthLeptonnotfromW_Stack_pT->cd();
	gStyle->SetPalette(1);
	h_TruthLeptonnotfromW_Stack_pT->Draw("PFC");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "");

	c_TruthLeptonSource_pT = new TCanvas("c_TruthLeptonSource_pT", "", 1000, 1000);
	c_TruthLeptonSource_pT->Divide(2, 1);
	c_TruthLeptonSource_pT->cd(1);
	gStyle->SetPalette(1);
	h_TruthLeptonfromW_Stack_pT->Draw("PFC");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "From W");
	c_TruthLeptonSource_pT->cd(2);
	gStyle->SetPalette(1);
	h_TruthLeptonnotfromW_Stack_pT->Draw("PFC");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Not from W");

	h_ReconElectronfromW_pT->SetOption("HIST");
	h_ReconElectronnotfromW_pT->SetOption("HIST");
	h_ReconMuonfromW_pT->SetOption("HIST");
	h_ReconMuonnotfromW_pT->SetOption("HIST");

	h_ReconLeptonfromW_Stack_pT->Add(h_ReconElectronfromW_pT);
	h_ReconLeptonfromW_Stack_pT->Add(h_ReconMuonfromW_pT);
	h_ReconLeptonnotfromW_Stack_pT->Add(h_ReconElectronnotfromW_pT);
	h_ReconLeptonnotfromW_Stack_pT->Add(h_ReconMuonnotfromW_pT);

	c_ReconLeptonfromW_Stack_pT = new TCanvas("c_ReconLeptonfromW_Stack_pT", "", 1000, 1000);
	c_ReconLeptonfromW_Stack_pT->cd();
	gStyle->SetPalette(1);
	h_ReconLeptonfromW_Stack_pT->Draw("PFC");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "");


	c_ReconLeptonnotfromW_Stack_pT = new TCanvas("c_ReconLeptonnotfromW_Stack_pT", "", 1000, 1000);
	c_ReconLeptonnotfromW_Stack_pT->cd();
	gStyle->SetPalette(1);
	h_ReconLeptonnotfromW_Stack_pT->Draw("PFC");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "");

	c_ReconLeptonSource_pT = new TCanvas("c_ReconLeptonSource_pT", "", 1000, 1000);
	c_ReconLeptonSource_pT->Divide(2, 1);
	c_ReconLeptonSource_pT->cd(1);
	gStyle->SetPalette(1);
	h_ReconLeptonfromW_Stack_pT->Draw("PFC");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "From W");
	c_ReconLeptonSource_pT->cd(2);
	gStyle->SetPalette(1);
	h_ReconLeptonnotfromW_Stack_pT->Draw("PFC");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Not from W");

	std::cout << "Electrons from W: " << (ElectronWRecon / ElectronWTruth) * 100.0 << "% Reconstructed" << std::endl;
	std::cout << "Electrons not from W: " << (ElectronOtherRecon / ElectronOtherTruth) * 100.0 << "% Reconstructed" << std::endl;
	std::cout << "Muons from W: " << (MuonWRecon / MuonWTruth) * 100.0 << "% Reconstructed" << std::endl;
	std::cout << "Muons not from W: " << (MuonOtherRecon / MuonOtherTruth) * 100.0 << "% Reconstructed" << std::endl;

	std::cout << "ElectronOtherRecon " << ElectronOtherRecon << ", ElectronOtherTruth " << ElectronOtherTruth << std::endl;
	std::cout << "MuonOtherRecon " << MuonOtherRecon << ", MuonOtherTruth " << MuonOtherTruth << std::endl;

	h_ElectronSources->SetOption("HIST");
	h_MuonSources->SetOption("HIST");

	h_LeptonSources->Add(h_ElectronSources);
	h_LeptonSources->Add(h_MuonSources);

	c_LeptonSources = new TCanvas("c_LeptonSources", "", 1000, 1000);
	c_LeptonSources->cd();
	gStyle->SetPalette(1);
	h_LeptonSources->Draw("PFC");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "");

	std::cout << "MET_NegDet_PassCut = " << MET_NegDet_PassCut << std::endl;

	h_ElectronfromLeptonSources->SetOption("HIST");
	h_MuonfromLeptonSources->SetOption("HIST");

	h_LeptonfromLeptonSources->Add(h_ElectronfromLeptonSources);
	h_LeptonfromLeptonSources->Add(h_MuonfromLeptonSources);

	c_LeptonfromLeptonSources = new TCanvas("c_LeptonfromLeptonSources", "", 1000, 1000);
	c_LeptonfromLeptonSources->cd();
	gStyle->SetPalette(1);
	h_LeptonfromLeptonSources->Draw("PFC");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "");

}
