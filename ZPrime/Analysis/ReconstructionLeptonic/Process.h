#include "TFile.h"
#include "TH2.h"
#include "TF1.h"
#include "TLorentzVector.h"
#include "TRandom3.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TMath.h"
#include "TColor.h"
#include "TStyle.h"
#include "TVector.h"
#include "TError.h"
#include "TSystemDirectory.h"
#include "TSystemFile.h"
#include "TVector3.h"
#include "TArrayI.h"

#include <iostream>
#include <utility>
#include <vector>

#include "TROOT.h"
#include "TSystem.h"
#include "TApplication.h"

#include "TString.h"
#include "TRandom3.h"
#include "TClonesArray.h"

#include "TLorentzVector.h"

#include "classes/DelphesClasses.h"

#include "ExRootAnalysis/ExRootTreeReader.h"
#include "ExRootAnalysis/ExRootTreeWriter.h"
#include "ExRootAnalysis/ExRootTreeBranch.h"
#include "ExRootAnalysis/ExRootResult.h"
#include "ExRootAnalysis/ExRootUtilities.h"

// Plots

TClonesArray * bEvent;
TClonesArray * bJet;
TClonesArray * bGenJet;
TClonesArray * bElectron;
TClonesArray * bMuon;
TClonesArray * bTruthLeptons;
TClonesArray * bMissingET;
TClonesArray * bGenMissingET;
TClonesArray * bParticle;

// Output
TFile * OutputFile;

TH1D * h_EventCount;
TH1D * h_WeightCount;

TH1D * h_ZprimeMass;
TH1D * h_JetUsedpT;
TH1D * h_JetUsedEta;
TH1D * h_ElectronUsedpT;
TH1D * h_ElectronUsedEta;
TH1D * h_MuonUsedpT;
TH1D * h_MuonUsedEta;
TH1D * h_METUsedpT;
TH1D * h_METUsedEta;
TH1D * h_JetComparisonDeltaR;
TH1D * h_JetComparisonDeltaRGoodMatch;
TH1D * h_JetComparisonSigma_pT;
TH1D * h_JetComparisonSigma_pTGoodMatch;
TH1D * h_ElectronComparisonDeltaR;
TH1D * h_ElectronComparisonDeltaRGoodMatch;
TH1D * h_ElectronComparisonSigma_pT;
TH1D * h_ElectronComparisonSigma_pTGoodMatch;
TH1D * h_MuonComparisonDeltaR;
TH1D * h_MuonComparisonDeltaRGoodMatch;
TH1D * h_MuonComparisonSigma_pT;
TH1D * h_MuonComparisonSigma_pTGoodMatch;
TH1D * h_MatchedGenJetEta;
TH1D * h_UnmatchedGenJetEta;
TH1D * h_MatchedTruthElectronEta;
TH1D * h_UnmatchedTruthElectronEta;
TH1D * h_MatchedTruthMuonEta;
TH1D * h_UnmatchedTruthMuonEta;
TH1D * h_MatchedGenJetpT;
TH1D * h_UnmatchedGenJetpT;
TH1D * h_MatchedTruthElectronpT;
TH1D * h_UnmatchedTruthElectronpT;
TH1D * h_MatchedTruthMuonpT;
TH1D * h_UnmatchedTruthMuonpT;

ExRootTreeReader * InitReader(const TString FilePath);

void Process(ExRootTreeReader * treeReader);

void ClearBranches();

int main(int argc, char* argv[]);
