
#include "Process.h"

bool Debug = false;
Int_t ToPrint = 50;
Double_t e_mass = 0.00051099895;
Double_t muon_mass = 0.1056583755;
Double_t W_mass = 80.38;
Int_t graph_n = 200;
Double_t eta[200], deltaM[200];

int main(int argc, char* argv[]) {

	// Input Delphes File

	const TString InputFile = argv[1];
	const TString OutputFileName = argv[2];

	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "Running Process"  << std::endl;
	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "InputFile = " << InputFile << std::endl;
	std::cout << "OutputFileName = " << OutputFileName << std::endl;
	std::cout << "-------------------------------------------------------------"  << std::endl;

	ExRootTreeReader * reader = NULL;
	reader = InitReader(InputFile);

	//------------------------------------
	// Declare the output
	//------------------------------------

	OutputFile = new TFile(OutputFileName,"recreate");

	OutputFile->cd();

	h_EventCount = new TH1D("h_EventCount", "", 1, 0, 1);
	h_WeightCount = new TH1D("h_WeightCount", "", 1, 0, 1);

	//------------------------------------

	// Run the selection
	Process(reader);

	std::cout << "Write to file..." << std::endl;

	OutputFile->cd();

	h_EventCount->Write();
	h_WeightCount->Write();

	h_EtaGoodnessDist_Event1->Write();

	OutputFile->Close();

	std::cout << "Tidy..." << std::endl;

	delete reader;

	std::cout << "Done!" << std::endl;

	return 0;

}

ExRootTreeReader * InitReader(const TString FilePath) {

	std::cout << "InitReader" << std::endl;

	TFile * f = TFile::Open(FilePath);

	TChain * Chain = new TChain("Delphes","");

	Chain->Add(FilePath);

	// Create object of class ExRootTreeReader
	ExRootTreeReader * r = new ExRootTreeReader(Chain);

	return r;
}

void Process(ExRootTreeReader * treeReader) {

	// Get pointers to branches used in this analysis
	bEvent = treeReader->UseBranch("Event");
	bJet = treeReader->UseBranch("Jet");
	bGenJet = treeReader->UseBranch("GenJet");
	bElectron = treeReader->UseBranch("Electron");
	bMuon = treeReader->UseBranch("Muon");
	bTruthLeptons = treeReader->UseBranch("TruthLeptonParticles");
	bMissingET = treeReader->UseBranch("MissingET");
	bGenMissingET = treeReader->UseBranch("GenMissingET");
	bParticle = treeReader->UseBranch("Particle");
	bTruthWZ = treeReader->UseBranch("TruthWZParticles");

	Long64_t numberOfEntries = treeReader->GetEntries();
	Int_t UsedEvent;

	int nSelected = 0;

	std::cout << "-------------------------------------------------------------" << std::endl;
	std::cout << "Input: " << numberOfEntries << " events to process" << std::endl;

	// Load selected branches with data from specified event
	treeReader->ReadEntry(0);

	HepMCEvent* event = (HepMCEvent*)bEvent->At(0);
	const float Event_Weight = event->Weight;

	h_EventCount->Fill(0.5);
	h_WeightCount->Fill(0.5, Event_Weight);
	
	for (Int_t i = 0; i < graph_n; i++) {
		eta[i] = i * 0.1;
		deltaM[i] = 10 * sin(eta[i] + 0.2);
	}

	h_EtaGoodnessDist_Event1 = new TGraph(graph_n, eta, deltaM);
	h_EtaGoodnessDist_Event1->SetNameTitle("h_EtaGoodnessDist_Event1", "; #eta of MissingET; #Delta M");
	h_EtaGoodnessDist_Event1->Draw("AEP");
}
