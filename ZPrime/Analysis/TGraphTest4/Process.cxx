
#include "Process.h"

bool Debug = false;
Int_t ToPrint = 50;
Double_t e_mass = 0.00051099895;
Double_t muon_mass = 0.1056583755;
Double_t W_mass = 80.38;

int main(int argc, char* argv[]) {

	// Input Delphes File

	const TString InputFile = argv[1];
	const TString OutputFileName = argv[2];

	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "Running Process"  << std::endl;
	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "InputFile = " << InputFile << std::endl;
	std::cout << "OutputFileName = " << OutputFileName << std::endl;
	std::cout << "-------------------------------------------------------------"  << std::endl;

	ExRootTreeReader * reader = NULL;
	reader = InitReader(InputFile);

	//------------------------------------
	// Declare the output
	//------------------------------------

	OutputFile = new TFile(OutputFileName,"recreate");

	OutputFile->cd();

	h_EventCount = new TH1D("h_EventCount", "", 1, 0, 1);
	h_WeightCount = new TH1D("h_WeightCount", "", 1, 0, 1);

	//------------------------------------

	// Run the selection
	Process(reader);

	std::cout << "Write to file..." << std::endl;

	OutputFile->cd();

	h_EventCount->Write();
	h_WeightCount->Write();

	h_EtaGoodnessDist_Event1->Write();

	OutputFile->Close();

	std::cout << "Tidy..." << std::endl;

	delete reader;

	std::cout << "Done!" << std::endl;

	return 0;

}

ExRootTreeReader * InitReader(const TString FilePath) {

	std::cout << "InitReader" << std::endl;

	TFile * f = TFile::Open(FilePath);

	TChain * Chain = new TChain("Delphes","");

	Chain->Add(FilePath);

	// Create object of class ExRootTreeReader
	ExRootTreeReader * r = new ExRootTreeReader(Chain);

	return r;
}

void Process(ExRootTreeReader * treeReader) {

	// Get pointers to branches used in this analysis
	bEvent = treeReader->UseBranch("Event");
	bJet = treeReader->UseBranch("Jet");
	bGenJet = treeReader->UseBranch("GenJet");
	bElectron = treeReader->UseBranch("Electron");
	bMuon = treeReader->UseBranch("Muon");
	bTruthLeptons = treeReader->UseBranch("TruthLeptonParticles");
	bMissingET = treeReader->UseBranch("MissingET");
	bGenMissingET = treeReader->UseBranch("GenMissingET");
	bParticle = treeReader->UseBranch("Particle");
	bTruthWZ = treeReader->UseBranch("TruthWZParticles");

	Long64_t numberOfEntries = treeReader->GetEntries();
	Int_t UsedEvent;

	int nSelected = 0;

	std::cout << "-------------------------------------------------------------" << std::endl;
	std::cout << "Input: " << numberOfEntries << " events to process" << std::endl;

	// MissingET Eta Goodness Distribution

	treeReader->ReadEntry(42);

	HepMCEvent* event = (HepMCEvent*)bEvent->At(0);
	const float Event_Weight = event->Weight;

	// Lepton
	TLorentzVector Vec_Lepton;

	if (bMuon->GetEntriesFast() == 0) {
		Electron* lepton = (Electron*)bElectron->At(0);
		Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, e_mass);
	}
	else if (bElectron->GetEntriesFast() == 0) {
		Muon* lepton = (Muon*)bMuon->At(0);
		Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, muon_mass);
	}
	else {
		Electron* electron = (Electron*)bElectron->At(0);
		Muon* muon = (Muon*)bMuon->At(0);
		if (electron->PT > muon->PT) {
			Electron* lepton = electron;
			Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, e_mass);
		}
		else {
			Muon* lepton = muon;
			Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, muon_mass);
		}
	}

	// MissingET 

	MissingET* missingET = (MissingET*)bMissingET->At(0);

	TLorentzVector Vec_MissingET;
	Vec_MissingET.SetPtEtaPhiM(missingET->MET, missingET->Eta, missingET->Phi, 0);

	// Distribution

	Int_t graph_n = 200;
	Double_t eta[200], deltaM[200];
	TLorentzVector Vec_MissingET_i;
	TLorentzVector LeptonandMET_i;

	for (Int_t i = 0; i < graph_n; i++) {
		eta[i] = -5 + (i * (10 / graph_n));
		Vec_MissingET_i.SetPtEtaPhiM(Vec_MissingET.Pt(), eta[i], Vec_MissingET.Phi(), 0);
		LeptonandMET_i = Vec_Lepton + Vec_MissingET_i;
		deltaM[i] = abs(LeptonandMET_i.M() - W_mass);
	}

	h_EtaGoodnessDist_Event1 = new TGraph(graph_n, eta, deltaM);
	h_EtaGoodnessDist_Event1->SetNameTitle("h_EtaGoodnessDist_Event1", "; #eta of MissingET; #Delta M");
	h_EtaGoodnessDist_Event1->Draw("AEP");

}