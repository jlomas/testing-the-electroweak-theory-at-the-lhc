
#include "Process.h"

bool Debug = false;

int main(int argc, char* argv[]) {

	// Input Delphes File

	const TString InputFile = argv[1];
	const TString OutputFileName = argv[2];

	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "Running Process"  << std::endl;
	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "InputFile = " << InputFile << std::endl;
	std::cout << "OutputFileName = " << OutputFileName << std::endl;
	std::cout << "-------------------------------------------------------------"  << std::endl;

	ExRootTreeReader * reader = NULL;
	reader = InitReader(InputFile);

	//------------------------------------
	// Declare the output
	//------------------------------------

	OutputFile = new TFile(OutputFileName,"recreate");

	OutputFile->cd();

	h_EventCount = new TH1D("h_EventCount","",1,0,1);
	h_WeightCount = new TH1D("h_WeightCount","",1,0,1);

	h_WSources = new TH1D("h_WSources ", "; W Source; Events", 2, 0.5, 2.5);
	h_TauSources = new TH1D("h_TauSources ","; Tau Source; Events",5,0.5,5.5);

	//------------------------------------

	// Run the selection
	Process(reader);

	std::cout << "Events in EventCount: " << h_EventCount->GetEntries() << std::endl;

	std::cout << "Write to file..." << std::endl;

	OutputFile->cd();

	h_EventCount->Write();
	h_WeightCount->Write();

	h_WSources->Write();
	h_TauSources->Write();

	OutputFile->Close();

	std::cout << "Tidy..." << std::endl;

	delete reader;

	std::cout << "Done!" << std::endl;

	return 0;

}

ExRootTreeReader * InitReader(const TString FilePath) {

	std::cout << "InitReader" << std::endl;

	TFile * f = TFile::Open(FilePath);

	TChain * Chain = new TChain("Delphes","");

	Chain->Add(FilePath);

	// Create object of class ExRootTreeReader
	ExRootTreeReader * r = new ExRootTreeReader(Chain);

	return r;
}

void Process(ExRootTreeReader * treeReader) {

	// Get pointers to branches used in this analysis
	bEvent = treeReader->UseBranch("Event");
	bJet = treeReader->UseBranch("Jet");
	bGenJet = treeReader->UseBranch("GenJet");
	bElectron = treeReader->UseBranch("Electron");
	bMuon = treeReader->UseBranch("Muon");
	bTruthLeptons = treeReader->UseBranch("TruthLeptonParticles");
	bMissingET = treeReader->UseBranch("MissingET");
	bGenMissingET = treeReader->UseBranch("GenMissingET");
	bParticle = treeReader->UseBranch("Particle");
	bTruthWZ = treeReader->UseBranch("TruthWZParticles");

	Long64_t numberOfEntries = treeReader->GetEntries();
	Int_t EntriestoPrint = 50;
	std::vector<Long64_t> WMothers;

	int nSelected = 0;

	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "Input: " << numberOfEntries << " events to process" << std::endl;

	// Loop over all events
	for(Int_t entry = 0; entry < numberOfEntries; ++entry) {

		// Load selected branches with data from specified event
		treeReader->ReadEntry(entry);

		HepMCEvent * event = (HepMCEvent*) bEvent->At(0);
			const float Event_Weight = event->Weight;

		h_EventCount->Fill(0.5);
		h_WeightCount->Fill(0.5,Event_Weight);

		if( (entry > 0 && entry%1000 == 0) || Debug) {
			std::cout << "-------------------------------------------------------------"  << std::endl;
			std::cout << "Processing Event Number =  " << entry  << std::endl;
			std::cout << "-------------------------------------------------------------"  << std::endl;
		}

		Long64_t PrintEntry = 0;
		Long64_t Wnum = 0;

		if( entry < EntriestoPrint ) {
			PrintEntry += 1;
		}

		for(int i = 0; i < bTruthWZ->GetEntriesFast(); ++i) {
			GenParticle* particle = (GenParticle*) bTruthWZ->At(i);
			if(abs(particle->PID) == 24 && particle->Status == 22) {
				Wnum++;
				GenParticle* M1 = (GenParticle*) bParticle->At(particle->M1);
				Long64_t Curr_PID = abs(M1->PID);
				Long64_t Match = 0;
				for(int k = 0; k < WMothers.size(); ++k) {
					if (WMothers.at(k) == Curr_PID) {
						Match += 1;
					}
				}
				if (Match == 0) {
					WMothers.push_back(Curr_PID);
				}
				TString W_output = "None";
				TString M1_Out = "Unknown";
				if (abs(M1->PID) == 32) {
					h_WSources->Fill(0.5, Event_Weight);
					M1_Out = "Z'";
					if (particle->Charge < 0) {
						W_output = "\033[1;32mW-\033[0m";
					}
					else {
						W_output = "\033[1;32mW+\033[0m";
					}
				}
				else if (abs(M1->PID) == 24) {
					GenParticle* First_NonW_M1 = (GenParticle*) bParticle->At(M1->M1);
					while (abs(First_NonW_M1->PID) == 24) {
						First_NonW_M1 = (GenParticle*)bParticle->At(First_NonW_M1->M1);
					}
					if (abs(First_NonW_M1->PID) == 32) {
						h_WSources->Fill(0.5, Event_Weight);
						if (particle->Charge < 0) {
							W_output = "\033[1;32mW-\033[0m";
						}
						else {
							W_output = "\033[1;32mW+\033[0m";
						}
					}
					else {
						h_WSources->Fill(1.5, Event_Weight);
						std::cout << First_NonW_M1->PID << std::endl;
						if (particle->Charge < 0) {
							W_output = "\033[1;31mW-\033[0m";
						}
						else {
							W_output = "\033[1;31mW+\033[0m";
						}
					}
				}
				else {
					h_WSources->Fill(1.5, Event_Weight);
					std::cout << M1->PID << std::endl;
					if (particle->Charge < 0) {
						W_output = "\033[1;31mW-\033[0m";
					}
					else {
						W_output = "\033[1;31mW+\033[0m";
					}
				}
				if (abs(M1->PID) == 24) {
					if (M1->Charge > 0) {
						M1_Out = "W+";
					}
					else {
						M1_Out = "W-";
					}
				}

				//if(PrintEntry > 0) {
				//	if (particle->M2 >= 0) {
				//		GenParticle* M2 = (GenParticle*) bParticle->At(particle->M2);
				//		TString M2_Out = "Unknown";
				//		if (abs(M2->PID) == 24) {
				//				if (M2->Charge > 0) {
				//				M2_Out = "W+";
				//				}
				//				else {
				//				M2_Out = "W-";
				//				}
				//		}
				//		std::cout << "Event " << entry+1 << ", " << W_output << " " << i << ", M1 = " << M1_Out << " " << particle->M1 << ", M2 = " << M2_Out << " " << particle->M2 << ", status = " << particle->Status << ", pT = " << particle->PT << ", eta = " << particle->Eta << ", phi = " << particle->Phi << ", energy = " << particle->E << std::endl;
				//	}
				//	else {
				//		std::cout << "Event " << entry+1 << ", " << W_output << " " << i << ", M1 = " << M1_Out << " " << particle->M1 << ", status = " << particle->Status << ", pT = " << particle->PT << ", eta = " << particle->Eta << ", phi = " << particle->Phi << ", energy = " << particle->E << std::endl;
				//	}
				//}
			}
		}
	}
	std::cout << "W Mothers PID: " << std::endl;
	for(int i = 0; i < WMothers.size(); ++i) {
		std::cout << WMothers.at(i) << std::endl;
	}
}
