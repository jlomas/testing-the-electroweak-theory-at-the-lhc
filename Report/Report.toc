\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}%
\contentsline {chapter}{\numberline {2}Background}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Types of Heavy Resonance}{3}{section.2.1}%
\contentsline {section}{\numberline {2.2}Decay Channels}{3}{section.2.2}%
\contentsline {section}{\numberline {2.3}ATLAS Detector}{4}{section.2.3}%
\contentsline {chapter}{\numberline {3}Simulation}{7}{chapter.3}%
\contentsline {section}{\numberline {3.1}Signal}{7}{section.3.1}%
\contentsline {section}{\numberline {3.2}Background}{8}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Control Regions}{12}{subsection.3.2.1}%
\contentsline {chapter}{\numberline {4}Reconstruction}{13}{chapter.4}%
\contentsline {section}{\numberline {4.1}Efficiency}{13}{section.4.1}%
\contentsline {section}{\numberline {4.2}Neutrino $\eta $ Calculation}{16}{section.4.2}%
\contentsline {section}{\numberline {4.3}Reconstructing the $Z'$}{19}{section.4.3}%
\contentsline {chapter}{\numberline {5}Event Selection}{20}{chapter.5}%
\contentsline {section}{\numberline {5.1}Signal}{20}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}Jet Pair Mass}{20}{subsection.5.1.1}%
\contentsline {subsection}{\numberline {5.1.2}Reconstruction Accuracy}{20}{subsection.5.1.2}%
\contentsline {section}{\numberline {5.2}Background}{22}{section.5.2}%
\contentsline {subsection}{\numberline {5.2.1}Optimising Significance}{22}{subsection.5.2.1}%
\contentsline {subsection}{\numberline {5.2.2}Cut Flow}{25}{subsection.5.2.2}%
\contentsline {section}{\numberline {5.3}Summary of Event Selection Criteria}{28}{section.5.3}%
\contentsline {chapter}{\numberline {6}Systematic Uncertainty}{29}{chapter.6}%
\contentsline {chapter}{\numberline {7}Results and Interpretation}{30}{chapter.7}%
\contentsline {chapter}{\numberline {8}Conclusion}{37}{chapter.8}%
